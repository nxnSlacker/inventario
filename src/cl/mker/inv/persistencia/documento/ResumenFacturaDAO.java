package cl.mker.inv.persistencia.documento;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.inject.Inject;

import cl.mker.inv.modelo.Factura;
import cl.mker.inv.modelo.documento.ResumenFactura;
import cl.mker.inv.persistencia.DAOException;

public class ResumenFacturaDAO {

	@Inject
	private Connection connection;
	
	public void setConnection(Connection connection) {
		this.connection = connection;
	}
	
	@PostConstruct
	private void init(){
		System.out.println("ResumenFacturaDAO init");
	}
	
	public List<ResumenFactura> listaResumenFacturas() throws DAOException {
		List<ResumenFactura> resumenesFactura = new ArrayList<ResumenFactura>();

		CallableStatement stmt = null;
        ResultSet rs = null;
        ResumenFactura resumenFactura = null;
        
		try {
			stmt = connection.prepareCall("{call listaResumenFacturas()}");
			rs = stmt.executeQuery();
			
			while (rs.next()) {
				resumenFactura = new ResumenFactura();
				resumenFactura.setIdFactura(rs.getLong("idFactura"));
				resumenFactura.setNumeroFactura(rs.getLong("numeroFactura"));
				resumenFactura.setFecha(rs.getDate("fecha"));
				resumenFactura.setPorcentaje(rs.getDouble("porcentaje"));
				resumenFactura.setTotalBruto(rs.getLong("totalBruto"));
				resumenFactura.setTotalNeto(rs.getLong("totalNeto"));
				resumenFactura.setCantidadAbonos(rs.getLong("cantidadAbonos"));
				resumenFactura.setTotalAbonado(rs.getLong("totalAbonado"));
				resumenFactura.setDeudaActual(rs.getLong("deudaActual"));
				
				resumenesFactura.add(resumenFactura);
            }
			
			return resumenesFactura;
		} catch (Exception e){
			throw new DAOException(e);
		}
	}
	
	public ResumenFactura listaResumenPorFactura(Factura factura) throws DAOException {
		CallableStatement stmt = null;
        ResultSet rs = null;
        ResumenFactura resumenFactura = null;
        
		try {
			stmt = connection.prepareCall("{call listaResumenPorFactura(?)}");
			stmt.setLong("idFactura", factura.getIdFactura());
			rs = stmt.executeQuery();
			
			if (rs.next()) {
				resumenFactura = new ResumenFactura();
				resumenFactura.setIdFactura(rs.getLong("idFactura"));
				resumenFactura.setNumeroFactura(rs.getLong("numeroFactura"));
				resumenFactura.setFecha(rs.getDate("fecha"));
				resumenFactura.setPorcentaje(rs.getDouble("porcentaje"));
				resumenFactura.setTotalBruto(rs.getLong("totalBruto"));
				resumenFactura.setTotalNeto(rs.getLong("totalNeto"));
				resumenFactura.setCantidadAbonos(rs.getLong("cantidadAbonos"));
				resumenFactura.setTotalAbonado(rs.getLong("totalAbonado"));
				resumenFactura.setDeudaActual(rs.getLong("deudaActual"));
            }
			
			return resumenFactura;
		} catch (Exception e){
			throw new DAOException(e);
		}
	}
}
