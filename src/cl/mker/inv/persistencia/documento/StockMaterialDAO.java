package cl.mker.inv.persistencia.documento;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.inject.Inject;

import cl.mker.inv.modelo.documento.StockMaterial;
import cl.mker.inv.persistencia.DAOException;

public class StockMaterialDAO {

	@Inject
	private Connection connection;
	
	public void setConnection(Connection connection) {
		this.connection = connection;
	}
	
	@PostConstruct
	private void init(){
		System.out.println("StockMaterialDAO init");
	}
	
	public List<StockMaterial> listaStockMateriales() throws DAOException {
		List<StockMaterial> stockMateriales = new ArrayList<StockMaterial>();

		CallableStatement stmt = null;
        ResultSet rs = null;
        StockMaterial stockMaterial = null;
        
		try {
			stmt = connection.prepareCall("{call listaStockMateriales()}");
			rs = stmt.executeQuery();
			
			while (rs.next()) {
				stockMaterial = new StockMaterial();
				stockMaterial.setIdMaterial(rs.getLong("idMaterial"));
				stockMaterial.setNombreMaterial(rs.getString("nombre"));
				stockMaterial.setCantidadVendidos(rs.getInt("vendidos"));
				stockMaterial.setCantidadComprados(rs.getInt("comprados"));
				stockMaterial.setCantidadDevueltos(rs.getInt("devueltos"));
				stockMaterial.setCantidadDisponible(rs.getInt("disponibles"));
				
				stockMateriales.add(stockMaterial);
            }
			
			return stockMateriales;
		} catch (Exception e){
			throw new DAOException(e);
		}
	}

}
