package cl.mker.inv.persistencia.documento;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.Date;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.inject.Inject;

import cl.mker.inv.modelo.documento.ResumenVenta;
import cl.mker.inv.persistencia.DAOException;

public class ResumenVentaDAO {

	@Inject
	private Connection connection;
	
	@PostConstruct
	private void init(){
		System.out.println("ResumenVentaDAO init");
	}
	
	public List<ResumenVenta> listaResumenVentas(Date fechaInicio, Date fechaFin) throws DAOException {
		List<ResumenVenta> resumenesVentas = new ArrayList<ResumenVenta>();

		CallableStatement stmt = null;
        ResultSet rs = null;
        ResumenVenta resumenVenta = null;
        
		try {
			stmt = connection.prepareCall("{call listaResumenVentasPorFecha(?,?)}");
			stmt.setDate("fechaInicio", fechaInicio);
			stmt.setDate("fechaFin", fechaFin);
			rs = stmt.executeQuery();
			
			while (rs.next()) {
				resumenVenta = new ResumenVenta();
				resumenVenta.setIdOrden(rs.getLong("idOrden"));
				resumenVenta.setFecha(rs.getDate("fecha"));
				resumenVenta.setNumeroFactura(rs.getString("numeroFactura"));
				resumenVenta.setNombreCliente(rs.getString("nombreCliente"));
				resumenVenta.setRut(rs.getString("rut"));
				resumenVenta.setPorcentaje(rs.getDouble("porcentaje"));
				resumenVenta.setIdMaterial(rs.getLong("idMaterial"));
				resumenVenta.setNombreMaterial(rs.getString("nombreMaterial"));
				resumenVenta.setCantidad(rs.getLong("cantidad"));
				resumenVenta.setValor(rs.getLong("valor"));
				resumenVenta.setTotalBruto(rs.getLong("totalBruto"));
				resumenVenta.setTotalNeto(rs.getLong("totalNeto"));
				
				resumenesVentas.add(resumenVenta);
            }
			
			return resumenesVentas;
		} catch (Exception e){
			throw new DAOException(e);
		}
	}

}
