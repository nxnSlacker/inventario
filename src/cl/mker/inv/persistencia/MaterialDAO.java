package cl.mker.inv.persistencia;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.inject.Inject;

import cl.mker.inv.modelo.Material;

public class MaterialDAO {

	@Inject
	private Connection connection;
	
	@PostConstruct
	private void init(){
		System.out.println("MaterialDAO init");
	}
	
	public List<Material> listaMateriales() throws DAOException {
		List<Material> materiales = new ArrayList<Material>();

		CallableStatement stmt = null;
        ResultSet rs = null;
        Material material = null;
        
		try {
			stmt = connection.prepareCall("{call listaMateriales()}");
			rs = stmt.executeQuery();
			
			while (rs.next()) {
				material = new Material();
				material.setIdMaterial(rs.getLong("idMaterial"));
				material.setValor(rs.getLong("valor"));
				material.setNombre(rs.getString("nombre"));
				material.setSector(rs.getString("sector"));
				material.setLugar(rs.getString("lugar"));
				
				materiales.add(material);
            }
			
			return materiales;
		} catch (Exception e){
			throw new DAOException(e);
		}
	}
	
	public boolean registraMaterial(Material material) throws DAOException {
		CallableStatement stmt = null;
        
		try {
			stmt = connection.prepareCall("{call insertaMaterial(?,?,?,?)}");
			stmt.setString("nombre", material.getNombre());
			stmt.setLong("valor", material.getValor());
			stmt.setString("sector", material.getSector());
			stmt.setString("lugar", material.getLugar());
			
			return ( stmt.executeUpdate() > 0 ) ;
		} catch (Exception e){
			throw new DAOException(e);
		}
	}
	
	public boolean actualizaMaterial(Material material) throws DAOException {
		CallableStatement stmt = null;
        
		try {
			stmt = connection.prepareCall("{call actualizaMaterial(?,?,?,?,?)}");
			stmt.setLong("idMaterial", material.getIdMaterial());
			stmt.setLong("valor", material.getValor());
			stmt.setString("nombre", material.getNombre());
			stmt.setString("sector", material.getSector());
			stmt.setString("lugar", material.getLugar());
			
			return ( stmt.executeUpdate() > 0 ) ;
		} catch (Exception e){
			throw new DAOException(e);
		}
	}
	
	public boolean eliminaMaterial(Material material) throws DAOException {
		CallableStatement stmt = null;
        
		try {
			stmt = connection.prepareCall("{call eliminaMaterial(?)}");
			stmt.setLong("idMaterial", material.getIdMaterial());
			
			return ( stmt.executeUpdate() > 0 ) ;
		} catch (Exception e){
			throw new DAOException(e);
		}
	}
	
	public Material buscaMaterialPorNombre(String nombre) throws DAOException{
		CallableStatement stmt = null;
        ResultSet rs = null;
        Material material = null;
        
		try {
			stmt = connection.prepareCall("{call buscaMaterialPorNombre(?)}");
			stmt.setString("nombre", nombre);
			rs = stmt.executeQuery();
			
			if (rs.next()) {
				material = new Material();
				material.setIdMaterial(rs.getLong("idMaterial"));
				material.setValor(rs.getLong("valor"));
				material.setNombre(rs.getString("nombre"));
				material.setSector(rs.getString("sector"));
				material.setLugar(rs.getString("lugar"));
            }
			
			return material;
		} catch (Exception e){
			throw new DAOException(e);
		}
	}

}
