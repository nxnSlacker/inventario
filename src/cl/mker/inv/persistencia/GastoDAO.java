package cl.mker.inv.persistencia;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.inject.Inject;

import cl.mker.inv.modelo.Gasto;

public class GastoDAO {

	@Inject
	private Connection connection;
	
	@PostConstruct
	private void init(){
		System.out.println("GastoDAO init");
	}

	public List<Gasto> listaGastos() throws DAOException {
		List<Gasto> gastos = new ArrayList<Gasto>();

		CallableStatement stmt = null;
        ResultSet rs = null;
        Gasto gasto = null;
        
		try {
			stmt = connection.prepareCall("{call listaGastos()}");
			rs = stmt.executeQuery();
			
			while (rs.next()) {
				gasto = new Gasto();
				gasto.setIdGasto(rs.getLong("idGasto"));
				gasto.setFecha(new Date(rs.getDate("fecha").getTime()));
				gasto.setMonto(rs.getLong("monto"));
				gasto.setDescripcion(rs.getString("descripcion"));
				
				gastos.add(gasto);
            }
			
			return gastos;
		} catch (Exception e){
			throw new DAOException(e);
		}
	}

	public boolean registraGasto(Gasto gasto) throws DAOException {
		CallableStatement stmt = null;
        
		try {
			stmt = connection.prepareCall("{call insertaGasto(?,?)}");
			stmt.setLong("monto", gasto.getMonto());
			stmt.setString("descripcion", gasto.getDescripcion());
			
			return ( stmt.executeUpdate() > 0 ) ;
		} catch (Exception e){
			throw new DAOException(e);
		}
	}

	public boolean actualizaGasto(Gasto gasto) throws DAOException {
		CallableStatement stmt = null;
        
		try {
			stmt = connection.prepareCall("{call actualizaGasto(?,?,?)}");
			stmt.setLong("idGasto", gasto.getIdGasto());
			stmt.setLong("monto", gasto.getMonto());
			stmt.setString("descripcion", gasto.getDescripcion());
			
			return ( stmt.executeUpdate() > 0 ) ;
		} catch (Exception e){
			throw new DAOException(e);
		}
	}

	public boolean eliminaGasto(Gasto gasto) throws DAOException {
		CallableStatement stmt = null;
        
		try {
			stmt = connection.prepareCall("{call eliminaGasto(?)}");
			stmt.setLong("idGasto", gasto.getIdGasto());
			
			return ( stmt.executeUpdate() > 0 ) ;
		} catch (Exception e){
			throw new DAOException(e);
		}
	}
	
	
}
