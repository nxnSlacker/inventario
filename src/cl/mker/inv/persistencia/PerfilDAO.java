package cl.mker.inv.persistencia;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.util.HashSet;
import java.util.Set;

import javax.annotation.PostConstruct;
import javax.inject.Inject;

import cl.mker.inv.seguridad.Perfil;
import cl.mker.inv.seguridad.Usuario;

public class PerfilDAO {

	@Inject
	private Connection connection;
	
	@PostConstruct
	private void init(){
		System.out.println("PerfilDAO init");
	}
	
	public Set<Perfil> listaPerfiles() throws DAOException {
		Set<Perfil> perfiles = new HashSet<Perfil>();

		CallableStatement stmt = null;
        ResultSet rs = null;
        Perfil perfil = null;
        
		try {
			stmt = connection.prepareCall("{call listaPerfiles()}");
			rs = stmt.executeQuery();
			
			while (rs.next()) {
				perfil = new Perfil();
				perfil.setIdPerfil(rs.getLong("idPerfil"));
				perfil.setGrupo(rs.getString("grupo"));
				
				perfiles.add(perfil);
            }
			
			return perfiles;
		} catch (Exception e){
			throw new DAOException(e);
		}
	}
	
	public Set<Perfil> buscaPerfilesPorUsuario(Usuario usuario) throws DAOException {
		Set<Perfil> perfiles = new HashSet<Perfil>();

		CallableStatement stmt = null;
        ResultSet rs = null;
        Perfil perfil = null;
        
		try {
			stmt = connection.prepareCall("{call buscaPerfilesPorUsuario(?)}");
			stmt.setLong("idUsuario", usuario.getIdUsuario());
			rs = stmt.executeQuery();
			
			while (rs.next()) {
				perfil = new Perfil();
				perfil.setIdPerfil(rs.getLong("idPerfil"));
				perfil.setGrupo(rs.getString("grupo"));
				
				perfiles.add(perfil);
            }
			
			return perfiles;
		} catch (Exception e){
			throw new DAOException(e);
		}
	}

	public boolean agregaPerfilUsuario(Usuario usuario, Perfil perfil) throws DAOException {
		CallableStatement stmt = null;
        
		try {
			stmt = connection.prepareCall("{call agregaPerfilUsuario(?,?)}");
			stmt.setLong("idUsuario", usuario.getIdUsuario());
			stmt.setLong("idPerfil", perfil.getIdPerfil());
			
			return ( stmt.executeUpdate() > 0 ) ;
		} catch (Exception e){
			throw new DAOException(e);
		}
	}

	public boolean eliminaPerfilUsuario(Usuario usuario, Perfil perfil) throws DAOException {
		CallableStatement stmt = null;
        
		try {
			stmt = connection.prepareCall("{call eliminaPerfilUsuario(?,?)}");
			stmt.setLong("idUsuario", usuario.getIdUsuario());
			stmt.setLong("idPerfil", perfil.getIdPerfil());
			
			return ( stmt.executeUpdate() > 0 ) ;
		} catch (Exception e){
			throw new DAOException(e);
		}
	}
}
