package cl.mker.inv.persistencia;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.inject.Inject;

import cl.mker.inv.modelo.LineaSurtido;
import cl.mker.inv.modelo.Material;
import cl.mker.inv.modelo.Surtido;

public class LineaSurtidoDAO {

	@Inject
	private Connection connection;
	
	@PostConstruct
	private void init(){
		System.out.println("LineaSurtidoDAO init");
	}

	public List<LineaSurtido> listaLineasSurtidoPorIdSurtido(Long idSurtido) throws DAOException {
		List<LineaSurtido> lineasSurtido = new ArrayList<LineaSurtido>();
		CallableStatement stmt = null;
		ResultSet rs = null;
		LineaSurtido lineaSurtido = null;
		
		try {
			stmt = connection.prepareCall("{call listaLineasSurtidoPorIdSurtido(?)}");
			stmt.setLong("idSurtido", idSurtido);
			rs = stmt.executeQuery();
			
			while (rs.next()) {
				lineaSurtido = new LineaSurtido();
				lineaSurtido.setIdLineaSurtido(rs.getLong("idLineaSurtido"));
				lineaSurtido.setValor(rs.getLong("valor"));
				lineaSurtido.setCantidad(rs.getInt("cantidad"));
				
				lineasSurtido.add(lineaSurtido);
			}
			
			return lineasSurtido;
		} catch (Exception e){
			throw new DAOException(e);
		}
	}

	public Material buscaMaterialPorIdLineaSurtido(Long idLineaSurtido) throws DAOException {
		CallableStatement stmt = null;
		ResultSet rs = null;
		Material material = null;
		
		try {
			stmt = connection.prepareCall("{call buscaMaterialPorIdLineaSurtido(?)}");
			stmt.setLong("idLineaSurtido", idLineaSurtido);
			rs = stmt.executeQuery();
			
			if (rs.next()) {
				material = new Material();
				material.setIdMaterial(rs.getLong("idMaterial"));
				material.setValor(rs.getLong("valor"));
				material.setNombre(rs.getString("nombre"));
			}
			
			return material;
		} catch (Exception e){
			throw new DAOException(e);
		}
	}

	public List<Material> listaMaterialesFueraDeSurtido(Long idSurtido) throws DAOException {
		List<Material> materiales = new ArrayList<Material>();
		CallableStatement stmt = null;
		ResultSet rs = null;
		Material material = null;
		
		try {
			stmt = connection.prepareCall("{call listaMaterialesFueraDeSurtido(?)}");
			stmt.setLong("idSurtido", idSurtido);
			rs = stmt.executeQuery();
			
			while (rs.next()) {
				material = new Material();
				material.setIdMaterial(rs.getLong("idMaterial"));
				material.setValor(rs.getLong("valor"));
				material.setNombre(rs.getString("nombre"));
				
				materiales.add(material);
			}
			
			return materiales;
		} catch (Exception e){
			throw new DAOException(e);
		}
	}
	
	public boolean agregaLineasSurtido(LineaSurtido lineaSurtido) throws DAOException {
		CallableStatement stmt = null;
		
		try{ 
			stmt = connection.prepareCall("{call insertaLineaSurtido(?,?,?,?)}");
			stmt.setLong("idSurtido", lineaSurtido.getSurtido().getIdSurtido());
			stmt.setLong("idMaterial", lineaSurtido.getMaterial().getIdMaterial());
			stmt.setInt("cantidad", lineaSurtido.getNuevaCantidad());
			stmt.setLong("valor", lineaSurtido.getValor());
			
			return ( stmt.executeUpdate() > 0 ) ;
		} catch (Exception e){
			throw new DAOException(e);
		}
	}

	public boolean actualizaLineasSurtido(LineaSurtido lineaSurtido) throws DAOException {
		CallableStatement stmt = null;
		
		try{ 
			stmt = connection.prepareCall("{call actualizaLineaSurtido(?,?,?,?)}");
			stmt.setLong("idSurtido", lineaSurtido.getSurtido().getIdSurtido());
			stmt.setLong("idMaterial", lineaSurtido.getMaterial().getIdMaterial());
			stmt.setInt("cantidad", lineaSurtido.getNuevaCantidad());
			stmt.setLong("valor", lineaSurtido.getValor());
			
			return ( stmt.executeUpdate() > 0 ) ;
		} catch (Exception e){
			throw new DAOException(e);
		}
	}

	public boolean eliminaLineasSurtido(LineaSurtido lineaSurtido) throws DAOException {
		CallableStatement stmt = null;
		
		try{ 
			stmt = connection.prepareCall("{call eliminaLineaSurtido(?)}");
			stmt.setLong("idLineaSurtido", lineaSurtido.getIdLineaSurtido());
			
			return ( stmt.executeUpdate() > 0 ) ;
		} catch (Exception e){
			throw new DAOException(e);
		}
	}

	public boolean eliminaLineasSurtidoPorSurtido(Surtido surtido) throws DAOException {
		CallableStatement stmt = null;
		
		try{ 
			stmt = connection.prepareCall("{call eliminaLineaSurtidoPorSurtido(?)}");
			stmt.setLong("idSurtido", surtido.getIdSurtido());
			
			return ( stmt.executeUpdate() > 0 ) ;
		} catch (Exception e){
			throw new DAOException(e);
		}
	}

	public int sumaCantidadLineaSurtidoPorMaterial(Material material) throws DAOException {
		int stock = 0;
		CallableStatement stmt = null;
		ResultSet rs = null;
		
		try {
			stmt = connection.prepareCall("{call sumaCantidadLineaSurtidoPorMaterial(?)}");
			stmt.setLong("idMaterial", material.getIdMaterial());
			rs = stmt.executeQuery();
			
			while (rs.next()) {
				stock += rs.getInt("cantidad");
			}
			
			return stock;
		} catch (Exception e){
			throw new DAOException(e);
		}
	}

	public List<LineaSurtido> listaLineasSurtidoPorMaterial(Material material) throws DAOException {
		List<LineaSurtido> lineasSurtido = new ArrayList<LineaSurtido>();
		CallableStatement stmt = null;
		ResultSet rs = null;
		LineaSurtido lineaSurtido = null;
		
		try {
			stmt = connection.prepareCall("{call listaLineasSurtidoPorMaterial(?)}");
			stmt.setLong("idMaterial", material.getIdMaterial());
			rs = stmt.executeQuery();
			
			while( rs.next() ) {
				lineaSurtido = new LineaSurtido();
				lineaSurtido.setIdLineaSurtido(rs.getLong("idLineaSurtido"));
				lineaSurtido.setValor(rs.getLong("valor"));
				lineaSurtido.setCantidad(rs.getInt("cantidad"));
				
				lineasSurtido.add(lineaSurtido);
			}
			
			return lineasSurtido;
		} catch (Exception e){
			throw new DAOException(e);
		}
	}

}
