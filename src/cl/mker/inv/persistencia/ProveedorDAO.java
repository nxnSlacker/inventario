package cl.mker.inv.persistencia;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.inject.Inject;

import cl.mker.inv.modelo.Proveedor;

public class ProveedorDAO {

	@Inject
	private Connection connection;
	
	@PostConstruct
	private void init(){
		System.out.println("ProveedorDAO init");
	}
	
	public List<Proveedor> listaProveedores() throws DAOException {
		List<Proveedor> proveedores = new ArrayList<Proveedor>();

		CallableStatement stmt = null;
        ResultSet rs = null;
        Proveedor proveedor = null;
        
		try {
			stmt = connection.prepareCall("{call listaProveedores()}");
			rs = stmt.executeQuery();
			
			while (rs.next()) {
				proveedor = new Proveedor();
				proveedor.setIdProveedor(rs.getLong("idProveedor"));
				proveedor.setRut(rs.getString("rut"));
				proveedor.setRazonSocial(rs.getString("razonSocial"));
				proveedor.setDireccion(rs.getString("direccion"));
				proveedor.setTelefono(rs.getString("telefono"));

				proveedores.add(proveedor);
            }
			
			return proveedores;
		} catch (Exception e){
			throw new DAOException(e);
		}
	}
	
	public boolean registraProveedor(Proveedor proveedor) throws DAOException {
		CallableStatement stmt = null;
        
		try {
			stmt = connection.prepareCall("{call insertaProveedor(?,?,?,?)}");
			stmt.setString("rut", proveedor.getRut());
			stmt.setString("razonSocial", proveedor.getRazonSocial());
			stmt.setString("direccion", proveedor.getDireccion());
			stmt.setString("telefono", proveedor.getTelefono());
			
			return ( stmt.executeUpdate() > 0 ) ;
		} catch (Exception e){
			throw new DAOException(e);
		}
	}
	
	public boolean actualizaProveedor(Proveedor proveedor) throws DAOException {
		CallableStatement stmt = null;
        
		try {
			stmt = connection.prepareCall("{call actualizaProveedor(?,?,?,?,?)}");
			stmt.setLong("idProveedor", proveedor.getIdProveedor());
			stmt.setString("rut", proveedor.getRut());
			stmt.setString("razonSocial", proveedor.getRazonSocial());
			stmt.setString("direccion", proveedor.getDireccion());
			stmt.setString("telefono", proveedor.getTelefono());
			
			return ( stmt.executeUpdate() > 0 ) ;
		} catch (Exception e){
			throw new DAOException(e);
		}
	}
	
	public boolean eliminaProveedor(Proveedor proveedor) throws DAOException {
		CallableStatement stmt = null;
        
		try {
			stmt = connection.prepareCall("{call eliminaProveedor(?)}");
			stmt.setLong("idProveedor", proveedor.getIdProveedor());
			
			return ( stmt.executeUpdate() > 0 ) ;
		} catch (Exception e){
			throw new DAOException(e);
		}
	}
	
	public Proveedor buscaProveedorPorRut(String rut) throws DAOException{
		CallableStatement stmt = null;
        ResultSet rs = null;
        Proveedor proveedor = null;
        
		try {
			stmt = connection.prepareCall("{call buscaProveedorPorRut(?)}");
			stmt.setString("rut", rut);
			rs = stmt.executeQuery();
			
			if (rs.next()) {
				proveedor = new Proveedor();
				proveedor.setIdProveedor(rs.getLong("idProveedor"));
				proveedor.setRut(rs.getString("rut"));
				proveedor.setRazonSocial(rs.getString("razonSocial"));
				proveedor.setDireccion(rs.getString("direccion"));
				proveedor.setTelefono(rs.getString("telefono"));
            }
			
			return proveedor;
		} catch (Exception e){
			throw new DAOException(e);
		}
	}

	public Proveedor buscaProveedorPorSurtido(long idSurtido) throws DAOException {
		CallableStatement stmt = null;
        ResultSet rs = null;
        Proveedor proveedor = null;
        
		try {
			stmt = connection.prepareCall("{call buscaProveedorPorSurtido(?)}");
			stmt.setLong("idSurtido", idSurtido);
			rs = stmt.executeQuery();
			
			if (rs.next()) {
				proveedor = new Proveedor();
				proveedor.setIdProveedor(rs.getLong("idProveedor"));
				proveedor.setRut(rs.getString("rut"));
				proveedor.setRazonSocial(rs.getString("razonSocial"));
				proveedor.setDireccion(rs.getString("direccion"));
				proveedor.setTelefono(rs.getString("telefono"));
            }
			
			return proveedor;
		} catch (Exception e){
			throw new DAOException(e);
		}
	}

}
