package cl.mker.inv.persistencia;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.inject.Inject;

import cl.mker.inv.modelo.Cliente;

public class ClienteDAO {

	@Inject
	private Connection connection;
	
	@PostConstruct
	private void init(){
		System.out.println("ClienteDAO init");
	}
	
	public List<Cliente> listaClientes() throws DAOException {
		List<Cliente> clientes = new ArrayList<Cliente>();

		CallableStatement stmt = null;
        ResultSet rs = null;
        Cliente cliente = null;
        
		try {
			stmt = connection.prepareCall("{call listaClientes()}");
			rs = stmt.executeQuery();
			
			while (rs.next()) {
				cliente = new Cliente();
				cliente.setIdCliente(rs.getLong("idCliente"));
				cliente.setRut(rs.getString("rut"));
				cliente.setNombre(rs.getString("nombre"));
				cliente.setDireccion(rs.getString("direccion"));
				cliente.setTelefono(rs.getString("telefono"));
				
				clientes.add(cliente);
            }
			
			return clientes;
		} catch (Exception e){
			throw new DAOException(e);
		}
	}
	
	public boolean registraCliente(Cliente cliente) throws DAOException {
		CallableStatement stmt = null;
        
		try {
			stmt = connection.prepareCall("{call insertaCliente(?,?,?,?)}");
			stmt.setString("rut", cliente.getRut());
			stmt.setString("nombre", cliente.getNombre());
			stmt.setString("direccion", cliente.getDireccion());
			stmt.setString("telefono", cliente.getTelefono());
			
			return ( stmt.executeUpdate() > 0 ) ;
		} catch (Exception e){
			throw new DAOException(e);
		}
	}
	
	public boolean actualizaCliente(Cliente cliente) throws DAOException {
		CallableStatement stmt = null;
        
		try {
			stmt = connection.prepareCall("{call actualizaCliente(?,?,?,?,?)}");
			stmt.setLong("idCliente", cliente.getIdCliente());
			stmt.setString("rut", cliente.getRut());
			stmt.setString("nombre", cliente.getNombre());
			stmt.setString("direccion", cliente.getDireccion());
			stmt.setString("telefono", cliente.getTelefono());
			
			return ( stmt.executeUpdate() > 0 ) ;
		} catch (Exception e){
			throw new DAOException(e);
		}
	}
	
	public boolean eliminaCliente(Cliente cliente) throws DAOException {
		CallableStatement stmt = null;
        
		try {
			stmt = connection.prepareCall("{call eliminaCliente(?)}");
			stmt.setLong("idCliente", cliente.getIdCliente());
			
			return ( stmt.executeUpdate() > 0 ) ;
		} catch (Exception e){
			throw new DAOException(e);
		}
	}
	
	public Cliente buscaClientePorRut(String rut) throws DAOException{
		CallableStatement stmt = null;
        ResultSet rs = null;
        Cliente cliente = null;
        
		try {
			stmt = connection.prepareCall("{call buscaClientePorRut(?)}");
			stmt.setString("rut", rut);
			rs = stmt.executeQuery();
			
			if (rs.next()) {
				cliente = new Cliente();
				cliente.setIdCliente(rs.getLong("idCliente"));
				cliente.setRut(rs.getString("rut"));
				cliente.setNombre(rs.getString("nombre"));
				cliente.setDireccion(rs.getString("direccion"));
				cliente.setTelefono(rs.getString("telefono"));
            }
			
			return cliente;
		} catch (Exception e){
			throw new DAOException(e);
		}
	}

	public Cliente buscaClientePorOrden(Long idOrden) throws DAOException {
		CallableStatement stmt = null;
        ResultSet rs = null;
        Cliente cliente = null;
        
		try {
			stmt = connection.prepareCall("{call buscaClientePorOrden(?)}");
			stmt.setLong("idOrden", idOrden);
			rs = stmt.executeQuery();
			
			if (rs.next()) {
				cliente = new Cliente();
				cliente.setIdCliente(rs.getLong("idCliente"));
				cliente.setRut(rs.getString("rut"));
				cliente.setNombre(rs.getString("nombre"));
				cliente.setDireccion(rs.getString("direccion"));
				cliente.setTelefono(rs.getString("telefono"));
            }
			
			return cliente;
		} catch (Exception e){
			throw new DAOException(e);
		}
	}

}
