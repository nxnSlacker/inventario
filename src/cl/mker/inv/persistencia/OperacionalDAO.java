package cl.mker.inv.persistencia;

import java.io.File;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;

import cl.mker.inv.modelo.configuracion.CuentaCorreo;
import cl.mker.inv.modelo.configuracion.Empresa;

public class OperacionalDAO {

	@PostConstruct
	private void init(){
		System.out.println("OperacionalDAO init");
	}

	public Empresa listaDatosEmpresa(){
		InputStream xml = getClass().getResourceAsStream("/empresa.xml");
		JAXBContext jc;
		try {
			jc = JAXBContext.newInstance(Empresa.class);
			Unmarshaller unmarshaller = jc.createUnmarshaller();
			return (Empresa) unmarshaller.unmarshal(xml);
		} catch (JAXBException e) {
			e.printStackTrace();
		}
		
		return null;
	}
	
	public CuentaCorreo listaDatosCuentaCorreo(){
		InputStream xml = getClass().getResourceAsStream("/correo.xml");
		JAXBContext jc;
		try {
			jc = JAXBContext.newInstance(CuentaCorreo.class);
			Unmarshaller unmarshaller = jc.createUnmarshaller();
			return (CuentaCorreo) unmarshaller.unmarshal(xml);
		} catch (JAXBException e) {
			e.printStackTrace();
		}
		
		return null;
	}
	
	/**
	 * Test
	 * 
	 * @param args
	 */
	public static void main(String[] args) {
		// Los archivos se crean en la raiz del proyecto Ej: Inventario/correo.xml
		pruebaCorreo();
		pruebaEmpresa();
	}
	
	private static void pruebaCorreo(){
		CuentaCorreo cuentaCorreo = new CuentaCorreo();
		cuentaCorreo.setHost("smtp.gmail.com");
		cuentaCorreo.setPuerto(587);
		cuentaCorreo.setUsuario("info.jormat@gmail.com");
		cuentaCorreo.setContraseña("x5qMR8MMc3x");
		
		try {
			JAXBContext jaxbContext = JAXBContext.newInstance( CuentaCorreo.class );
			Marshaller jaxbMarshaller = jaxbContext.createMarshaller();
			
			jaxbMarshaller.setProperty( Marshaller.JAXB_FORMATTED_OUTPUT, true );
            jaxbMarshaller.marshal( cuentaCorreo, new File( "correo.xml" ) );
            jaxbMarshaller.marshal( cuentaCorreo, System.out );

		} catch (JAXBException e) {
			e.printStackTrace();
		}
	}
	
	private static void pruebaEmpresa(){
		List<String> telefonos = new ArrayList<String>();
		List<String> celulares = new ArrayList<String>();
		
		telefonos.add("043-2361215");
		telefonos.add("043-2362968");
		
		celulares.add("09-92050943");
		celulares.add("09-88075107");
		
		Empresa empresa = new Empresa();
		empresa.setNombre("Jormat");
		empresa.setCiudad("Los Angeles");
		empresa.setDireccion("Valdivia 906");
		empresa.setTelefonosFijos(telefonos);
		empresa.setCelulares(celulares);
		empresa.setEmail("contacto@importadorajormat.cl");
		
		try {
			JAXBContext jaxbContext = JAXBContext.newInstance( Empresa.class );
			Marshaller jaxbMarshaller = jaxbContext.createMarshaller();
			
			jaxbMarshaller.setProperty( Marshaller.JAXB_FORMATTED_OUTPUT, true );
            jaxbMarshaller.marshal( empresa, new File( "empresa.xml" ) );
            jaxbMarshaller.marshal( empresa, System.out );

		} catch (JAXBException e) {
			e.printStackTrace();
		}
	}

}
