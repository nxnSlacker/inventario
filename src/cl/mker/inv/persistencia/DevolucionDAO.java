package cl.mker.inv.persistencia;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.inject.Inject;

import cl.mker.inv.modelo.Devolucion;
import cl.mker.inv.modelo.LineaMaterial;

public class DevolucionDAO {

	@Inject
	private Connection connection;
	
	@PostConstruct
	private void init(){
		System.out.println("DevolucionDAO init");
	}
	
	public List<Devolucion> buscaDevolucionesPorLineaMaterial(LineaMaterial lineaMaterial) throws DAOException{
		List<Devolucion> devoluciones = new ArrayList<Devolucion>();
		CallableStatement stmt = null;
        ResultSet rs = null;
        Devolucion devolucion = null;
        
		try {
			stmt = connection.prepareCall("{call buscaDevolucionesPorLineaMaterial(?)}");
			stmt.setLong("idLineaMaterial", lineaMaterial.getIdLineaMaterial());
			rs = stmt.executeQuery();
			
			while (rs.next()) {
				devolucion = new Devolucion();
				devolucion.setIdDevolucion(rs.getLong("idDevolucion"));
				devolucion.setCantidad(rs.getInt("cantidad"));
				devolucion.setFecha(new Date(rs.getDate("fecha").getTime()));
				devolucion.setMotivo(rs.getString("motivo"));
				devolucion.setLineaMaterial(lineaMaterial);
				
				devoluciones.add(devolucion);
            }
			
			return devoluciones;
		} catch (Exception e){
			throw new DAOException(e);
		}
	}

	public boolean registraDevolucion(Devolucion devolucion) throws DAOException {
		CallableStatement stmt = null;
        
		try {
			stmt = connection.prepareCall("{call insertaDevolucion(?,?,?)}");
			stmt.setLong("idLineaMaterial", devolucion.getLineaMaterial().getIdLineaMaterial());
			stmt.setInt("cantidad", devolucion.getCantidad());
			stmt.setString("motivo", devolucion.getMotivo());
			
			return ( stmt.executeUpdate() > 0 ) ;
		} catch (Exception e){
			throw new DAOException(e);
		}
	}

	public boolean actualizaDevolucion(Devolucion devolucion) throws DAOException {
		CallableStatement stmt = null;
        
		try {
			stmt = connection.prepareCall("{call actualizaDevolucion(?,?,?)}");
			stmt.setLong("idDevolucion", devolucion.getIdDevolucion());
			stmt.setInt("cantidad", devolucion.getCantidad());
			stmt.setString("motivo", devolucion.getMotivo());
			
			return ( stmt.executeUpdate() > 0 ) ;
		} catch (Exception e){
			throw new DAOException(e);
		}
	}

	public boolean eliminaDevolucion(Devolucion devolucion) throws DAOException {
		CallableStatement stmt = null;
        
		try {
			stmt = connection.prepareCall("{call eliminaDevolucion(?)}");
			stmt.setLong("idDevolucion", devolucion.getIdDevolucion());
			
			return ( stmt.executeUpdate() > 0 ) ;
		} catch (Exception e){
			throw new DAOException(e);
		}
	}
}
