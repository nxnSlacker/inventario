package cl.mker.inv.persistencia;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.inject.Inject;

import cl.mker.inv.seguridad.Usuario;

public class UsuarioDAO {
	
	@Inject
	private Connection connection;

	@PostConstruct
	private void init(){
		System.out.println("UsuarioDAO init");
	}
	
	public Usuario buscaUsuarioPorUsername(String username) throws DAOException {
		Usuario usuario = null;
		CallableStatement stmt = null;
		ResultSet rs = null;
		try {
			stmt = connection.prepareCall("{call buscaUsuarioPorUsername(?)}");
			stmt.setString("username", username);
			rs = stmt.executeQuery();
			
			if (rs.next()) {
				usuario = new Usuario();
				usuario.setIdUsuario(rs.getLong("idUsuario"));
				usuario.setUsername(rs.getString("username"));
				usuario.setPassword(rs.getString("password"));
				usuario.setHabilitado(rs.getBoolean("habilitado"));
				usuario.setCodigoActivacion(rs.getString("codigoActivacion"));
				usuario.setFechaActivacion(rs.getDate("fechaActivacion"));
	        }
			
			return usuario;
		} catch (Exception e) {
			throw new DAOException(e);
		}
	}

	public boolean insertUsuario(Usuario usuario) throws DAOException {
		try {
			CallableStatement stmt = null;
	        ResultSet rs = null;
	        
			try {
				stmt = connection.prepareCall("{call insertaUsuario(?,?)}");
				stmt.setString("username", usuario.getUsername());
				stmt.setBoolean("habilitado", usuario.isHabilitado());
				rs = stmt.executeQuery();
				
				if(rs.next()){
					usuario.setIdUsuario(rs.getLong(1));
					return true;
				}

				return false;
			} catch (Exception e){
				throw new DAOException(e);
			}
		} catch (Exception e) {
			throw new DAOException(e);
		}
	}
	
	public boolean actualizaUsuario(Usuario usuario) throws DAOException {
		try {
			CallableStatement stmt = null;
	        
			try {
				stmt = connection.prepareCall("{call actualizaUsuario(?,?,?)}");
				stmt.setLong("idUsuario", usuario.getIdUsuario());
				stmt.setString("username", usuario.getUsername());
				stmt.setBoolean("habilitado", usuario.isHabilitado());
				
				return ( stmt.executeUpdate() > 0 );
			} catch (Exception e){
				throw new DAOException(e);
			}
		} catch (Exception e) {
			throw new DAOException(e);
		}
	}

	public List<Usuario> listaUsuarios() throws DAOException {
		List<Usuario> usuarios = new ArrayList<Usuario>();
		
		CallableStatement stmt = null;
		ResultSet rs = null;
		Usuario usuario = null;
		
		try {
			stmt = connection.prepareCall("{call listaUsuarios()}");
			rs = stmt.executeQuery();
			
			while (rs.next()) {
				usuario = new Usuario();
				usuario.setIdUsuario(rs.getLong("idUsuario"));
				usuario.setUsername(rs.getString("username"));
				usuario.setPassword(rs.getString("password"));
				usuario.setHabilitado(rs.getBoolean("habilitado"));
				usuario.setCodigoActivacion(rs.getString("codigoActivacion"));
				usuario.setFechaActivacion(rs.getDate("fechaActivacion"));
				
				usuarios.add(usuario);
	        }
			
			return usuarios;
		} catch (Exception e) {
			throw new DAOException(e);
		}
	}

	public boolean actualizaCodigoActivacion(Usuario usuario) throws DAOException {
		try {
			CallableStatement stmt = null;
	        
			try {
				stmt = connection.prepareCall("{call actualizaCodigoActivacion(?,?)}");
				stmt.setLong("idUsuario", usuario.getIdUsuario());
				stmt.setString("codigoActivacion", usuario.getCodigoActivacion());
				
				return ( stmt.executeUpdate() > 0 );
			} catch (Exception e){
				throw new DAOException(e);
			}
		} catch (Exception e) {
			throw new DAOException(e);
		}
	}
	
	public boolean actualizaPassword(Usuario usuario) throws DAOException {
		try {
			CallableStatement stmt = null;
	        
			try {
				stmt = connection.prepareCall("{call actualizaPassword(?,?)}");
				stmt.setLong("idUsuario", usuario.getIdUsuario());
				stmt.setString("password", usuario.getPassword());
				
				return ( stmt.executeUpdate() > 0 );
			} catch (Exception e){
				throw new DAOException(e);
			}
		} catch (Exception e) {
			throw new DAOException(e);
		}
	}
	
	public boolean eliminaUsuario(Usuario usuario) throws DAOException {
		CallableStatement stmt = null;
        
		try {
			stmt = connection.prepareCall("{call eliminaUsuario(?)}");
			stmt.setLong("idUsuario", usuario.getIdUsuario());
			
			return ( stmt.executeUpdate() > 0 ) ;
		} catch (Exception e){
			throw new DAOException(e);
		}
	}

}
