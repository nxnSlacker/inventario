package cl.mker.inv.persistencia;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.inject.Inject;

import cl.mker.inv.modelo.Cliente;
import cl.mker.inv.modelo.Factura;
import cl.mker.inv.modelo.Orden;

public class FacturaDAO {

	@Inject
	private Connection connection;
	
	@PostConstruct
	private void init(){
		System.out.println("ClienteDAO init");
	}

	public List<Factura> listaFacturas() throws DAOException {
		List<Factura> facturas = new ArrayList<Factura>();

		CallableStatement stmt = null;
        ResultSet rs = null;
        Factura factura = null;
        
		try {
			stmt = connection.prepareCall("{call listaFacturas()}");
			rs = stmt.executeQuery();
			
			while (rs.next()) {
				factura = new Factura();
				factura.setIdFactura(rs.getLong("idFactura"));
				factura.setNumeroFactura(rs.getString("numeroFactura"));
				factura.setFecha(new Date(rs.getDate("fecha").getTime()));
				
				facturas.add(factura);
            }
			
			return facturas;
		} catch (Exception e){
			throw new DAOException(e);
		}
	}

	public Cliente buscaClientePorFactura(Factura factura) throws DAOException {
		CallableStatement stmt = null;
        ResultSet rs = null;
        Cliente cliente = null;
        
		try {
			stmt = connection.prepareCall("{call buscaClientePorFactura(?)}");
			stmt.setLong("idFactura", factura.getIdFactura());
			rs = stmt.executeQuery();
			
			if (rs.next()) {
				cliente = new Cliente();
				cliente.setIdCliente(rs.getLong("idCliente"));
				cliente.setRut(rs.getString("rut"));
				cliente.setNombre(rs.getString("nombre"));
            }
			
			return cliente;
		} catch (Exception e){
			throw new DAOException(e);
		}
	}
	
	public boolean registraFactura(Factura factura) throws DAOException {
		CallableStatement stmt = null;
        ResultSet rs = null;
		try {
			stmt = connection.prepareCall("{call insertaFactura(?,?)}");
			stmt.setLong("idImpuesto", factura.getImpuesto().getIdImpuesto());
			stmt.setString("numeroFactura", factura.getNumeroFactura());
			rs = stmt.executeQuery();
			
			if(rs.next()){
				factura.setIdFactura(rs.getLong(1));
				return true;
			}

			return false;
		} catch (Exception e){
			throw new DAOException(e);
		}
	}

	public boolean eliminaFactura(Factura factura) throws DAOException {
		CallableStatement stmt = null;
		
		try{ 
			stmt = connection.prepareCall("{call eliminaFactura(?)}");
			stmt.setLong("idFactura", factura.getIdFactura());
			
			return ( stmt.executeUpdate() > 0 ) ;
		} catch (Exception e){
			throw new DAOException(e);
		}
	}

	public boolean actualizaFactura(Factura factura) throws DAOException {
		CallableStatement stmt = null;
		
		try {
			stmt = connection.prepareCall("{call actualizaFactura(?,?,?)}");
			stmt.setLong("idFactura", factura.getIdFactura());
			stmt.setLong("idImpuesto", factura.getImpuesto().getIdImpuesto());
			stmt.setString("numeroFactura", factura.getNumeroFactura());
			
			return ( stmt.executeUpdate() > 0 ) ;
		} catch (Exception e){
			throw new DAOException(e);
		}
	}

	public Factura buscaFacturaPorOrden(Orden orden) throws DAOException {
		CallableStatement stmt = null;
        ResultSet rs = null;
        Factura factura = null;
        
		try {
			stmt = connection.prepareCall("{call buscaFacturaPorOrden(?)}");
			stmt.setLong("idOrden", orden.getIdOrden());
			rs = stmt.executeQuery();
			
			if (rs.next()) {
				factura = new Factura();
				factura.setIdFactura(rs.getLong("idFactura"));
				factura.setNumeroFactura(rs.getString("numeroFactura"));
				factura.setFecha(new Date(rs.getDate("fecha").getTime()));
            }
			
			return factura;
		} catch (Exception e){
			throw new DAOException(e);
		}
	}

	public boolean descartaOrdenesParaFactura(Factura factura) throws DAOException {
		CallableStatement stmt = null;
		
		try {
			stmt = connection.prepareCall("{call descartaOrdenesParaFactura(?)}");
			stmt.setLong("idFactura", factura.getIdFactura());
			
			return ( stmt.executeUpdate() > 0 ) ;
		} catch (Exception e){
			throw new DAOException(e);
		}
	}

	public Factura buscaFacturaPorNumero(Long numeroFactura) throws DAOException {
		CallableStatement stmt = null;
        ResultSet rs = null;
        Factura factura = null;
        
		try {
			stmt = connection.prepareCall("{call buscaFacturaPorNumero(?)}");
			stmt.setLong("numeroFactura", numeroFactura);
			rs = stmt.executeQuery();
			
			if (rs.next()) {
				factura = new Factura();
				factura.setIdFactura(rs.getLong("idFactura"));
				factura.setNumeroFactura(rs.getString("numeroFactura"));
				factura.setFecha(new Date(rs.getDate("fecha").getTime()));
            }
			
			return factura;
		} catch (Exception e){
			throw new DAOException(e);
		}
	}

}
