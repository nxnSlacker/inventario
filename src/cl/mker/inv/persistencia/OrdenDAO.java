package cl.mker.inv.persistencia;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.inject.Inject;

import cl.mker.inv.modelo.Cliente;
import cl.mker.inv.modelo.Factura;
import cl.mker.inv.modelo.Orden;

public class OrdenDAO {

	@Inject
	private Connection connection;
	
	@PostConstruct
	private void init(){
		System.out.println("OrdenDAO init");
	}

	public List<Orden> listaOrdenes() throws DAOException {
		List<Orden> ordenes = new ArrayList<Orden>();

		CallableStatement stmt = null;
        ResultSet rs = null;
        Orden orden = null;
        
		try {
			stmt = connection.prepareCall("{call listaOrdenes()}");
			rs = stmt.executeQuery();
			
			while (rs.next()) {
				orden = new Orden();
				orden.setIdOrden(rs.getLong("idOrden"));
				orden.setFecha(rs.getDate("fecha"));
				
				ordenes.add(orden);
            }
			
			return ordenes;
		} catch (Exception e){
			throw new DAOException(e);
		}
	}

	public boolean agregaOrden(Orden orden) throws DAOException {
		CallableStatement stmt = null;
        ResultSet rs = null;
		try {
			stmt = connection.prepareCall("{call insertaOrden(?)}");
			stmt.setLong("idCliente", orden.getCliente().getIdCliente());
			rs = stmt.executeQuery();
			
			if(rs.next()){
				orden.setIdOrden(rs.getLong(1));
				return true;
			}

			return false;
		} catch (Exception e){
			throw new DAOException(e);
		}
	}

	public boolean actualizaOrden(Orden orden) throws DAOException {
		CallableStatement stmt = null;

		try {
			stmt = connection.prepareCall("{call actualizaOrden(?,?)}");
			stmt.setLong("idOrden", orden.getIdOrden());
			stmt.setLong("idCliente", orden.getCliente().getIdCliente());
			
			return ( stmt.executeUpdate() > 0 );
		} catch (Exception e){
			throw new DAOException(e);
		}
	}
	
	public boolean actualizaFacturaParaOrden(Orden orden) throws DAOException {
		CallableStatement stmt = null;

		try {
			stmt = connection.prepareCall("{call actualizaFacturaParaOrden(?,?)}");
			stmt.setLong("idOrden", orden.getIdOrden());
			stmt.setLong("idFactura", orden.getFactura().getIdFactura());
			
			return ( stmt.executeUpdate() > 0 );
		} catch (Exception e){
			throw new DAOException(e);
		}
	}
	
	public boolean descartaFacturaParaOrden(Orden orden) throws DAOException {
		CallableStatement stmt = null;

		try {
			stmt = connection.prepareCall("{call descartaFacturaParaOrden(?)}");
			stmt.setLong("idOrden", orden.getIdOrden());
			
			return ( stmt.executeUpdate() > 0 );
		} catch (Exception e){
			throw new DAOException(e);
		}
	}

	public boolean eliminaOrden(Orden orden) throws DAOException {
		CallableStatement stmt = null;
		
		try{ 
			stmt = connection.prepareCall("{call eliminaOrden(?)}");
			stmt.setLong("idOrden", orden.getIdOrden());
			
			return ( stmt.executeUpdate() > 0 ) ;
		} catch (Exception e){
			throw new DAOException(e);
		}
	}

	public List<Orden> listaOrdenesPorFactura(Factura factura) throws DAOException {
		List<Orden> ordenes = new ArrayList<Orden>();

		CallableStatement stmt = null;
        ResultSet rs = null;
        Orden orden = null;
        
		try {
			stmt = connection.prepareCall("{call listaOrdenesPorFactura(?)}");
			stmt.setLong("idFactura", factura.getIdFactura());
			rs = stmt.executeQuery();
			
			while (rs.next()) {
				orden = new Orden();
				orden.setIdOrden(rs.getLong("idOrden"));
				orden.setFecha(rs.getDate("fecha"));
				
				ordenes.add(orden);
            }
			
			return ordenes;
		} catch (Exception e){
			throw new DAOException(e);
		}
	}

	public List<Orden> listaOrdenesSinFacturaPorCliente(Cliente cliente) throws DAOException {
		List<Orden> ordenes = new ArrayList<Orden>();

		CallableStatement stmt = null;
        ResultSet rs = null;
        Orden orden = null;
        
		try {
			stmt = connection.prepareCall("{call listaOrdenesSinFacturaPorCliente(?)}");
			stmt.setLong("idCliente", cliente.getIdCliente());
			rs = stmt.executeQuery();
			
			while (rs.next()) {
				orden = new Orden();
				orden.setIdOrden(rs.getLong("idOrden"));
				orden.setFecha(rs.getDate("fecha"));
				
				ordenes.add(orden);
            }
			
			return ordenes;
		} catch (Exception e){
			throw new DAOException(e);
		}
	}

	public List<Orden> listaOrdenesPorCliente(Cliente cliente) throws DAOException {
		List<Orden> ordenes = new ArrayList<Orden>();

		CallableStatement stmt = null;
        ResultSet rs = null;
        Orden orden = null;
        
		try {
			stmt = connection.prepareCall("{call listaOrdenesPorCliente(?)}");
			stmt.setLong("idCliente", cliente.getIdCliente());
			rs = stmt.executeQuery();
			
			while (rs.next()) {
				orden = new Orden();
				orden.setIdOrden(rs.getLong("idOrden"));
				orden.setFecha(rs.getDate("fecha"));
				
				ordenes.add(orden);
            }
			
			return ordenes;
		} catch (Exception e){
			throw new DAOException(e);
		}
	}
}
