package cl.mker.inv.persistencia;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.inject.Inject;

import cl.mker.inv.modelo.Proveedor;
import cl.mker.inv.modelo.Surtido;

public class SurtidoDAO {

	@Inject
	private Connection connection;
	
	@PostConstruct
	private void init(){
		System.out.println("SurtidoDAO init");
	}

	public List<Surtido> listaSurtidos() throws DAOException {
		List<Surtido> surtidos = new ArrayList<Surtido>();

		CallableStatement stmt = null;
        ResultSet rs = null;
        Surtido surtido = null;
        
		try {
			stmt = connection.prepareCall("{call listaSurtidos()}");
			rs = stmt.executeQuery();
			
			while (rs.next()) {
				surtido = new Surtido();
				surtido.setIdSurtido(rs.getLong("idSurtido"));
				surtido.setNumeroFactura(rs.getLong("numeroFactura"));
				surtido.setFecha(rs.getDate("fecha"));
				
				surtidos.add(surtido);
            }
			
			return surtidos;
		} catch (Exception e){
			throw new DAOException(e);
		}
	}

	public boolean agregaSurtido(Surtido surtido) throws DAOException {
		CallableStatement stmt = null;
        ResultSet rs = null;
		try {
			stmt = connection.prepareCall("{call insertaSurtido(?,?)}");
			stmt.setLong("idProveedor", surtido.getProveedor().getIdProveedor());
			stmt.setLong("numeroFactura", surtido.getNumeroFactura());
			rs = stmt.executeQuery();
			
			if(rs.next()){
				surtido.setIdSurtido(rs.getLong(1));
				return true;
			}

			return false;
		} catch (Exception e){
			throw new DAOException(e);
		}
	}

	public boolean actualizaSurtido(Surtido surtido) throws DAOException {
		CallableStatement stmt = null;

		try {
			stmt = connection.prepareCall("{call actualizaSurtido(?,?,?)}");
			stmt.setLong("idSurtido", surtido.getIdSurtido());
			stmt.setLong("idProveedor", surtido.getProveedor().getIdProveedor());
			stmt.setLong("numeroFactura", surtido.getNumeroFactura());
			
			return ( stmt.executeUpdate() > 0 );
		} catch (Exception e){
			throw new DAOException(e);
		}
	}

	public boolean eliminaSurtido(Surtido surtido) throws DAOException {
		CallableStatement stmt = null;
		
		try{ 
			stmt = connection.prepareCall("{call eliminaSurtido(?)}");
			stmt.setLong("idSurtido", surtido.getIdSurtido());
			
			return ( stmt.executeUpdate() > 0 ) ;
		} catch (Exception e){
			throw new DAOException(e);
		}
	}

	public List<Surtido> listaSurtidosPorProveedor(Proveedor proveedor) throws DAOException {
		List<Surtido> surtidos = new ArrayList<Surtido>();

		CallableStatement stmt = null;
        ResultSet rs = null;
        Surtido surtido = null;
        
		try {
			stmt = connection.prepareCall("{call listaSurtidosPorProveedor(?)}");
			stmt.setLong("idProveedor", proveedor.getIdProveedor());
			rs = stmt.executeQuery();
			
			while (rs.next()) {
				surtido = new Surtido();
				surtido.setIdSurtido(rs.getLong("idSurtido"));
				surtido.setNumeroFactura(rs.getLong("numeroFactura"));
				surtido.setFecha(rs.getDate("fecha"));
				
				surtidos.add(surtido);
            }
			
			return surtidos;
		} catch (Exception e){
			throw new DAOException(e);
		}
	}

	public Surtido buscaSurtidoPorNumero(Long numeroFactura) throws DAOException {
		CallableStatement stmt = null;
        ResultSet rs = null;
        Surtido surtido = null;
        
		try {
			stmt = connection.prepareCall("{call buscaSurtidoPorNumero(?)}");
			stmt.setLong("numeroFactura", numeroFactura);
			rs = stmt.executeQuery();
			
			if (rs.next()) {
				surtido = new Surtido();
				surtido.setIdSurtido(rs.getLong("idSurtido"));
				surtido.setNumeroFactura(rs.getLong("numeroFactura"));
				surtido.setFecha(rs.getDate("fecha"));
            }
			
			return surtido;
		} catch (Exception e){
			throw new DAOException(e);
		}
	}
}
