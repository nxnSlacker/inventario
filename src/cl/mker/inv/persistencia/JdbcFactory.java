package cl.mker.inv.persistencia;

import java.sql.Connection;
import java.sql.SQLException;

import javax.annotation.PostConstruct;
import javax.enterprise.context.ApplicationScoped;
import javax.enterprise.context.RequestScoped;
import javax.enterprise.inject.Default;
import javax.enterprise.inject.Disposes;
import javax.enterprise.inject.Produces;

import org.mariadb.jdbc.MySQLDataSource;

// Se crear� solo una vez durante la vida de la aplicaci�n
@ApplicationScoped
public class JdbcFactory {
	private MySQLDataSource datasource;
	
	// M�todo que se ejecuta inmediatamente despu�s del constructor
	@PostConstruct
	public void init() {
		System.out.println("Configura datasource");

		datasource = new MySQLDataSource();
		datasource.setUser("root");
		datasource.setPassword("55ec319");
		datasource.setDatabaseName("inventario");
		datasource.setPort(3306);
		datasource.setServerName("localhost");
	}
	
	// Produce un objeto conexi�n que dura solo una petici�n
	@Produces 
	@RequestScoped
	@Default
	public Connection create() throws SQLException{
		return datasource.getConnection();
	}
	
	// M�todo que se ejecuta al final de la vida de la petici�n
	public void free(@Disposes Connection connection) throws SQLException{
		connection.close();
	}
	
}
