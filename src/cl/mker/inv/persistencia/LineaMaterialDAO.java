package cl.mker.inv.persistencia;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.inject.Inject;

import cl.mker.inv.modelo.LineaMaterial;
import cl.mker.inv.modelo.Material;
import cl.mker.inv.modelo.Orden;

public class LineaMaterialDAO {

	@Inject
	private Connection connection;
	
	@PostConstruct
	private void init(){
		System.out.println("LineaMaterialDAO init");
	}

	public List<LineaMaterial> listaLineasMaterialPorIdOrden(Long idOrden) throws DAOException {
		List<LineaMaterial> lineasMaterial = new ArrayList<LineaMaterial>();
		CallableStatement stmt = null;
		ResultSet rs = null;
		LineaMaterial lineaMaterial = null;
		
		try {
			stmt = connection.prepareCall("{call listaLineasMaterialPorIdOrden(?)}");
			stmt.setLong("idOrden", idOrden);
			rs = stmt.executeQuery();
			
			while (rs.next()) {
				lineaMaterial = new LineaMaterial();
				lineaMaterial.setIdLineaMaterial(rs.getLong("idLineaMaterial"));
				lineaMaterial.setValor(rs.getInt("valor"));
				lineaMaterial.setCantidad(rs.getInt("cantidad"));
				
				lineasMaterial.add(lineaMaterial);
			}
			
			return lineasMaterial;
		} catch (Exception e){
			throw new DAOException(e);
		}
	}

	public Material buscaMaterialPorIdLineaMaterial(Long idLineaMaterial) throws DAOException {
		CallableStatement stmt = null;
		ResultSet rs = null;
		Material material = null;
		
		try {
			stmt = connection.prepareCall("{call buscaMaterialPorIdLineaMaterial(?)}");
			stmt.setLong("idLineaMaterial", idLineaMaterial);
			rs = stmt.executeQuery();
			
			if (rs.next()) {
				material = new Material();
				material.setIdMaterial(rs.getLong("idMaterial"));
				material.setValor(rs.getLong("valor"));
				material.setNombre(rs.getString("nombre"));
			}
			
			return material;
		} catch (Exception e){
			throw new DAOException(e);
		}
	}

	public List<Material> listaMaterialesFueraDeOrden(Long idOrden) throws DAOException {
		List<Material> materiales = new ArrayList<Material>();
		CallableStatement stmt = null;
		ResultSet rs = null;
		Material material = null;
		
		try {
			stmt = connection.prepareCall("{call listaMaterialesFueraDeOrden(?)}");
			stmt.setLong("idOrden", idOrden);
			rs = stmt.executeQuery();
			
			while (rs.next()) {
				material = new Material();
				material.setIdMaterial(rs.getLong("idMaterial"));
				material.setValor(rs.getLong("valor"));
				material.setNombre(rs.getString("nombre"));
				
				materiales.add(material);
			}
			
			return materiales;
		} catch (Exception e){
			throw new DAOException(e);
		}
	}
	
	public boolean agregaLineasMaterial(LineaMaterial lineaMaterial) throws DAOException {
		CallableStatement stmt = null;
		
		try{ 
			stmt = connection.prepareCall("{call insertaLineaMaterial(?,?,?,?)}");
			stmt.setLong("idOrden", lineaMaterial.getOrden().getIdOrden());
			stmt.setLong("idMaterial", lineaMaterial.getMaterial().getIdMaterial());
			stmt.setInt("cantidad", lineaMaterial.getCantidad());
			stmt.setLong("valor", lineaMaterial.getValor());
			
			return ( stmt.executeUpdate() > 0 ) ;
		} catch (Exception e){
			throw new DAOException(e);
		}
	}

	public boolean actualizaLineasMaterial(LineaMaterial lineaMaterial) throws DAOException {
		CallableStatement stmt = null;
		
		try{ 
			stmt = connection.prepareCall("{call actualizaLineaMaterial(?,?,?,?)}");
			stmt.setLong("idOrden", lineaMaterial.getOrden().getIdOrden());
			stmt.setLong("idMaterial", lineaMaterial.getMaterial().getIdMaterial());
			stmt.setInt("cantidad", lineaMaterial.getCantidad());
			stmt.setLong("valor", lineaMaterial.getValor());
			
			return ( stmt.executeUpdate() > 0 ) ;
		} catch (Exception e){
			throw new DAOException(e);
		}
	}

	public boolean eliminaLineasMaterial(LineaMaterial lineaMaterial) throws DAOException {
		CallableStatement stmt = null;
		
		try{ 
			stmt = connection.prepareCall("{call eliminaLineaMaterial(?)}");
			stmt.setLong("idLineaMaterial", lineaMaterial.getIdLineaMaterial());
			
			return ( stmt.executeUpdate() > 0 ) ;
		} catch (Exception e){
			throw new DAOException(e);
		}
	}

	public boolean eliminaLineasMaterialPorOrden(Orden orden) throws DAOException {
		CallableStatement stmt = null;
		
		try{ 
			stmt = connection.prepareCall("{call eliminaLineaMaterialPorOrden(?)}");
			stmt.setLong("idOrden", orden.getIdOrden());
			
			return ( stmt.executeUpdate() > 0 ) ;
		} catch (Exception e){
			throw new DAOException(e);
		}
	}

	public int sumaCantidadLineaMaterialPorMaterial(Material material) throws DAOException {
		int stock = 0;
		CallableStatement stmt = null;
		ResultSet rs = null;
		
		try {
			stmt = connection.prepareCall("{call sumaCantidadLineaMaterialPorMaterial(?)}");
			stmt.setLong("idMaterial", material.getIdMaterial());
			rs = stmt.executeQuery();
			
			while (rs.next()) {
				stock += rs.getInt("cantidad");
			}
			
			return stock;
		} catch (Exception e){
			throw new DAOException(e);
		}
	}

	public List<LineaMaterial> listaLineasMaterialPorMaterial(Material material) throws DAOException {
		List<LineaMaterial> lineasMaterial = new ArrayList<LineaMaterial>();
		CallableStatement stmt = null;
		ResultSet rs = null;
		LineaMaterial lineaMaterial = null;
		
		try {
			stmt = connection.prepareCall("{call listaLineasMaterialPorMaterial(?)}");
			stmt.setLong("idMaterial", material.getIdMaterial());
			rs = stmt.executeQuery();
			
			while( rs.next() ) {
				lineaMaterial = new LineaMaterial();
				lineaMaterial.setIdLineaMaterial(rs.getLong("idLineaMaterial"));
				lineaMaterial.setValor(rs.getLong("valor"));
				lineaMaterial.setCantidad(rs.getInt("cantidad"));
				
				lineasMaterial.add(lineaMaterial);
			}
			
			return lineasMaterial;
		} catch (Exception e){
			throw new DAOException(e);
		}
	}

}
