package cl.mker.inv.persistencia;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.util.HashSet;
import java.util.Set;

import javax.annotation.PostConstruct;
import javax.inject.Inject;

import cl.mker.inv.seguridad.Perfil;
import cl.mker.inv.seguridad.Permiso;

public class PermisoDAO {

	@Inject
	private Connection connection;
	
	@PostConstruct
	private void init(){
		System.out.println("PermisoDAO init");
	}
	
	public Set<Permiso> buscaPermisosPorPerfil(Perfil perfil) throws DAOException {
		Set<Permiso> permisos = new HashSet<Permiso>();

		CallableStatement stmt = null;
        ResultSet rs = null;
        Permiso permiso = null;
        
		try {
			stmt = connection.prepareCall("{call buscaPermisosPorPerfil(?)}");
			stmt.setLong("idPerfil", perfil.getIdPerfil());
			rs = stmt.executeQuery();
			
			while (rs.next()) {
				permiso = new Permiso();
				permiso.setIdPermiso(rs.getLong("idPermiso"));
				permiso.setAccion(rs.getString("accion"));
				permiso.setDescripcion(rs.getString("descripcion"));
				
				permisos.add(permiso);
            }
			
			return permisos;
		} catch (Exception e){
			throw new DAOException(e);
		}
	}
}
