package cl.mker.inv.persistencia;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.inject.Inject;

import cl.mker.inv.modelo.Factura;
import cl.mker.inv.modelo.Impuesto;

public class ImpuestoDAO {
	
	@Inject
	private Connection connection;

	public List<Impuesto> listaImpuestos() throws DAOException {
		List<Impuesto> impuestos = new ArrayList<Impuesto>();
	
		CallableStatement stmt = null;
		    ResultSet rs = null;
		    Impuesto impuesto = null;
		    
		try {
			stmt = connection.prepareCall("{call listaImpuestos()}");
			rs = stmt.executeQuery();
			
			while (rs.next()) {
				impuesto = new Impuesto();
				impuesto.setIdImpuesto(rs.getLong("idImpuesto"));
				impuesto.setPorcentaje(rs.getDouble("porcentaje"));
				impuesto.setFecha(new Date(rs.getDate("fecha").getTime()));
				
				impuestos.add(impuesto);
			}
			
			return impuestos;
		} catch (Exception e){
			throw new DAOException(e);
		}
	}
	
	public boolean registraImpuesto(Impuesto impuesto) throws DAOException {
		CallableStatement stmt = null;
        
		try {
			stmt = connection.prepareCall("{call insertaImpuesto(?,?)}");
			stmt.setDouble("porcentaje", impuesto.getPorcentaje());
			stmt.setDate("fecha", new java.sql.Date(impuesto.getFecha().getTime()));
			
			return ( stmt.executeUpdate() > 0 ) ;
		} catch (Exception e){
			throw new DAOException(e);
		}
	}
	
	public boolean actualizaImpuesto(Impuesto impuesto) throws DAOException {
		CallableStatement stmt = null;
		try {
			stmt = connection.prepareCall("{call actualizaImpuesto(?,?,?)}");
			stmt.setLong("idImpuesto", impuesto.getIdImpuesto());
			stmt.setDouble("porcentaje", impuesto.getPorcentaje());
			stmt.setDate("fecha", new java.sql.Date(impuesto.getFecha().getTime()));
			
			return ( stmt.executeUpdate() > 0 ) ;
		} catch (Exception e){
			throw new DAOException(e);
		}
	}
	
	public boolean eliminaImpuesto(Impuesto impuesto) throws DAOException {
		CallableStatement stmt = null;
        
		try {
			stmt = connection.prepareCall("{call eliminaImpuesto(?)}");
			stmt.setLong("idImpuesto", impuesto.getIdImpuesto());
			
			return ( stmt.executeUpdate() > 0 ) ;
		} catch (Exception e){
			throw new DAOException(e);
		}
	}

	public Impuesto buscaImpuestoPorFactura(Factura factura) throws DAOException {
		CallableStatement stmt = null;
		ResultSet rs = null;
		Impuesto impuesto = null;
		    
		try {
			stmt = connection.prepareCall("{call buscaImpuestoPorFactura(?)}");
			stmt.setLong("idFactura", factura.getIdFactura());
			rs = stmt.executeQuery();
			
			if( rs.next() ){
				impuesto = new Impuesto();
				impuesto.setIdImpuesto(rs.getLong("idImpuesto"));
				impuesto.setPorcentaje(rs.getDouble("porcentaje"));
				impuesto.setFecha(new Date(rs.getDate("fecha").getTime()));
			}
			
			return impuesto;
		} catch (Exception e){
			throw new DAOException(e);
		}
	}

}
