package cl.mker.inv.persistencia;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.inject.Inject;

import cl.mker.inv.modelo.Abono;
import cl.mker.inv.modelo.documento.ResumenFactura;

public class AbonoDAO {

	@Inject
	private Connection connection;
	
	@PostConstruct
	private void init(){
		System.out.println("ClienteDAO init");
	}
	
	public List<Abono> buscaAbonosPorFactura(ResumenFactura factura) throws DAOException {
		List<Abono> abonos = new ArrayList<Abono>();

		CallableStatement stmt = null;
        ResultSet rs = null;
        Abono abono = null;
        
		try {
			stmt = connection.prepareCall("{call buscaAbonosPorFactura(?)}");
			stmt.setLong("idFactura", factura.getIdFactura());
			rs = stmt.executeQuery();
			
			while (rs.next()) {
				abono = new Abono();
				abono.setIdAbono(rs.getLong("idAbono"));
				abono.setFecha(rs.getDate("fecha"));
				abono.setMonto(rs.getLong("monto"));
				
				abonos.add(abono);
            }
			
			return abonos;
		} catch (Exception e){
			throw new DAOException(e);
		}
	}
	
	public boolean eliminaAbono(Abono abono) throws DAOException {
		CallableStatement stmt = null;
        
		try {
			stmt = connection.prepareCall("{call eliminaAbono(?)}");
			stmt.setLong("idAbono", abono.getIdAbono());
			
			return ( stmt.executeUpdate() > 0 ) ;
		} catch (Exception e){
			throw new DAOException(e);
		}
	}

	public boolean insertaAbono(Abono abono, ResumenFactura resumenFactura) throws DAOException {
		CallableStatement stmt = null;
        
		try {
			stmt = connection.prepareCall("{call insertaAbono(?,?)}");
			stmt.setLong("idFactura", resumenFactura.getIdFactura());
			stmt.setLong("monto", abono.getMonto());
			
			return ( stmt.executeUpdate() > 0 ) ;
		} catch (Exception e){
			throw new DAOException(e);
		}
	}
}
