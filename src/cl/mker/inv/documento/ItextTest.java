package cl.mker.inv.documento;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.sql.SQLException;
import java.util.Date;
import java.util.List;

import cl.mker.inv.modelo.documento.StockMaterial;

import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.PageSize;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;

public class ItextTest {

	private List<StockMaterial> stockMateriales;
	
	private ByteArrayOutputStream baos = new ByteArrayOutputStream();
	
	private int stockCritico;
	
	public ItextTest(List<StockMaterial> stockMateriales, int stockCritico) {
		this.stockMateriales = stockMateriales;
		this.stockCritico = stockCritico;
	}
	
    /**
     * Creates a PDF document.
     * @param filename the path to the new PDF document
     * @throws    DocumentException 
     * @throws    IOException
     * @throws    SQLException
     */
    public ByteArrayOutputStream createPdf() throws DocumentException, IOException {
        // step 0
    	if( stockMateriales == null || stockMateriales.isEmpty() ){
    		return null;
    	}
    	// step 1
        Document document = new Document(PageSize.A4.rotate());
        // step 2
        PdfWriter writer = PdfWriter.getInstance(document, baos);
        writer.setCompressionLevel(0);
        // step 3
        
        document.open();
        // step 4
        // TODO : Revisar posicionamiento
        //Image img = Image.getInstance(getClass().getClassLoader().getResource("/logo_reporte.png"));
        //img.setAbsolutePosition(0, 0);
        //writer.getDirectContent().addImage(img);
        document.add(getTable());
        document.newPage();
        // step 5
        document.close();
        
        return baos;
    }
 
    /**
     * @param connection
     * @param day
     * @return
     * @throws SQLException
     * @throws DocumentException
     * @throws IOException
     */
    public PdfPTable getTable() throws DocumentException, IOException {
    	// Porporcion del ancho de las columna respecto a las demas
        PdfPTable table = new PdfPTable(new float[] { 1, 4, 1, 1, 1, 1 });
        // Asigna el total del ancho 100%
        table.setWidthPercentage(100f);
        
        table.getDefaultCell().setPadding(3);
        table.getDefaultCell().setUseAscender(true);
        table.getDefaultCell().setUseDescender(true);
        table.getDefaultCell().setColspan(6);
        
        table.getDefaultCell().setBackgroundColor(BaseColor.RED);
        table.getDefaultCell().setHorizontalAlignment(Element.ALIGN_CENTER);
        table.addCell(new Date().toString());
        
        table.getDefaultCell().setHorizontalAlignment(Element.ALIGN_LEFT);
        table.getDefaultCell().setColspan(1);
        table.getDefaultCell().setBackgroundColor(BaseColor.YELLOW);
        
        for (int i = 0; i < 1; i++) {
        	table.addCell("Id");
            table.addCell("Nombre");
            table.addCell("Vendidos");
            table.addCell("Comprados");
            table.addCell("Devueltos");
            table.addCell("Disponibles");
        }
        
        table.getDefaultCell().setBackgroundColor(null);
        table.setHeaderRows(2);

        for (StockMaterial stockMaterial : stockMateriales) {
        	table.addCell(String.valueOf(stockMaterial.getIdMaterial()));
        	table.addCell(stockMaterial.getNombreMaterial());
        	table.addCell(String.valueOf(stockMaterial.getCantidadVendidos()));
        	table.addCell(String.valueOf(stockMaterial.getCantidadComprados()));
        	table.addCell(String.valueOf(stockMaterial.getCantidadDevueltos()));
        	
        	if( stockMaterial.getCantidadDisponible() <= stockCritico ){
        		table.getDefaultCell().setBackgroundColor(BaseColor.RED);
        	}
        	
        	table.addCell(String.valueOf(stockMaterial.getCantidadDisponible()));
        	table.getDefaultCell().setBackgroundColor(null);
        }
        return table;
    }
 
    /**
     * Main method.
     * @param    args    no arguments needed
     * @throws DocumentException 
     * @throws IOException 
     * @throws SQLException
     */
//    public static void main(String[] args) throws DocumentException, IOException {
//    	ItextTest test = new ItextTest();
//    	ByteArrayInputStream bais = new ByteArrayInputStream(test.createPdf().toByteArray());
//    	new DefaultStreamedContent(bais, "image/png");
//    }
}
