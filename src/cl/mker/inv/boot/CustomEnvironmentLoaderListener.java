package cl.mker.inv.boot;

import javax.inject.Inject;
import javax.servlet.ServletContext;

import org.apache.shiro.authc.credential.HashedCredentialsMatcher;
import org.apache.shiro.crypto.hash.Sha256Hash;
import org.apache.shiro.mgt.RealmSecurityManager;
import org.apache.shiro.web.env.DefaultWebEnvironment;
import org.apache.shiro.web.env.EnvironmentLoaderListener;
import org.apache.shiro.web.env.WebEnvironment;

import cl.mker.inv.servicio.seguridad.UserRealm;

public class CustomEnvironmentLoaderListener extends EnvironmentLoaderListener {

    @Inject
    private UserRealm jpaRealm;

    @Override
    protected WebEnvironment createEnvironment(ServletContext pServletContext) {
        WebEnvironment environment = super.createEnvironment(pServletContext);
        RealmSecurityManager rsm = (RealmSecurityManager) environment.getSecurityManager();
        HashedCredentialsMatcher passwordMatcher = new HashedCredentialsMatcher();
        passwordMatcher.setHashAlgorithmName(Sha256Hash.ALGORITHM_NAME);
        jpaRealm.setCredentialsMatcher(passwordMatcher);
        rsm.setRealm(jpaRealm);
        ((DefaultWebEnvironment) environment).setSecurityManager(rsm);
        return environment;
    }

}