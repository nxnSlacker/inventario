package cl.mker.inv.seguridad;

import java.io.Serializable;
import java.util.Date;
import java.util.Set;

import javax.validation.constraints.NotNull;

public class Usuario implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -683719443340029103L;
	private Long idUsuario;
	@NotNull(message = "El nombre de usuario no puede estar vac�o")
	private String username;
	private String password;
	private Set<Perfil> perfiles;
	private boolean habilitado;
	private String codigoActivacion;
	private Date fechaActivacion;
	private boolean enviaMail = true;

	public Long getIdUsuario() {
		return idUsuario;
	}

	public void setIdUsuario(Long idUsuario) {
		this.idUsuario = idUsuario;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public Set<Perfil> getPerfiles() {
		return perfiles;
	}

	public void setPerfiles(Set<Perfil> perfiles) {
		this.perfiles = perfiles;
	}

	public boolean isHabilitado() {
		return habilitado;
	}
	
	public boolean getHabilitado(){
		return habilitado;
	}

	public void setHabilitado(boolean habilitado) {
		this.habilitado = habilitado;
	}
	
	public String getCodigoActivacion() {
		return codigoActivacion;
	}
	
	public void setCodigoActivacion(String codigoActivacion) {
		this.codigoActivacion = codigoActivacion;
	}
	
	public Date getFechaActivacion() {
		return fechaActivacion;
	}
	
	public void setFechaActivacion(Date fechaActivacion) {
		this.fechaActivacion = fechaActivacion;
	}
	
	public boolean isEnviaMail() {
		return enviaMail;
	}
	
	public void setEnviaMail(boolean enviaMail) {
		this.enviaMail = enviaMail;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((username == null) ? 0 : username.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Usuario other = (Usuario) obj;
		if (username == null) {
			if (other.username != null)
				return false;
		} else if (!username.equals(other.username))
			return false;
		return true;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("Usuario [idUsuario=");
		builder.append(idUsuario);
		builder.append(", username=");
		builder.append(username);
		builder.append(", password=");
		builder.append(password);
		builder.append(", perfiles=");
		builder.append(perfiles);
		builder.append(", habilitado=");
		builder.append(habilitado);
		builder.append(", codigoActivacion=");
		builder.append(codigoActivacion);
		builder.append(", fechaActivacion=");
		builder.append(fechaActivacion);
		builder.append(", enviaMail=");
		builder.append(enviaMail);
		builder.append("]");
		return builder.toString();
	}

}
