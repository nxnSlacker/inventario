package cl.mker.inv.seguridad;

import java.io.Serializable;
import java.util.Set;

public class Perfil implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 8227259465077908914L;
	private Long idPerfil;
	private String grupo;
	private Set<Permiso> permisos;

	public Long getIdPerfil() {
		return idPerfil;
	}

	public void setIdPerfil(Long idPerfil) {
		this.idPerfil = idPerfil;
	}

	public String getGrupo() {
		return grupo;
	}

	public void setGrupo(String grupo) {
		this.grupo = grupo;
	}

	public Set<Permiso> getPermisos() {
		return permisos;
	}

	public void setPermisos(Set<Permiso> permisos) {
		this.permisos = permisos;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((grupo == null) ? 0 : grupo.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Perfil other = (Perfil) obj;
		if (grupo == null) {
			if (other.grupo != null)
				return false;
		} else if (!grupo.equals(other.grupo))
			return false;
		return true;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("Perfil [idPerfil=");
		builder.append(idPerfil);
		builder.append(", grupo=");
		builder.append(grupo);
		builder.append(", permisos=");
		builder.append(permisos);
		builder.append("]");
		return builder.toString();
	}

}
