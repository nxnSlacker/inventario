package cl.mker.inv.web.convertidores;

import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.ConverterException;
import javax.faces.convert.FacesConverter;

import cl.mker.inv.util.ValidaCampo;
import cl.mker.inv.util.ValidadorException;

@FacesConverter(value = "rutConverter")
public class RutConverter implements Converter{

	@Override
	public Object getAsObject(FacesContext facesContext, UIComponent uiComponent, String rut) {
		try {
			if( rut == null || rut.isEmpty() ){
				return rut;
			}
			
			return ValidaCampo.validaRut(rut);
		} catch (ValidadorException e) {
			FacesMessage msg = 	new FacesMessage("Formto rut.", "Formato de rut invalido.");
			msg.setSeverity(FacesMessage.SEVERITY_ERROR);
			throw new ConverterException(msg);
		}
	}

	@Override
	public String getAsString(FacesContext facesContext, UIComponent uiComponent, Object rut) {
		return rut.toString();
	}

}
