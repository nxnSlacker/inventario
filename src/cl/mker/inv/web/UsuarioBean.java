package cl.mker.inv.web;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.inject.Inject;

import org.primefaces.context.RequestContext;

import cl.mker.inv.seguridad.Perfil;
import cl.mker.inv.seguridad.Usuario;
import cl.mker.inv.servicio.PerfilService;
import cl.mker.inv.servicio.ServiceException;
import cl.mker.inv.servicio.UsuarioService;

@ManagedBean
@ViewScoped
public class UsuarioBean implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1601019801012261294L;
	private List<String> perfiles = new ArrayList<String>();
	private List<Usuario> usuarios;
	private List<String> perfilesSeleccionados = new ArrayList<String>();
	private List<Usuario> usuariosFiltrados;
	
	private Map<String, Perfil> mapaPerfiles = new HashMap<String, Perfil>();
	@Inject
	private UsuarioService usuarioService;
	
	@Inject 
	private PerfilService perfilService;
	
	private Usuario usuarioSeleccionado;
	
	@PostConstruct
	private void init(){
		try {
			usuarios = usuarioService.listaUsuarios();
			
			for (Perfil perfil : perfilService.listaPerfiles()) {
				perfiles.add(perfil.getGrupo());
				mapaPerfiles.put(perfil.getGrupo(), perfil);
			}
		} catch (ServiceException e) {
			e.printStackTrace();
		}
	}
	
	public List<Usuario> getUsuarios() {
		return usuarios;
	}
	
	public void setUsuarios(List<Usuario> usuarios) {
		this.usuarios = usuarios;
	}
	
	public List<Usuario> getUsuariosFiltrados() {
		return usuariosFiltrados;
	}
	
	public void setUsuariosFiltrados(List<Usuario> usuariosFiltrados) {
		this.usuariosFiltrados = usuariosFiltrados;
	}
	
	public List<String> getPerfilesSeleccionados() {
		return perfilesSeleccionados;
	}
	
	public void setPerfilesSeleccionados(List<String> perfilesSeleccionados) {
		this.perfilesSeleccionados = perfilesSeleccionados;
	}
	
	public List<String> getPerfiles() {
		return perfiles;
	}
	
	public void setPerfiles(List<String> perfiles) {
		this.perfiles = perfiles;
	}
	
	public Usuario getUsuarioSeleccionado() {
		return usuarioSeleccionado;
	}
	
	public void setUsuarioSeleccionado(Usuario usuarioSeleccionado) {
		this.usuarioSeleccionado = usuarioSeleccionado;
		perfilesSeleccionados.clear();
		for (Perfil perfil : usuarioSeleccionado.getPerfiles()) {
			perfilesSeleccionados.add(perfil.getGrupo());
		}
	}
	
	public void asignaNuevoUsuario(){
		usuarioSeleccionado = new Usuario();
		usuarioSeleccionado.setPerfiles(new HashSet<Perfil>());
		perfilesSeleccionados.clear();
	}
	
	public void eliminaUsuario(Usuario usuarioSeleccionado){
		System.out.println("Elimina usuario " + usuarioSeleccionado);
		
		try {
			usuarioService.eliminaUsuario(usuarioSeleccionado);
			usuarios.remove(usuarioSeleccionado);
		} catch (ServiceException e) {
			e.printStackTrace();
		}
	}
	
	public void registraUsuario(){
		System.out.println("Usuario seleccionado " + usuarioSeleccionado);
		List<Perfil> perfilesEliminar = new ArrayList<Perfil>();
		List<Perfil> perfilesAgregar = new ArrayList<Perfil>();
		
		for (Perfil perfil : usuarioSeleccionado.getPerfiles()) {
			if( ! perfilesSeleccionados.contains(perfil.getGrupo()) ){
				perfilesEliminar.add(perfil);
			}
		}

		for (String grupoPerfil : perfilesSeleccionados) {
			Perfil perfil = mapaPerfiles.get(grupoPerfil);
			if( ! usuarioSeleccionado.getPerfiles().contains(perfil) ){
				perfilesAgregar.add(perfil);
			}
		}
		
		
		try {
			usuarioService.registraUsuario(usuarioSeleccionado);
			perfilService.agregaPerfiles(usuarioSeleccionado, perfilesAgregar);
			perfilService.eliminaPerfiles(usuarioSeleccionado, perfilesEliminar);
			
			usuarioSeleccionado.getPerfiles().addAll(perfilesAgregar);
			usuarioSeleccionado.getPerfiles().removeAll(perfilesEliminar);
			
			if( ! usuarios.contains(usuarioSeleccionado) ){
				usuarios.add(usuarioSeleccionado);
			}
		} catch (ServiceException e) {
			e.printStackTrace();
		}
		
		RequestContext.getCurrentInstance().update("form1");
		RequestContext.getCurrentInstance().update("form0");
	}
}
