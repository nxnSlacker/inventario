package cl.mker.inv.web;

import java.io.ByteArrayInputStream;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.inject.Inject;

import org.primefaces.context.RequestContext;
import org.primefaces.model.DefaultStreamedContent;
import org.primefaces.model.StreamedContent;

import cl.mker.inv.modelo.Cliente;
import cl.mker.inv.modelo.Factura;
import cl.mker.inv.modelo.Orden;
import cl.mker.inv.servicio.ClienteService;
import cl.mker.inv.servicio.FacturaService;
import cl.mker.inv.servicio.ImpuestoService;
import cl.mker.inv.servicio.OrdenService;
import cl.mker.inv.servicio.ServiceException;
import cl.mker.inv.servicio.documento.DetalleFacturaService;

@ManagedBean
@ViewScoped
public class FacturaBean implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 8317549383008673845L;
	@Inject
	private FacturaService facturaService;
	@Inject
	private OrdenService ordenService;
	@Inject 
	private ClienteService clienteService;
	@Inject
	private ImpuestoService impuestoService;
	@Inject
	private DetalleFacturaService detalleFacturaService;
	
	private List<Factura> facturas;
	
	private List<Factura> facturasFiltradas;
	
	private Factura facturaSeleccionada;
	private Cliente clienteSeleccionado;	
	
	private List<Orden> listadoOrdenes;
	private List<Orden> listadoOrdenesEnFactura;
	private List<Orden> listadoOrdenesFueraDeFactura;
	private List<Orden> listadoEliminar = new ArrayList<Orden>();
	
	private boolean clientesOcultos = false;
	
	@PostConstruct
	private void init(){
		System.out.println("OrdenBean init");
		
		try {
			facturas = facturaService.listaFacturas();
		} catch (ServiceException e) {
			e.printStackTrace();
		}
	}
	
	public boolean isFacturaExiste(){
		System.out.println(facturaSeleccionada);
		return facturaSeleccionada.getIdFactura() != null;
	}
	
	public List<Factura> getFacturas() {
		return facturas;
	}
	
	public void setFacturas(List<Factura> facturas) {
		this.facturas = facturas;
	}
	
	public List<Factura> getFacturasFiltradas() {
		return facturasFiltradas;
	}
	
	public void setFacturasFiltradas(List<Factura> facturasFiltradas) {
		this.facturasFiltradas = facturasFiltradas;
	}
	
	public Cliente getClienteSeleccionado() {
		return clienteSeleccionado;
	}
	
	public void setClienteSeleccionado(Cliente clienteSeleccionado) {
		this.clienteSeleccionado = clienteSeleccionado;
		
		try {
			if( clienteSeleccionado == null ){
				System.out.println("HAHAAAA");
				return;
			}
			
			listadoEliminar = new ArrayList<Orden>();
			listadoOrdenes = new ArrayList<Orden>();
			listadoOrdenesEnFactura = new ArrayList<Orden>(); 
			listadoOrdenesFueraDeFactura = ordenService.listaOrdenesSinFacturaPorCliente(clienteSeleccionado);
			
			for (Orden orden: listadoOrdenesFueraDeFactura) {
				orden.setFactura(facturaSeleccionada);
			}
			
		} catch (ServiceException e) {
			e.printStackTrace();
		}
	}
	
	public List<Orden> getListadoOrdenes() {
		return listadoOrdenes;
	}
	
	public void setListadoOrdenes(List<Orden> listadoOrdenes) {
		this.listadoOrdenes = listadoOrdenes;
	}
	
	public List<Orden> getListadoOrdenesEnFactura() {
		return listadoOrdenesEnFactura;
	}
	
	public void setListadoOrdenesEnFactura(List<Orden> listadoOrdenesEnFactura) {
		this.listadoOrdenesEnFactura = listadoOrdenesEnFactura;
	}
	
	public List<Orden> getListadoOrdenesFueraDeFactura() {
		return listadoOrdenesFueraDeFactura;
	}
	
	public void setListadoOrdenesFueraDeFactura(List<Orden> listadoOrdenesFueraDeFactura) {
		this.listadoOrdenesFueraDeFactura = listadoOrdenesFueraDeFactura;
	}
	
	public Factura getFacturaSeleccionada() {
		return facturaSeleccionada;
	}
	
	public boolean isClientesOcultos() {
		return clientesOcultos;
	}
	
	public void setClientesOcultos(boolean clientesOcultos) {
		this.clientesOcultos = clientesOcultos;
	}
	
	public void setFacturaSeleccionada(Factura facturaSeleccionada) {
		this.facturaSeleccionada = facturaSeleccionada;
		try {
			listadoOrdenes = ordenService.listaOrdenesPorFactura(facturaSeleccionada);
			listadoOrdenesEnFactura = new ArrayList<Orden>();
			listadoOrdenesEnFactura.addAll(listadoOrdenes);
			
			this.clienteSeleccionado = facturaService.buscaClientePorFactura(facturaSeleccionada);
			
			listadoOrdenesFueraDeFactura = ordenService.listaOrdenesSinFacturaPorCliente(clienteSeleccionado);
			
			clientesOcultos = true;
			
			calculaTotal();
		} catch (ServiceException e){
			e.printStackTrace();
		}
	}
	
	public List<Cliente> buscaClientePorRut(String rut){
		List<Cliente> clientesFiltrados = new ArrayList<Cliente>();

		try {
			List<Cliente> clientes = clienteService.listaClientes();
			
			for (Cliente cliente : clientes) {
				if(cliente.getRut().toLowerCase().trim().startsWith(rut)){
					clientesFiltrados.add(cliente);
				}
			}
		} catch (ServiceException e) {
			e.printStackTrace();
		}
		
		return clientesFiltrados;
	}
	
	public void mueveAgregar(Orden orden){
		listadoOrdenesEnFactura.add(orden);
		listadoOrdenesFueraDeFactura.remove(orden);
		
		listadoEliminar.remove(orden);
		calculaTotal();
	}
	
	public void mueveEliminar(Orden orden){
		listadoOrdenesFueraDeFactura.add(orden);
		listadoOrdenesEnFactura.remove(orden);
		
		if( listadoOrdenes.contains(orden) ){
			listadoEliminar.add(orden);
		}
		calculaTotal();
	}
	
	public long calculaTotalConImpuestoPorOrden(Orden orden){
		if ( orden == null ){
			return 0;
		}
		return (long) (orden.getTotalNeto() * ( 1 + facturaSeleccionada.getImpuesto().getPorcentaje() / 100));
	}
	
	public long calculaTotal(){
		long total = 0;
		for (Orden orden : listadoOrdenesEnFactura) {
			total += orden.getTotalNeto();
		}
		
		return total;
	}
	
	public long calculaTotalConImpuesto(){
		long total = 0;
		for (Orden orden : listadoOrdenesEnFactura) {
			total += calculaTotalConImpuestoPorOrden(orden);
		}
		
		return total;
	}
	
	public void asignaNuevaFactura(){
		clienteSeleccionado = null;
		facturaSeleccionada = new Factura();
		try {
			facturaSeleccionada.setImpuesto(impuestoService.recuperaImpuestoActual());
		} catch (ServiceException e) {
			e.printStackTrace();
		}
		
		clientesOcultos = false;
		listadoEliminar = new ArrayList<Orden>();
		listadoOrdenes = new ArrayList<Orden>();
		listadoOrdenesEnFactura = new ArrayList<Orden>();
		listadoOrdenesFueraDeFactura = new ArrayList<Orden>();
	}
	
	public void registraFactura(){
		if( clienteSeleccionado == null ){
			FacesContext context = FacesContext.getCurrentInstance();
		    context.addMessage(null, new FacesMessage("Failed",  "Debe escoger a un cliente."));
		    return;
		}
		
		if( facturaSeleccionada.getNumeroFactura() == null || facturaSeleccionada.getNumeroFactura().isEmpty() ){
			FacesContext context = FacesContext.getCurrentInstance();
		    context.addMessage(null, new FacesMessage("Failed",  "Debe asignar un n�mero de factura."));
		    return;
		}
		
		if( listadoOrdenesEnFactura.size() <= 0 ){
			FacesContext context = FacesContext.getCurrentInstance();
		    context.addMessage(null, new FacesMessage("Failed",  "No puede existir una factura sin ordenes."));
		    return;
		}
		
		if( existeNumeroFacturaEnOtraFactura() ){ 
			FacesContext context = FacesContext.getCurrentInstance();
		    context.addMessage(null, new FacesMessage("Failed",  "El n�mero de factura ya lo tiene otra factura."));
		    return;
		}
		
		try {
			if( facturaSeleccionada.getIdFactura() == null ){
				facturaService.regsitraFactura(facturaSeleccionada);
			} else {
				facturaService.actualizaFactura(facturaSeleccionada);
			}
			
			FacesContext context = FacesContext.getCurrentInstance();
		    context.addMessage(null, new FacesMessage("Successful",  "Factura registrada."));
		}catch(ServiceException e){
			FacesContext context = FacesContext.getCurrentInstance();
		    context.addMessage(null, new FacesMessage("Failed",  "Factura no registrada."));
		}
		
		for (Orden orden : listadoOrdenesEnFactura) {
			orden.setFactura(facturaSeleccionada);
		}

		actualizaOrdenes();
		
		descartaOrdenes();
		
		eliminaFactura();
		
		try {
			facturas = facturaService.listaFacturas();
			for (Factura factura : facturas) {
				factura.setImpuesto(impuestoService.buscaImpuestoPorFactura(factura));
			}
		} catch (ServiceException e) {
			e.printStackTrace();
		}
		
		RequestContext.getCurrentInstance().update("form0");
		RequestContext.getCurrentInstance().execute("PF('dialog').hide()");
	}
	
	private boolean existeNumeroFacturaEnOtraFactura(){
		Factura facturaPorNumero = null;
		try {
			facturaPorNumero = facturaService.buscaFacturaPorNumero(Long.valueOf(facturaSeleccionada.getNumeroFactura()));
		} catch (NumberFormatException e) {
			return false;
		} catch (ServiceException e) {
			return false;
		}
		
		if( facturaPorNumero == null){
			return false;
		}
		
		if( ! facturaPorNumero.getIdFactura().equals(facturaSeleccionada.getIdFactura())){
			return true;
		}
		
		return false;
	}

	public void eliminaFactura(Factura facturaSeleccionada){
		try {
			facturaService.descartaOrdenesParaFactura(facturaSeleccionada);
			facturaService.eliminaFactura(facturaSeleccionada);
			
			FacesContext context = FacesContext.getCurrentInstance();
		    context.addMessage(null, new FacesMessage("Successful",  "Factura eliminada."));
			
			try {
				facturas = facturaService.listaFacturas();
				for (Factura factura : facturas) {
					factura.setImpuesto(impuestoService.buscaImpuestoPorFactura(factura));
				}
			} catch (ServiceException e) {
				e.printStackTrace();
			}
		} catch (ServiceException e) {
			FacesContext context = FacesContext.getCurrentInstance();
		    context.addMessage(null, new FacesMessage("Failed",  "La factura no se elimin�."));
		}
	}
	
	private void eliminaFactura() {
		if( listadoOrdenesEnFactura.isEmpty() ){
			try {
				facturaService.eliminaFactura(facturaSeleccionada);
			} catch (ServiceException e) {
				e.printStackTrace();
			}
		}
	}

	private void descartaOrdenes() {
		try {
			ordenService.descartaFacturaParaOrdenes(listadoEliminar);
		} catch (ServiceException e) {
			e.printStackTrace();
		}
	}

	private void actualizaOrdenes() {
		try {
			ordenService.actualizaFacturaParaOrdenes(listadoOrdenesEnFactura);
		} catch (ServiceException e) {
			e.printStackTrace();
		}
	}
	
	public StreamedContent getContent() {
		if( facturaSeleccionada.getIdFactura() == null ){
			return null;
		}
		
		try {
			return new DefaultStreamedContent(new ByteArrayInputStream(detalleFacturaService.buscaContenidoDetalleFactura(facturaSeleccionada).toByteArray()), "application/pdf", "resumen facturas.pdf");  
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return null;
	}
}
