package cl.mker.inv.web;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.inject.Inject;

import org.primefaces.context.RequestContext;
import org.primefaces.event.CellEditEvent;

import cl.mker.inv.modelo.LineaSurtido;
import cl.mker.inv.modelo.Proveedor;
import cl.mker.inv.modelo.Surtido;
import cl.mker.inv.servicio.LineaSurtidoService;
import cl.mker.inv.servicio.ProveedorService;
import cl.mker.inv.servicio.ServiceException;
import cl.mker.inv.servicio.SurtidoService;

@ManagedBean
@ViewScoped
public class SurtidoBean implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 3054541792672257950L;

	@Inject
	private SurtidoService surtidoService;
	@Inject 
	private ProveedorService proveedorService;
	@Inject
	private LineaSurtidoService lineaSurtidoService;
	
	private List<LineaSurtido> listadoMaterialesEnSurtidoFiltrados;
	private List<LineaSurtido> listadoMaterialesFueraDeSurtidoFiltrados;
	
	private List<Surtido> surtidosFiltrados;
	
	private List<Surtido> surtidos;
	private Surtido surtidoSeleccionado;
	private Proveedor proveedorSeleccionado;	
	
	private List<LineaSurtido> listadoLineaSurtido;
	private List<LineaSurtido> listadoMaterialesEnSurtido;
	private List<LineaSurtido> listadoMaterialesFueraDeSurtido;
	
	private List<LineaSurtido> listadoEliminar = new ArrayList<LineaSurtido>();
	
	@PostConstruct
	private void init(){
		System.out.println("SurtidoBean init");
		
		try {
			surtidos = surtidoService.listaSurtidos();
		} catch (ServiceException e) {
			e.printStackTrace();
		}
	}
	
	public List<LineaSurtido> getListadoMaterialesFueraDeSurtidoFiltrados() {
		return listadoMaterialesFueraDeSurtidoFiltrados;
	}

	public void setListadoMaterialesFueraDeSurtidoFiltrados(
			List<LineaSurtido> listadoMaterialesFueraDeSurtidoFiltrados) {
		this.listadoMaterialesFueraDeSurtidoFiltrados = listadoMaterialesFueraDeSurtidoFiltrados;
	}

	public List<LineaSurtido> getListadoMaterialesEnSurtidoFiltrados() {
		return listadoMaterialesEnSurtidoFiltrados;
	}

	public void setListadoMaterialesEnSurtidoFiltrados(
			List<LineaSurtido> listadoMaterialesEnSurtidoFiltrados) {
		this.listadoMaterialesEnSurtidoFiltrados = listadoMaterialesEnSurtidoFiltrados;
	}

	public List<Surtido> getSurtidosFiltrados() {
		return surtidosFiltrados;
	}
	
	public void setSurtidosFiltrados(List<Surtido> surtidosFiltrados) {
		this.surtidosFiltrados = surtidosFiltrados;
	}
	
	public List<LineaSurtido> getListadoMaterialesEnSurtido() {
		return listadoMaterialesEnSurtido;
	}
	
	public void setListadoMaterialesEnSurtido(List<LineaSurtido> listadoMaterialesEnSurtido) {
		this.listadoMaterialesEnSurtido = listadoMaterialesEnSurtido;
	}
	
	public List<LineaSurtido> getListadoMaterialesFueraDeSurtido() {
		return listadoMaterialesFueraDeSurtido;
	}
	
	public void setListadoMaterialesFueraDeSurtido(List<LineaSurtido> listadoMaterialesFueraDeSurtido) {
		this.listadoMaterialesFueraDeSurtido = listadoMaterialesFueraDeSurtido;
	}
	
	public Proveedor getProveedorSeleccionado() {
		return proveedorSeleccionado;
	}
	
	public void setProveedorSeleccionado(Proveedor proveedorSeleccionado) {
		this.proveedorSeleccionado = proveedorSeleccionado;
	}
	
	public List<Surtido> getSurtidos() {
		return surtidos;
	}
	
	public void setSurtidos(List<Surtido> surtidos) {
		this.surtidos = surtidos;
	}
	
	public void onCellEdit(CellEditEvent event) {
        Object oldValue = event.getOldValue();
        Object newValue = event.getNewValue();
        
        if(newValue != null && !newValue.equals(oldValue)) {
            FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_INFO, "Cell Changed", "Old: " + oldValue + ", New:" + newValue);
            FacesContext.getCurrentInstance().addMessage(null, msg);
        }
    }
	
	public void setSurtidoSeleccionado(Surtido surtidoSeleccionado) {
		this.surtidoSeleccionado = surtidoSeleccionado;
		this.proveedorSeleccionado = surtidoSeleccionado.getProveedor();
		
		try {
			listadoLineaSurtido = lineaSurtidoService.listaLineaSurtido(surtidoSeleccionado);
			listadoMaterialesEnSurtido = new ArrayList<LineaSurtido>();
			listadoMaterialesEnSurtido.addAll(listadoLineaSurtido);
			listadoMaterialesFueraDeSurtido = lineaSurtidoService.listaLineaSurtidoFalsa(surtidoSeleccionado);
		} catch (ServiceException e){
			
		}
	}
	
	public Surtido getSurtidoSeleccionado() {
		return surtidoSeleccionado;
	}
	
	public void eliminaSurtido(Surtido surtido){
		try {
			surtidoService.eliminaSurtido(surtido);
			surtidos.remove(surtido);
			FacesContext context = FacesContext.getCurrentInstance();
			context.addMessage(null, new FacesMessage("Successful",  "Surtido eliminado."));
		} catch (ServiceException e) {
			FacesContext context = FacesContext.getCurrentInstance();
			context.addMessage(null, new FacesMessage("Failed",  "No se puedo eliminar el surtido."));
		}
		
		//RequestContext.getCurrentInstance().update("growl");
	}
	
	public void asignaNuevoSurtido(){
		setSurtidoSeleccionado(new Surtido());
	}
	
	public void registraSurtido(){
		if( surtidoSeleccionado.getNumeroFactura() == null || surtidoSeleccionado.getNumeroFactura() <= 0){
			FacesContext context = FacesContext.getCurrentInstance();
	        context.addMessage(null, new FacesMessage("Failed",  "Ingrese un n�mero de factura mayor a 0."));
	        RequestContext.getCurrentInstance().update(":frm:tablas");
			return;
		}
		
		if( proveedorSeleccionado == null ){
			FacesContext context = FacesContext.getCurrentInstance();
	        context.addMessage(null, new FacesMessage("Failed",  "Escoja un proveedor."));
	        RequestContext.getCurrentInstance().update(":frm:tablas");
			return;
		}
		
		if( listadoMaterialesEnSurtido.size() <= 0 ){
			FacesContext context = FacesContext.getCurrentInstance();
		    context.addMessage(null, new FacesMessage("Failed",  "No se pueden registrar o modificar surtidos vac�os.") );
		    RequestContext.getCurrentInstance().update(":frm:tablas");
		    return;
		}
		
		if( existeNumeroFacturaEnOtraFactura() ){ 
			FacesContext context = FacesContext.getCurrentInstance();
		    context.addMessage(null, new FacesMessage("Failed",  "El n�mero de factura ya lo tiene otra factura."));
		    return;
		}
		
		List<LineaSurtido> listadoAgregar = new ArrayList<LineaSurtido>();
		for ( LineaSurtido lineaSurtido : listadoMaterialesEnSurtido ) {
			if( lineaSurtido.getNuevaCantidad() > 0 && lineaSurtido.getValor() > 0){
				listadoAgregar.add(lineaSurtido);
			}
		}
		
		if( listadoAgregar.size() <= 0 ){
			FacesContext context = FacesContext.getCurrentInstance();
		    context.addMessage(null, new FacesMessage("Failed",  "No se pueden registrar surtidos sin cantidad o valor.") );
		    RequestContext.getCurrentInstance().update(":frm:tablas");
		    return;
		}
		
		surtidoSeleccionado.setProveedor(proveedorSeleccionado);
		
		try {
			surtidoService.agregaSurtido(surtidoSeleccionado);
		} catch (ServiceException e) {
			FacesContext context = FacesContext.getCurrentInstance();
	        context.addMessage(null, new FacesMessage("Failed",  "No se pudo registrar el surtido."));
			RequestContext.getCurrentInstance().update(":frm:tablas");
			return;
		}
		
		try {
			lineaSurtidoService.agregaLineasSurtido(listadoAgregar);
		} catch (ServiceException e) {
			FacesContext context = FacesContext.getCurrentInstance();
	        context.addMessage(null, new FacesMessage("Failed",  "No se pudieron registrar las lineas de surtidos."));
			RequestContext.getCurrentInstance().update(":frm:tablas");
			return;
		}

		try {
			lineaSurtidoService.eliminaLineasSurtido(listadoEliminar);
		} catch (ServiceException e) {
			FacesContext context = FacesContext.getCurrentInstance();
	        context.addMessage(null, new FacesMessage("Failed",  "No se pudieron eliminar las lineas de surtido."));
			RequestContext.getCurrentInstance().update(":frm:tablas");
			return;
		}
		
		FacesContext context = FacesContext.getCurrentInstance();
        context.addMessage(null, new FacesMessage("Successful",  "Operacion completada.") );
		
        try {
			surtidos = surtidoService.listaSurtidos();
		} catch (ServiceException e) {
			e.printStackTrace();
		}
        
		RequestContext.getCurrentInstance().execute("PF('dialog').hide()");
		RequestContext.getCurrentInstance().update("frm");
	}
	
	private boolean existeNumeroFacturaEnOtraFactura(){
		Surtido surtidoPorNumero = null;
		try {
			surtidoPorNumero = surtidoService.buscaSurtidoPorNumero(surtidoSeleccionado.getNumeroFactura());
		} catch (NumberFormatException e) {
			return false;
		} catch (ServiceException e) {
			return false;
		}
		
		if( surtidoPorNumero == null){
			return false;
		}
		
		if( ! surtidoPorNumero.getIdSurtido().equals(surtidoSeleccionado.getIdSurtido())){
			return true;
		}
		
		return false;
	}
	
	public List<Proveedor> buscaProveedor(String razonSocial){
		List<Proveedor> proveedoresFiltrados = new ArrayList<Proveedor>();

		try {
			List<Proveedor> proveedores = proveedorService.listaProveedores();
			
			for (Proveedor proveedor : proveedores) {
				if(proveedor.getRazonSocial().toLowerCase().contains(razonSocial)){
					proveedoresFiltrados.add(proveedor);
				}
			}
		} catch (ServiceException e) {
			e.printStackTrace();
		}
		
		return proveedoresFiltrados;
	}
	
	public void mueveAgregar(LineaSurtido lineaSurtido){
		listadoMaterialesEnSurtido.add(lineaSurtido);
		listadoMaterialesFueraDeSurtido.remove(lineaSurtido);
		
		listadoEliminar.remove(lineaSurtido);
		filtra();
	}
	
	public void mueveEliminar(LineaSurtido lineaSurtido){
		listadoMaterialesFueraDeSurtido.add(lineaSurtido);
		listadoMaterialesEnSurtido.remove(lineaSurtido);
		
		if( listadoLineaSurtido.contains(lineaSurtido) ){
			listadoEliminar.add(lineaSurtido);
		}
		filtra();
	}
	
	private void filtra(){
		RequestContext.getCurrentInstance().execute("PF('listadoMaterialesEnSurtidoWV').filter()");
		RequestContext.getCurrentInstance().execute("PF('listadoMaterialesFueraDeSurtidoWV').filter()");
	}
	
}
