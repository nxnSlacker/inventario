package cl.mker.inv.web;

import java.io.Serializable;

import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.ConverterException;
import javax.inject.Inject;
import javax.inject.Named;

import cl.mker.inv.modelo.Cliente;
import cl.mker.inv.servicio.ClienteService;
import cl.mker.inv.servicio.ServiceException;

@Named
public class ClienteConverter implements Converter, Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -8267486130550989240L;
	@Inject
	private ClienteService clienteService; 
	
	@Override
	public Object getAsObject(FacesContext facesContext, UIComponent uiComponent, String value) {
		if(value != null && value.trim().length() > 0) {
			try {
				Cliente cliente = clienteService.buscaClientePorRut(value);
				return cliente;
			} catch (NumberFormatException nfe){
				return null;
			} catch (ServiceException e) {
				throw new ConverterException(new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error de conversion", "No es un proveedor valido."));
			}
		} else {
			return null;
		}
	}

	@Override
	public String getAsString(FacesContext facesContext, UIComponent uiComponent, Object object) {
		if(object != null) {
            return String.valueOf(((Cliente) object).getRut());
        }
        else {
            return null;
        }
	}

}
