package cl.mker.inv.web;

import java.io.Serializable;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.inject.Inject;

import org.primefaces.context.RequestContext;

import cl.mker.inv.modelo.Cliente;
import cl.mker.inv.servicio.ClienteService;
import cl.mker.inv.servicio.OrdenService;
import cl.mker.inv.servicio.ServiceException;

// Marca a la clase como un Controlador
@ManagedBean
// Indica que mientras las peticiones se hagan desde la misma p�gina esta solo se cargr� una sola vez
@ViewScoped
public class ClienteBean implements Serializable {

	// Variable generada por Eclipse cuando se implementa la clase Serializable
	// Se requiere para que JSF funcione adecuadamente
	private static final long serialVersionUID = -2215031272311472541L;

	// ############################################
	// Variables instanciadas automaticamente
	// ############################################
	
	@Inject
	private ClienteService clienteService;
	
	@Inject
	private OrdenService ordenService;
	
	// ############################################
	// Variables 
	// ############################################
	
	// La lista de clientes que se muestra en la grilla de la p�gina
	private List<Cliente> clientes;
	// Cuando se usa un filtro en la grilla, automaticamente se se llena esta lista con los datos encontrados y luego mostrada
	private List<Cliente> clientesFiltrados;
	// Almacena el cliente seleccionado a traves del boton de modificar de la grilla
	private Cliente clienteSeleccionado;
	
	// ############################################
	// Metodos de acceso para las variables 
	// ############################################
	
	public List<Cliente> getClientes() {
		return clientes;
	}
	
	public List<Cliente> getClientesFiltrados() {
		return clientesFiltrados;
	}
	
	public void setClientesFiltrados(List<Cliente> clientesFiltrados) {
		this.clientesFiltrados = clientesFiltrados;
	}
	
	public void setClienteSeleccionado(Cliente clienteSeleccionado) {
		this.clienteSeleccionado = clienteSeleccionado;
	}
	
	public Cliente getClienteSeleccionado() {
		return clienteSeleccionado;
	}
	
	//############################################
	// Se ejecuta cuandom se carga la p�gina
	//############################################
	@PostConstruct
	private void init(){
		try {
			// Asigna a la variable clientes (una lista) los datos recibidos desde el servicio 
			clientes = clienteService.listaClientes();
		} catch (ServiceException e) {
			// Solo deber�a darse este error si hay problemas desde la base de datos
			FacesContext context = FacesContext.getCurrentInstance();
		    context.addMessage(null, new FacesMessage("Failed",  "No se pudo cargar la lista de clientes."));
		}
	}
	
	//############################################
	// Acciones de la p�gina
	//############################################
	
	// Cauando se procede a crear un nuevo cliente, se asigna uno nuevo
	public void asignaNuevoCliente(){
		setClienteSeleccionado(new Cliente());
	}
	
	// Registra un nuevo cliente
	public void registraCliente(){
		try {
			// Busca un cliente que tenga el rut indicado
			Cliente cliente = clienteService.buscaClientePorRut(clienteSeleccionado.getRut());
			
			// Valida que el cliente no exista previamente
			if( cliente != null && cliente.getIdCliente() != clienteSeleccionado.getIdCliente() ){
				FacesMessage msg = new FacesMessage("Ya existe un cliente con ese rut");
				FacesContext.getCurrentInstance().addMessage(null, msg);
				return;
			}
			
			// Registra al cliente
			if( clienteService.registraCliente(clienteSeleccionado) ){
				asignaNuevoCliente();
				clientes = clienteService.listaClientes();
				// Oculta el dialogo de la p�gina
				RequestContext.getCurrentInstance().execute("PF('dialog').hide()");
				// Actualiza el formulario de la p�gina
				RequestContext.getCurrentInstance().update("form0");
			}
			
			FacesContext context = FacesContext.getCurrentInstance();
			context.addMessage(null, new FacesMessage("Succsessful",  "Cliente registrado."));
		} catch (ServiceException e) {
			FacesContext context = FacesContext.getCurrentInstance();
			context.addMessage(null, new FacesMessage("Failed",  "No se pudo registrar al cliente."));
		}
	}

	// Elimina un cliente
	public void eliminaCliente(Cliente cliente){
		try {
			// Busca ordenes que el cliente pueda tener
			if( ordenService.tieneOrdenes(cliente) ){
				FacesContext contextoJSF = FacesContext.getCurrentInstance();
			    contextoJSF.addMessage(null, new FacesMessage("Failed",  "Elimine las ordenes del cliente en primer lugar."));
				return;
			}
			
			// Elimina al cliente desde la base de datos
			clienteService.eliminaCliente(cliente);
			// Elimina al cliente desde la lista de la grilla
			clientes.remove(cliente);
			
			// Imprime mensaje satisfactorio
			FacesContext contextoJSF = FacesContext.getCurrentInstance();
		    contextoJSF.addMessage(null, new FacesMessage("Succsessful",  "Cliente eliminado."));
		} catch (ServiceException e) {
			FacesContext context = FacesContext.getCurrentInstance();
		    context.addMessage(null, new FacesMessage("Failed",  "No se pudo eliminar al cliente."));
		}
		
		// Actualiza la grilla con la lista actualizada modificados. El nombre del formulario es "form0"
		RequestContext.getCurrentInstance().update("form0");
	}
	
}
