package cl.mker.inv.web;

import java.io.Serializable;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.inject.Inject;

import org.primefaces.context.RequestContext;

import cl.mker.inv.modelo.Devolucion;
import cl.mker.inv.modelo.LineaMaterial;
import cl.mker.inv.modelo.Orden;
import cl.mker.inv.servicio.DevolucionService;
import cl.mker.inv.servicio.LineaMaterialService;
import cl.mker.inv.servicio.OrdenService;
import cl.mker.inv.servicio.ServiceException;

@ManagedBean
@ViewScoped
public class DevolucionBean implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 7167164727385719133L;
	@Inject
	private OrdenService ordenService;
	@Inject 
	private LineaMaterialService lineaMaterialService;
	@Inject
	private DevolucionService devolucionService;
	
	private List<Orden> ordenes;
	private Orden ordenSeleccionada;
	
	private List<LineaMaterial> lineasMaterial;
	
	private Devolucion devolucionSeleccionada;
	private LineaMaterial lineaMaterialSeleccionada;
	
	private List<Devolucion> devolucionesFiltradas;
	
	@PostConstruct
	private void init(){
		System.out.println("DevolucionBean init");
		
		try {
			ordenes = ordenService.listaOrdenes();
		} catch (ServiceException e) {
			e.printStackTrace();
		}
	}
	
	public List<Devolucion> getDevolucionesFiltradas() {
		return devolucionesFiltradas;
	}
	
	public void setDevolucionesFiltradas(List<Devolucion> devolucionesFiltradas) {
		this.devolucionesFiltradas = devolucionesFiltradas;
	}
	
	public List<LineaMaterial> getLineasMaterial() {
		return lineasMaterial;
	}
	
	public void setLineasMaterial(List<LineaMaterial> lineasMaterial) {
		this.lineasMaterial = lineasMaterial;
	}
	
	public List<Orden> getOrdenes() {
		return ordenes;
	}
	
	public void setOrdenes(List<Orden> ordenes) {
		this.ordenes = ordenes;
	}
	
	public void setOrdenSeleccionada(Orden ordenSeleccionada) {
		this.ordenSeleccionada = ordenSeleccionada;
		
		try {
			lineasMaterial = lineaMaterialService.listaLineaMaterial(ordenSeleccionada);
		} catch (ServiceException e){
			e.printStackTrace();
		}
	}
	
	public Orden getOrdenSeleccionada() {
		return ordenSeleccionada;
	}
	
	public void setLineaMaterialSeleccionada(LineaMaterial lineaMaterialSeleccionada) {
		this.lineaMaterialSeleccionada = lineaMaterialSeleccionada;
		devolucionSeleccionada = new Devolucion();
		devolucionSeleccionada.setLineaMaterial(lineaMaterialSeleccionada);
	}
	
	public LineaMaterial getLineaMaterialSeleccionada() {
		return lineaMaterialSeleccionada;
	}
	
	public Devolucion getDevolucionSeleccionada() {
		return devolucionSeleccionada;
	}
	
	public void setDevolucionSeleccionada(Devolucion devolucionSeleccionada) {
		this.devolucionSeleccionada = devolucionSeleccionada;
	}
	
	public int cantidadMaxima(Devolucion devolucionSeleccionada){
		int devueltos = 0;
		for(Devolucion devolucion : devolucionSeleccionada.getLineaMaterial().getDevoluciones()){
			if( devolucionSeleccionada == devolucion ){
				continue;
			}
			
			devueltos += devolucion.getCantidad();
		}
		
		return devolucionSeleccionada.getLineaMaterial().getCantidad() - devueltos;
	}
	
	public void eliminaDevolucion(Devolucion devolucion){
		try {
			devolucionService.eliminaDevolucion(devolucion);
			
			FacesContext context = FacesContext.getCurrentInstance();
		    context.addMessage(null, new FacesMessage("Successful",  "Devolucion eliminada."));
			
			lineasMaterial = lineaMaterialService.listaLineaMaterial(ordenSeleccionada);
			RequestContext.getCurrentInstance().update(":form1:tablaDevoluciones");
		} catch (ServiceException e) {
			e.printStackTrace();
		}
	}
	
	public void registraDevolucion(){
		if( devolucionSeleccionada.getMotivo() == null || devolucionSeleccionada.getMotivo().isEmpty() ){
			FacesContext context = FacesContext.getCurrentInstance();
			context.addMessage(null, new FacesMessage("Failed",  "El motivo no puede estar vac�o."));
			RequestContext.getCurrentInstance().update(":form2:growl");
			return;
		}
		
		if( devolucionSeleccionada.getLineaMaterial().getCantidad() == null || devolucionSeleccionada.getLineaMaterial().getCantidad() <= 0 ){
			FacesContext context = FacesContext.getCurrentInstance();
		    context.addMessage(null, new FacesMessage("Failed",  "Debe ingresar una cantidad positiva."));
		    RequestContext.getCurrentInstance().update(":form2:growl");
		    return;
		}
		
		int cantidadDisponible = devolucionSeleccionada.getLineaMaterial().getCantidad();
		for (Devolucion devolucion : devolucionSeleccionada.getLineaMaterial().getDevoluciones()) {
			if( devolucion == devolucionSeleccionada ){
				continue;
			}
			cantidadDisponible -= devolucion.getCantidad();
		}
		
		if( devolucionSeleccionada.getCantidad() > cantidadDisponible ){
			FacesContext context = FacesContext.getCurrentInstance();
		    context.addMessage(null, new FacesMessage("Failed",  "No se pueden devolver mas materiales que la cantidad disponible."));
		    RequestContext.getCurrentInstance().update(":form2:growl");
		    return;
		}
		
		try {
			devolucionService.registraDevolucion(devolucionSeleccionada);
			FacesContext context = FacesContext.getCurrentInstance();
		    context.addMessage(null, new FacesMessage("Successful",  "Devolucion registrada."));
		} catch (ServiceException e) {
			FacesContext context = FacesContext.getCurrentInstance();
		    context.addMessage(null, new FacesMessage("Failed",  "No se pudo registrar la devolucion."));
		    return;
		}
		
		try {
			lineasMaterial = lineaMaterialService.listaLineaMaterial(ordenSeleccionada);
			devolucionSeleccionada = null;
			RequestContext.getCurrentInstance().execute("PF('dialog1').hide()");
			RequestContext.getCurrentInstance().update("form2");
		} catch (ServiceException e) {
			e.printStackTrace();
		}
	}
}
