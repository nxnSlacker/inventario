package cl.mker.inv.web;

import javax.enterprise.inject.Model;

import org.apache.shiro.SecurityUtils;
import org.apache.shiro.subject.Subject;

@Model
public class CuentaBean {

	public String logout() {
        Subject currentUser = SecurityUtils.getSubject();
        try {
            currentUser.logout();
        } catch (Exception e) {
            
        }
        
        return "/views/login.xhtml";
    }
}
