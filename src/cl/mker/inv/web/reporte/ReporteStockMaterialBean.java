package cl.mker.inv.web.reporte;

import java.io.ByteArrayInputStream;
import java.io.Serializable;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.inject.Inject;

import org.primefaces.model.DefaultStreamedContent;
import org.primefaces.model.StreamedContent;

import cl.mker.inv.documento.ItextTest;
import cl.mker.inv.modelo.documento.StockMaterial;
import cl.mker.inv.servicio.ServiceException;
import cl.mker.inv.servicio.documento.StockMaterialService;

@ManagedBean
@ViewScoped
public class ReporteStockMaterialBean implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 6188913067997376794L;

	private List<StockMaterial> stockMateriales;
	
	private int stockCritico = 3;
	
	@Inject
	private StockMaterialService stockMaterialService;
	
	@PostConstruct
	private void init(){
		System.out.println("ReporteStockMaterialBean init");
		try {
			stockMateriales = stockMaterialService.listaStockMateriales();
		} catch (ServiceException e) {
			e.printStackTrace();
		}
	}
	
	public StreamedContent getContent() {
		try {
			ItextTest test = new ItextTest(stockMateriales, stockCritico);
			return new DefaultStreamedContent(new ByteArrayInputStream(test.createPdf().toByteArray()), "application/pdf", "stock materiales.pdf");  
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
    }
	
	public int getStockCritico() {
		return stockCritico;
	}
	
	public void setStockCritico(int stockCritico) {
		this.stockCritico = stockCritico;
	}
	
	public List<StockMaterial> getStockMateriales() {
		return stockMateriales;
	}
	
	public void setStockMateriales(List<StockMaterial> stockMateriales) {
		this.stockMateriales = stockMateriales;
	}
	
}
