package cl.mker.inv.web.reporte;

import java.io.ByteArrayInputStream;
import java.io.Serializable;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.inject.Inject;

import org.primefaces.model.DefaultStreamedContent;
import org.primefaces.model.StreamedContent;

import cl.mker.inv.modelo.documento.ResumenFactura;
import cl.mker.inv.servicio.ResumenFacturaService;
import cl.mker.inv.servicio.ServiceException;
import cl.mker.inv.servicio.documento.ReporteResumenFacturaService;

@ManagedBean
@ViewScoped
public class ReporteResumenFacturaBean implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -439892889758941946L;

	private List<ResumenFactura> resumenFacturas;
	
	@Inject
	private ResumenFacturaService resumenFacturaService;
	
	@Inject
	private ReporteResumenFacturaService reporteResumenFacturaService;
	
	private int diasMoroso = 30;
	
	@PostConstruct
	private void init(){
		System.out.println("ReporteResumenFacturaBean init");
		try {
			resumenFacturas = resumenFacturaService.listaResumenFacturas();
		} catch (ServiceException e) {
			e.printStackTrace();
		}
	}
	
	public List<ResumenFactura> getResumenFacturas() {
		return resumenFacturas;
	}
	
	public void setResumenFacturas(List<ResumenFactura> resumenFacturas) {
		this.resumenFacturas = resumenFacturas;
	}
	
	public int getDiasMoroso() {
		return diasMoroso;
	}
	
	public void setDiasMoroso(int diasMoroso) {
		this.diasMoroso = diasMoroso;
	}
	
	public StreamedContent getContent() {
		try {
			return new DefaultStreamedContent(new ByteArrayInputStream(reporteResumenFacturaService.listaResumenFacturas(resumenFacturas, diasMoroso).toByteArray()), "application/pdf", "resumen facturas.pdf");  
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return null;
	}
	
}
