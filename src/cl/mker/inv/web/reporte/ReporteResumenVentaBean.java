package cl.mker.inv.web.reporte;

import java.io.ByteArrayInputStream;
import java.util.Date;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.inject.Inject;

import org.primefaces.model.DefaultStreamedContent;
import org.primefaces.model.StreamedContent;

import cl.mker.inv.modelo.documento.ResumenVenta;
import cl.mker.inv.servicio.ServiceException;
import cl.mker.inv.servicio.documento.ReporteResumenVentaService;
import cl.mker.inv.servicio.documento.ResumenVentaService;

@ManagedBean
@ViewScoped
public class ReporteResumenVentaBean {

	private List<ResumenVenta> resumenVentas;
	
	@Inject
	private ResumenVentaService resumenVentaService;
	
	@Inject
	private ReporteResumenVentaService reporteResumenVentaService;
	
	private Date fechaInicio;
	private Date fechaFin;

	@PostConstruct
	private void init(){
		fechaInicio = new Date();
		fechaFin = new Date();
		
		try {
			resumenVentaService.listaResumenVentas(fechaInicio, fechaFin);
		} catch (ServiceException e){
			e.printStackTrace();
		}
	}
	
	public Date getFechaInicio() {return fechaInicio; }
	public void setFechaInicio(Date fechaInicio) { this.fechaInicio = fechaInicio; }
	public Date getFechaFin() {	return fechaFin; }
	public void setFechaFin(Date fechaFin) { this.fechaFin = fechaFin; }
	public List<ResumenVenta> getResumenVentas() { return resumenVentas; }
	public void setResumenVentas(List<ResumenVenta> resumenVentas) { this.resumenVentas = resumenVentas; }
	
	public void actualizaFecha(){
		try {
			resumenVentas = resumenVentaService.listaResumenVentas(fechaInicio, fechaFin);
		} catch (ServiceException e){
			e.printStackTrace();
		}
	}
	
	public StreamedContent getContent() {
		try {
			return new DefaultStreamedContent(new ByteArrayInputStream(reporteResumenVentaService.listaResumenFacturas(resumenVentas).toByteArray()), "application/pdf", "resumen ventas.pdf");  
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return null;
	}
}
