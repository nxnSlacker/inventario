package cl.mker.inv.web;

import java.io.Serializable;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

import org.primefaces.context.RequestContext;

import cl.mker.inv.modelo.Admin;

@ManagedBean
@ViewScoped
public class AdminBean implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 8574225538319502767L;
	private Admin adminSeleccionado;

	@PostConstruct
	private void init(){
		System.out.println("@PostConstruct " + getClass().getSimpleName());
		
		adminSeleccionado = new Admin();
	}
	
	public Admin getAdminSeleccionado() {
		return adminSeleccionado;
	}
	
	public void setAdminSeleccionado(Admin adminSeleccionado) {
		this.adminSeleccionado = adminSeleccionado;
	}

	public void asignaNuevoAdmin(){
		adminSeleccionado = new Admin();
	}
	
	public void registraAdmin(){
		System.out.println("registraAdmin()");
		
		RequestContext.getCurrentInstance().execute("PF('dialog').hide()");
	}
	
}
