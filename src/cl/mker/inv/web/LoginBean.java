package cl.mker.inv.web;

import java.io.IOException;
import java.io.Serializable;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authc.AuthenticationException;
import org.apache.shiro.authc.IncorrectCredentialsException;
import org.apache.shiro.authc.LockedAccountException;
import org.apache.shiro.authc.UnknownAccountException;
import org.apache.shiro.authc.UsernamePasswordToken;
import org.apache.shiro.subject.Subject;
import org.apache.shiro.web.util.WebUtils;

@ManagedBean
@ViewScoped
public class LoginBean implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 3389542315752370689L;
	private String username;
    private String password;
    private Boolean rememberMe;
    
    public void doLogin() {
        Subject subject = SecurityUtils.getSubject();
        
        UsernamePasswordToken token = new UsernamePasswordToken(getUsername(), getPassword(), getRememberMe());

        try {
            subject.login(token);

            FacesContext fc = FacesContext.getCurrentInstance();
            fc.responseComplete();
            HttpServletRequest request = (HttpServletRequest) fc.getExternalContext().getRequest();
            HttpServletResponse response = (HttpServletResponse) fc.getExternalContext().getResponse();
            String fallbackUrl = "app/index.xhtml";
            WebUtils.redirectToSavedRequest(request, response, fallbackUrl);
        } catch (UnknownAccountException ex) {
            facesError("Cuenta desconocida");
        } catch (IncorrectCredentialsException ex) {
            facesError("Contraseņa equivocada");
        } catch (LockedAccountException ex) {
            facesError("Cuenta bloqueada");
        } catch (AuthenticationException | IOException ex) {
            facesError("Error desconocido");
        } finally {
            token.clear();
        }
    }
    
    public void verificaSesionIniciada(){
    	Subject subject = SecurityUtils.getSubject();
    	try {
    		if( subject.isAuthenticated() ){
    			ExternalContext ec = FacesContext.getCurrentInstance().getExternalContext();
    			ec.redirect(ec.getRequestContextPath() +"/views/app/index.xhtml");
    		}
		} catch (IOException e) {
			e.printStackTrace();
		}
    }
    
    private void facesError(String message) {
        FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, message, null));
    }
    
    public String getUsername() {
        return username;
    }

    public void setUsername(String login) {
        this.username = login;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String senha) {
        this.password = senha;
    }

    public Boolean getRememberMe() {
        return rememberMe;
    }

    public void setRememberMe(Boolean lembrar) {
        this.rememberMe = lembrar;
    }
}
