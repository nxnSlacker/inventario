package cl.mker.inv.web;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.faces.event.ValueChangeEvent;
import javax.inject.Inject;

import org.primefaces.component.datatable.DataTable;
import org.primefaces.context.RequestContext;
import org.primefaces.event.CellEditEvent;

import cl.mker.inv.modelo.Cliente;
import cl.mker.inv.modelo.Devolucion;
import cl.mker.inv.modelo.Factura;
import cl.mker.inv.modelo.Impuesto;
import cl.mker.inv.modelo.LineaMaterial;
import cl.mker.inv.modelo.Orden;
import cl.mker.inv.servicio.ClienteService;
import cl.mker.inv.servicio.FacturaService;
import cl.mker.inv.servicio.ImpuestoService;
import cl.mker.inv.servicio.LineaMaterialService;
import cl.mker.inv.servicio.OrdenService;
import cl.mker.inv.servicio.ServiceException;

@ManagedBean
@ViewScoped
public class OrdenBean implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 3054541792672257950L;

	@Inject
	private OrdenService ordenService;
	@Inject 
	private ClienteService clienteService;
	@Inject
	private LineaMaterialService lineaMaterialService;
	@Inject
	private ImpuestoService impuestoService;
	@Inject
	private FacturaService facturaService;
	
	private List<LineaMaterial> listadoMaterialesEnOrdenFiltrados;
	private List<LineaMaterial> listadoMaterialesFueraDeOrdenFiltrados;
	
	private List<Orden> ordenesFiltradas;
	
	private List<Orden> ordenes;
	private Orden ordenSeleccionada;
	private Cliente clienteSeleccionado;	
	
	private List<LineaMaterial> listadoLineaMaterial;
	private List<LineaMaterial> listadoMaterialesEnOrden;
	private List<LineaMaterial> listadoMaterialesFueraDeOrden;
	
	private List<LineaMaterial> listadoEliminar = new ArrayList<LineaMaterial>();
	
	private long totalNeto = 0L;
	
	private Integer cantidad;
	
	private Impuesto impuesto;


	@PostConstruct
	private void init(){
		System.out.println("OrdenBean init");
		
		try {
			ordenes = ordenService.listaOrdenes();
		} catch (ServiceException e) {
			e.printStackTrace();
		}
	}
	
	public List<LineaMaterial> getListadoMaterialesEnOrdenFiltrados() {
		return listadoMaterialesEnOrdenFiltrados;
	}
	
	public void setListadoMaterialesEnOrdenFiltrados(
			List<LineaMaterial> listadoMaterialesEnOrdenFiltrados) {
		this.listadoMaterialesEnOrdenFiltrados = listadoMaterialesEnOrdenFiltrados;
	}
	
	public List<LineaMaterial> getListadoMaterialesFueraDeOrdenFiltrados() {
		return listadoMaterialesFueraDeOrdenFiltrados;
	}

	public void setListadoMaterialesFueraDeOrdenFiltrados(
			List<LineaMaterial> listadoMaterialesFueraDeOrdenFiltrados) {
		this.listadoMaterialesFueraDeOrdenFiltrados = listadoMaterialesFueraDeOrdenFiltrados;
	}

	public List<Orden> getOrdenesFiltradas() {
		return ordenesFiltradas;
	}
	
	public void setOrdenesFiltradas(List<Orden> ordenesFiltradas) {
		this.ordenesFiltradas = ordenesFiltradas;
	}
	
	public List<LineaMaterial> getListadoMaterialesEnOrden() {
		return listadoMaterialesEnOrden;
	}
	
	public void setListadoMaterialesEnOrden(List<LineaMaterial> listadoMaterialesEnOrden) {
		this.listadoMaterialesEnOrden = listadoMaterialesEnOrden;
	}
	
	public List<LineaMaterial> getListadoMaterialesFueraDeOrden() {
		return listadoMaterialesFueraDeOrden;
	}
	
	public void setListadoMaterialesFueraDeOrden(List<LineaMaterial> listadoMaterialesFueraDeOrden) {
		this.listadoMaterialesFueraDeOrden = listadoMaterialesFueraDeOrden;
	}
	
	public Cliente getClienteSeleccionado() {
		return clienteSeleccionado;
	}
	
	public void setClienteSeleccionado(Cliente clienteSeleccionado) {
		this.clienteSeleccionado = clienteSeleccionado;
	}
	
	public List<Orden> getOrdenes() {
		return ordenes;
	}
	
	public void setOrdenes(List<Orden> ordenes) {
		this.ordenes = ordenes;
	}
	
	public void setTotalNeto(long totalNeto) {
		this.totalNeto = totalNeto;
	}
	
	public long getTotalNeto() {
		return totalNeto;
	}
	
	public Impuesto getImpuesto() {
		return impuesto;
	}
	
	public void setImpuesto(Impuesto impuesto) {
		this.impuesto = impuesto;
	}
	
	public void onCellEdit(CellEditEvent event) {
		DataTable tabla =(DataTable) event.getSource();
		LineaMaterial lineaMaterial = (LineaMaterial)tabla.getRowData();
		
		System.out.println("Linea Seleccionada: " + lineaMaterial);
		
		if( Integer.valueOf((String) event.getNewValue().toString()) <= 0){
			lineaMaterial.setCantidad(lineaMaterial.getCantidadAnterior());
			mueveEliminar(lineaMaterial);
		}
		
		System.out.println(event.getNewValue());
		System.out.println(event.getOldValue());
		
		Object oldValue = event.getOldValue();
        Object newValue = event.getNewValue();
        
        if(newValue != null && !newValue.equals(oldValue)) {
            FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_INFO, "Cell Changed", "Old: " + oldValue + ", New:" + newValue);
            FacesContext.getCurrentInstance().addMessage(null, msg);
        }
        
        calculaTotal();
    }
	
	public void setOrdenSeleccionada(Orden ordenSeleccionada) {
		this.ordenSeleccionada = ordenSeleccionada;
		this.clienteSeleccionado = ordenSeleccionada.getCliente();
		
		try {
			listadoLineaMaterial = lineaMaterialService.listaLineaMaterial(ordenSeleccionada);
			listadoMaterialesEnOrden = new ArrayList<LineaMaterial>();
			listadoMaterialesEnOrden.addAll(listadoLineaMaterial);
			listadoMaterialesFueraDeOrden = lineaMaterialService.listaLineaMaterialFalsa(ordenSeleccionada);
			
			Factura factura = facturaService.buscaFacturaPorOrden(ordenSeleccionada);
			if( factura != null ){
				impuesto = impuestoService.buscaImpuestoPorFactura(factura);
			} else {
				impuesto = impuestoService.recuperaImpuestoActual();
			}
			
			calculaTotal();
		} catch (ServiceException e){
			e.printStackTrace();
		}
	}
	
	public Orden getOrdenSeleccionada() {
		return ordenSeleccionada;
	}
	
	public void eliminaOrden(Orden orden){
		try {
			if( facturaService.tieneFactura(orden) ){
				FacesContext context = FacesContext.getCurrentInstance();
			    context.addMessage(null, new FacesMessage("Failed",  "Descarte la orden desde su factura en primer lugar."));
			    return;
			}
			
			ordenService.eliminaOrden(orden);
			ordenes.remove(orden);
			
			FacesContext context = FacesContext.getCurrentInstance();
		    context.addMessage(null, new FacesMessage("Successful",  "Orden eliminada."));
		} catch (ServiceException e) {
			FacesContext context = FacesContext.getCurrentInstance();
		    context.addMessage(null, new FacesMessage("Failed",  "No se pudo eliminar la orden."));
		}
	}
	
	public void asignaNuevaOrden(){
		try {
			impuesto = impuestoService.recuperaImpuestoActual();
		} catch (ServiceException e) {
			e.printStackTrace();
		}
		setOrdenSeleccionada(new Orden());
	}
	
	public void calculaTotal(){
		totalNeto = 0L;
		for (LineaMaterial lineaMaterial : listadoMaterialesEnOrden) {
			totalNeto += lineaMaterial.getCantidad() * lineaMaterial.getMaterial().getValor();
		}
	}
	
	public void registraOrden(){
		// Valida que exista un cliente seleccionado
		if( clienteSeleccionado == null ){
			FacesContext context = FacesContext.getCurrentInstance();
	        context.addMessage(null, new FacesMessage("Failed",  "Escoja un cliente."));
	        RequestContext.getCurrentInstance().update(":form:tablas");
			return;
		}
		
		if( listadoMaterialesEnOrden.size() <= 0){
			FacesContext context = FacesContext.getCurrentInstance();
		    context.addMessage(null, new FacesMessage("Failed",  "No se pueden registrar o modificar ordenen vacias.") );
		    RequestContext.getCurrentInstance().update(":form:tablas");
		    return;
		}
		
		List<LineaMaterial> listadoAgregar = new ArrayList<LineaMaterial>();
		for ( LineaMaterial lineaMaterial : listadoMaterialesEnOrden ) {
			if( lineaMaterial.getCantidad() > 0 ){
				listadoAgregar.add(lineaMaterial);
			}
		}
		
		if( listadoAgregar.size() <= 0 ){
			FacesContext context = FacesContext.getCurrentInstance();
		    context.addMessage(null, new FacesMessage("Failed",  "No se pueden registrar materiales sin cantidad.") );
		    RequestContext.getCurrentInstance().update(":form:tablas");
		    return;
		}
		
		ordenSeleccionada.setCliente(clienteSeleccionado);
		
		try {
			ordenService.agregaOrden(ordenSeleccionada);
		} catch (ServiceException e) {
			FacesContext context = FacesContext.getCurrentInstance();
	        context.addMessage(null, new FacesMessage("Failed",  "No se pudo registrar la orden."));
			RequestContext.getCurrentInstance().update(":form:tablas");
			return;
		}
		
		try {
			lineaMaterialService.agregaLineasMaterial(listadoAgregar);
		} catch (ServiceException e) {
			FacesContext context = FacesContext.getCurrentInstance();
	        context.addMessage(null, new FacesMessage("Failed",  "No se pudieron registrar las lineas de materiales."));
			RequestContext.getCurrentInstance().update(":form:tablas");
			return;
		}

		try {
			lineaMaterialService.eliminaLineasMaterial(listadoEliminar);
		} catch (ServiceException e) {
			FacesContext context = FacesContext.getCurrentInstance();
	        context.addMessage(null, new FacesMessage("Failed",  "No se pudieron eliminar las lineas de materiales."));
			RequestContext.getCurrentInstance().update(":form:tablas");
			return;
		}
		
		FacesContext context = FacesContext.getCurrentInstance();
        context.addMessage(null, new FacesMessage("Successful",  "Operacion completada.") );
		
        try {
			ordenes = ordenService.listaOrdenes();
		} catch (ServiceException e) {
			e.printStackTrace();
		}
        
		RequestContext.getCurrentInstance().execute("PF('dialog').hide()");
		RequestContext.getCurrentInstance().update("frm");
	}
	
	public List<Cliente> buscaClientePorRut(String rut){
		List<Cliente> clientesFiltrados = new ArrayList<Cliente>();

		try {
			List<Cliente> clientes = clienteService.listaClientes();
			
			for (Cliente cliente : clientes) {
				if(cliente.getRut().toLowerCase().trim().startsWith(rut)){
					clientesFiltrados.add(cliente);
				}
			}
		} catch (ServiceException e) {
			e.printStackTrace();
		}
		
		return clientesFiltrados;
	}
	
	public void mueveAgregar(LineaMaterial lineaMaterial){
		listadoMaterialesEnOrden.add(lineaMaterial);
		listadoMaterialesFueraDeOrden.remove(lineaMaterial);
		
		listadoEliminar.remove(lineaMaterial);
		calculaTotal();
		filtra();
	}
	
	public void mueveEliminar(LineaMaterial lineaMaterial){
		if( lineaMaterial.getDevoluciones() != null && ! lineaMaterial.getDevoluciones().isEmpty() ){
			FacesContext context = FacesContext.getCurrentInstance();
			context.addMessage(null, new FacesMessage("Failed",  "Los materiales con devoluciones se eliminan desde su propio mantenedor."));
			return;
		}

		listadoMaterialesFueraDeOrden.add(lineaMaterial);
		listadoMaterialesEnOrden.remove(lineaMaterial);
		
		if( listadoLineaMaterial.contains(lineaMaterial) ){
			listadoEliminar.add(lineaMaterial);
		}
		
		calculaTotal();
		filtra();
	}
	
	private void filtra(){
		RequestContext.getCurrentInstance().execute("PF('listadoMaterialesEnOrdenWV').filter()");
		RequestContext.getCurrentInstance().execute("PF('listadoMaterialesFueraDeOrdenWV').filter()");
	}
	
	public String limpiaFiltros(){
		RequestContext.getCurrentInstance().execute("PF('listadoMaterialesEnOrdenWV').clearFilters()");
		RequestContext.getCurrentInstance().execute("PF('listadoMaterialesFueraDeOrdenWV').clearFilters()");
		
		//RequestContext.getCurrentInstance().execute("$('#frm\\:tablaExporter5\\:globalFilter').val('').keyup();");
		//RequestContext.getCurrentInstance().execute("$('#frm\\:tablaExporter5\\:globalFilter').val('').keyup();");
		
		//$('#frm\\:myTab\\:globalFilter').val('').keyup(); return false;
		return null;
	}
	
	public void updateCantidad(ValueChangeEvent event) {
		  cantidad = (Integer) event.getNewValue();
	}
	
	public void refreshCantidad(LineaMaterial lineaMaterial) {
		  lineaMaterial.setCantidad(cantidad);
	}
	
	public int cantidadMinima(LineaMaterial lineaMaterial){
		int stock = 0;
		if( lineaMaterial.getDevoluciones() == null){
			return 0;
		}
		
		for (Devolucion devolucion : lineaMaterial.getDevoluciones()) {
			stock += devolucion.getCantidad();
		}
		
		return stock;
	}
	
	public int cantidadMaxima(LineaMaterial lineaMaterial){
		int cantidadActual = cantidadActual(lineaMaterial);
		if( cantidadActual >= 0 ){
			return cantidadActual + lineaMaterial.getCantidadAnterior();
		}
		
		return lineaMaterial.getCantidadAnterior();
	}
	
	public String stockActual(LineaMaterial lineaMaterial){
		int cantidadActual = cantidadActual(lineaMaterial);
		if( cantidadActual >= 0 ){
			String.valueOf(cantidadMaxima(lineaMaterial) - lineaMaterial.getCantidad());
		}
		return String.valueOf(lineaMaterial.getCantidadAnterior() - lineaMaterial.getCantidad() + cantidadActual);
	}
	
	public int cantidadActual(LineaMaterial lineaMaterial){
		return lineaMaterial.getMaterial().getStockProveedor() - lineaMaterial.getMaterial().getStockCliente();
	}
}
