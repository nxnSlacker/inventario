package cl.mker.inv.web;

import java.io.Serializable;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.inject.Inject;

import org.primefaces.context.RequestContext;

import cl.mker.inv.modelo.Proveedor;
import cl.mker.inv.servicio.ProveedorService;
import cl.mker.inv.servicio.ServiceException;
import cl.mker.inv.servicio.SurtidoService;

@ManagedBean
@ViewScoped
public class ProveedorBean implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 7387980243695912149L;

	@Inject
	private ProveedorService proveedorService;
	@Inject
	private SurtidoService surtidoService;
	
	private List<Proveedor> proveedores;
	private List<Proveedor> proveedoresFiltrados;
	private Proveedor proveedorSeleccionado;
	
	@PostConstruct
	private void init(){
		System.out.println("@PostConstruct " + getClass().getSimpleName());
		try {
			proveedores = proveedorService.listaProveedores();
		} catch (ServiceException e) {
			e.printStackTrace();
		}
	}
	
	public List<Proveedor> getProveedores() {
		return proveedores;
	}
	
	public List<Proveedor> getProveedoresFiltrados() {
		return proveedoresFiltrados;
	}
	
	public void setProveedoresFiltrados(List<Proveedor> proveedoresFiltrados) {
		this.proveedoresFiltrados = proveedoresFiltrados;
	}
	
	public void setProveedorSeleccionado(Proveedor proveedorSeleccionado) {
		this.proveedorSeleccionado = proveedorSeleccionado;
	}
	
	public Proveedor getProveedorSeleccionado() {
		return proveedorSeleccionado;
	}
	
	public void eliminaProveedor(Proveedor proveedor){
		try {
			if( surtidoService.tieneSurtido(proveedor) ){
				FacesContext context = FacesContext.getCurrentInstance();
			    context.addMessage(null, new FacesMessage("Failed",  "Elimine los surtidos del proveedor en primer lugar."));
				return;
			}
			
			proveedores.remove(proveedor);
			proveedorService.eliminaProveedor(proveedor);
			FacesContext context = FacesContext.getCurrentInstance();
		    context.addMessage(null, new FacesMessage("Succsessful",  "Proveedor eliminado."));
		} catch (ServiceException e) {
			FacesContext context = FacesContext.getCurrentInstance();
		    context.addMessage(null, new FacesMessage("Failed",  "No se pudo eliminar al proveedor."));
		}
	}
	
	public void asignaNuevoProveedor(){
		setProveedorSeleccionado(new Proveedor());
	}
	
	public void registraProveedor(){
		try {
			Proveedor proveedor = proveedorService.buscaProveedorPorRut(proveedorSeleccionado.getRut());
			
			if( proveedor != null && proveedor.getIdProveedor() != proveedorSeleccionado.getIdProveedor() ){
				FacesMessage msg = new FacesMessage("Ya existe un proveedor con ese rut");
				FacesContext.getCurrentInstance().addMessage(null, msg);
				return;
			}
			
			if( proveedorService.registraProveedor(proveedorSeleccionado) ){
				asignaNuevoProveedor();
				proveedores = proveedorService.listaProveedores();
				RequestContext.getCurrentInstance().execute("PF('dialog').hide()");
				RequestContext.getCurrentInstance().update("form0");
			}
			
			FacesContext context = FacesContext.getCurrentInstance();
		    context.addMessage(null, new FacesMessage("Succsessful",  "Proveedor registrado."));
		} catch (ServiceException e) {
			FacesContext context = FacesContext.getCurrentInstance();
		    context.addMessage(null, new FacesMessage("Failed",  "No se pudo registrar al proveedor."));
		}
	}
}
