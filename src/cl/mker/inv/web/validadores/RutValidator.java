package cl.mker.inv.web.validadores;

import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.validator.FacesValidator;
import javax.faces.validator.Validator;
import javax.faces.validator.ValidatorException;

import cl.mker.inv.util.ValidaCampo;
import cl.mker.inv.util.ValidadorException;

@FacesValidator
public class RutValidator implements Validator {

	@Override
	public void validate(FacesContext context, UIComponent component, Object value) throws ValidatorException {
		try {
			ValidaCampo.validaRut(value.toString());
		} catch (ValidadorException e) {
			FacesMessage msg = new FacesMessage("Validaci�n de rut fall�.", "Formato de rut invalido.");
			msg.setSeverity(FacesMessage.SEVERITY_ERROR);
			throw new ValidatorException(msg);
		}
	}
}
