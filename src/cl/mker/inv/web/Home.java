package cl.mker.inv.web;

import java.io.Serializable;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.inject.Inject;

import org.apache.shiro.SecurityUtils;
import org.apache.shiro.subject.Subject;

import cl.mker.inv.modelo.configuracion.CuentaCorreo;
import cl.mker.inv.modelo.configuracion.Empresa;
import cl.mker.inv.servicio.OperacionalService;

@ManagedBean
@ViewScoped
public class Home implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -129032250798577352L;

	@Inject
	private OperacionalService operacionalService;
	
	private String info;
	
	@PostConstruct
	private void init(){
		System.out.println("");
		Subject subject = SecurityUtils.getSubject();
		System.out.println(subject);
		System.out.println("ADM: " + subject.hasRole("Administrador"));
		System.out.println("OPE: " + subject.hasRole("Operador"));
		System.out.println("VEN: " + subject.hasRole("Vendedor"));
		System.out.println("OTR: " + subject.hasRole("Otro"));
		info = operacionalService.listaDatosEmpresa().toString();
		info += "\n";
		info += operacionalService.listaDatosCuentaCorreo().toString();
	}
	
	public String getUsuario(){
		return SecurityUtils.getSubject().getPrincipal().toString();
	}
	
	public String getInfo() {
		return info;
	}
	
	public Empresa getEmpresa(){
		return operacionalService.listaDatosEmpresa();
	}
	
	public CuentaCorreo getCuentaCorreo() {
		return operacionalService.listaDatosCuentaCorreo();
	}
}
