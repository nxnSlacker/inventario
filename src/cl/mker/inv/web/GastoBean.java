package cl.mker.inv.web;

import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.inject.Inject;

import org.primefaces.context.RequestContext;

import cl.mker.inv.modelo.Gasto;
import cl.mker.inv.servicio.GastoService;
import cl.mker.inv.servicio.ServiceException;

@ManagedBean
@ViewScoped
public class GastoBean {

	@Inject
	private GastoService gastoService;
	
	private List<Gasto> gastos;
	
	private Gasto gastoSeleccionado;
	
	@PostConstruct
	private void init(){
		System.out.println("GastoBean init");
	
		try {
			gastos = gastoService.listaGastos();
		} catch (ServiceException e) {
			e.printStackTrace();
		}
	}
	
	public List<Gasto> getGastos() {
		return gastos;
	}
	
	public void setGastos(List<Gasto> gastos) {
		this.gastos = gastos;
	}
	
	public Gasto getGastoSeleccionado() {
		return gastoSeleccionado;
	}
	
	public void setGastoSeleccionado(Gasto gastoSeleccionado) {
		this.gastoSeleccionado = gastoSeleccionado;
	}
	
	// Prepara nuevo gasto
	public void asignaNuevoGasto(){
		gastoSeleccionado = new Gasto();
	}
	
	// Elimina
	public void eliminaGasto(Gasto gasto){
		try {
			gastoService.eliminaGasto(gasto);
			gastos.remove(gasto);
			
			FacesContext context = FacesContext.getCurrentInstance();
		    context.addMessage(null, new FacesMessage("Succsessful",  "Gasto eliminado."));
		} catch (ServiceException e) {
			FacesContext context = FacesContext.getCurrentInstance();
		    context.addMessage(null, new FacesMessage("Failed",  "Gasto no eliminado."));
		}
		
		RequestContext.getCurrentInstance().update("form0");
	}
	
	// Registra o Actualiza
	public void registraGasto(){
		if( gastoSeleccionado.getMonto() == null ){
			FacesContext context = FacesContext.getCurrentInstance();
		    context.addMessage(null, new FacesMessage("Failed",  "Ingrese un valor para el monto."));
		    return;
		}
		
		try {
			gastoService.registraGasto(gastoSeleccionado);
			gastos = gastoService.listaGastos();
			
			FacesContext context = FacesContext.getCurrentInstance();
		    context.addMessage(null, new FacesMessage("Succsessful",  "Gasto registrado."));
		} catch (ServiceException e) {
			FacesContext context = FacesContext.getCurrentInstance();
		    context.addMessage(null, new FacesMessage("Failed",  "Gasto no registrado."));
		}
		
		RequestContext.getCurrentInstance().update("form1");
		RequestContext.getCurrentInstance().update("form0");
	}
}
