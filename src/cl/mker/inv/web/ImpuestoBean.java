package cl.mker.inv.web;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.inject.Inject;

import org.primefaces.context.RequestContext;
import org.primefaces.event.CellEditEvent;
import org.primefaces.event.RowEditEvent;

import cl.mker.inv.modelo.Impuesto;
import cl.mker.inv.servicio.ImpuestoService;
import cl.mker.inv.servicio.ServiceException;

@ManagedBean
@ViewScoped
public class ImpuestoBean implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 2502060290385460779L;

	@Inject
	private ImpuestoService impuestoService;
	
	private Impuesto impuesto;
	
	private List<Impuesto> impuestos;
	
	@PostConstruct
	private void init(){
		System.out.println("@PostConstruct " + getClass().getSimpleName());
		
		try {
			impuestos = impuestoService.listaImpuestos();
		} catch (ServiceException e) {
			e.printStackTrace();
		}
	}
	
	public ImpuestoService getImpuestoService() {
		return impuestoService;
	}
	
	public void setImpuestoService(ImpuestoService impuestoService) {
		this.impuestoService = impuestoService;
	}
	
	public List<Impuesto> getImpuestos() {
		return impuestos;
	}
	
	public void setImpuestos(List<Impuesto> impuestos) {
		this.impuestos = impuestos;
	}
	
	public Impuesto getImpuesto() {
		return impuesto;
	}
	
	public void setImpuesto(Impuesto impuesto) {
		this.impuesto = impuesto;
	}
	
	public void asignaNuevoImpuesto(){
		impuesto = new Impuesto();
		impuesto.setFecha(new Date());
	}
	
	public void onRowEdit(RowEditEvent event) {
        Impuesto impuestoEditado = (Impuesto) event.getObject();
        
        try {
        	for (Impuesto impuesto : impuestos){
        		if( impuesto == impuestoEditado ){
        			continue;
        		}
        		
        		if( impuesto.getFecha().equals(impuestoEditado.getFecha()) ){
        			FacesContext context = FacesContext.getCurrentInstance();
    			    context.addMessage(null, new FacesMessage("Failed",  "Ya existe un impuesto para esa fecha."));
        		}
        	}
        	
			impuestoService.actualizaImpuesto(impuestoEditado);
			FacesContext context = FacesContext.getCurrentInstance();
		    context.addMessage(null, new FacesMessage("Successful",  "Impuesto actualizado."));
		} catch (ServiceException e) {
			FacesContext context = FacesContext.getCurrentInstance();
		    context.addMessage(null, new FacesMessage("Failed", "No se puedo actualizar el impuesto."));
		}
    }
     
    public void onRowCancel(RowEditEvent event) {
        FacesMessage msg = new FacesMessage("Edici�n cancelada", ((Impuesto) event.getObject()).getIdImpuesto().toString());
        FacesContext.getCurrentInstance().addMessage(null, msg);
    }
     
    public void onCellEdit(CellEditEvent event) {
        Object oldValue = event.getOldValue();
        Object newValue = event.getNewValue();
         
        if(newValue != null && !newValue.equals(oldValue)) {
            FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_INFO, "Cell Changed", "Old: " + oldValue + ", New:" + newValue);
            FacesContext.getCurrentInstance().addMessage(null, msg);
        }
    }
    
    public void eliminaImpuesto(Impuesto impuesto){
    	try {
			impuestoService.eliminaImpuesto(impuesto);
			impuestos.remove(impuesto);
			FacesContext context = FacesContext.getCurrentInstance();
		    context.addMessage(null, new FacesMessage("Successful",  "Impuesto eliminado."));
		} catch (ServiceException e) {
			FacesContext context = FacesContext.getCurrentInstance();
		    context.addMessage(null, new FacesMessage("Failed",  "No se pudo eliminar."));
		}
    }
    
    public void registraImpuesto(){
    	try {
			impuestoService.registraImpuesto(impuesto);
			impuestos = impuestoService.listaImpuestos();
			asignaNuevoImpuesto();
			FacesContext context = FacesContext.getCurrentInstance();
		    context.addMessage(null, new FacesMessage("Successful",  "Impuesto registrado."));
			RequestContext.getCurrentInstance().execute("PF('dialog').hide()");
		} catch (ServiceException e) {
			FacesContext context = FacesContext.getCurrentInstance();
		    context.addMessage(null, new FacesMessage("Failed",  "No se puedo registrar."));
		}
    }
}
