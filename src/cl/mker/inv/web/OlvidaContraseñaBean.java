package cl.mker.inv.web;

import java.util.Calendar;
import java.util.Date;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.inject.Inject;

import org.primefaces.event.FlowEvent;

import cl.mker.inv.seguridad.Usuario;
import cl.mker.inv.servicio.ServiceException;
import cl.mker.inv.servicio.UsuarioService;

@ManagedBean
@ViewScoped
public class OlvidaContraseñaBean {

	@Inject
	private UsuarioService usuarioService;
	
	private Usuario usuario;
	
	private String username;
	private String firstPassword;
	private String secondPassword;
	private String codigoActivacion;
	
	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getFirstPassword() {
		return firstPassword;
	}

	public void setFirstPassword(String firstPassword) {
		this.firstPassword = firstPassword;
	}

	public String getSecondPassword() {
		return secondPassword;
	}

	public void setSecondPassword(String secondPassword) {
		this.secondPassword = secondPassword;
	}

	public String getCodigoActivacion() {
		return codigoActivacion;
	}

	public void setCodigoActivacion(String codigoActivacion) {
		this.codigoActivacion = codigoActivacion;
	}

	public String onFlowProcess(FlowEvent event) {
		String flujo = event.getNewStep();
		System.out.println(flujo);
		if( flujo.equals("activacion") ){//address
			try {
				usuario = usuarioService.buscaUsuarioPorUsername(username);
				// Valida que tenga fecha de activacion
				if( usuario == null ){
					System.out.println("usuario no existe");
					return event.getOldStep();
				}
				
				if( usuario.getFechaActivacion() == null ){
					System.out.println("Se ha enviado un email para el usuario");
					usuarioService.registraUsuario(usuario);
					return flujo;
				}
			} catch (ServiceException e) {
				e.printStackTrace();
			}
		}
		if( flujo.equals("contraseña") ){//contact
			// valida codigo de activacion
			Calendar diaAnterior = Calendar.getInstance();
			diaAnterior.add(Calendar.HOUR_OF_DAY, -24);
			
			Calendar diaActivacion = Calendar.getInstance();
			diaActivacion.setTime(usuario.getFechaActivacion());
			
			System.out.println("Hace un dia:    " + diaAnterior.getTime());
			System.out.println("Dia activacion: " + diaActivacion.getTime());

			if( diaAnterior.before(diaActivacion) ){
				return flujo;
			} else {
				System.out.println("Se envia otro codigo");
			}
		}
		if( flujo.equals("confirma") ){
			if( ! firstPassword.equals(secondPassword) ){
				// Mensaje de error
				return event.getOldStep();
			}
			
			return "confirma";
		}
		
		return flujo;
    }

	public void actualizaContraseña() {
		System.out.println("Nuevo contraseña: " + firstPassword);		
		usuario.setPassword(firstPassword);
		
		try {
			usuarioService.actualizaPassword(usuario);
		} catch (ServiceException e) {
			e.printStackTrace();
		}
	}
	
	public static void main(String[] args) {
		Calendar diaAnterior = Calendar.getInstance();
		diaAnterior.setTime(new Date());
		diaAnterior.add(Calendar.HOUR_OF_DAY, -22);

		Calendar diaActivacion = Calendar.getInstance();
		diaActivacion.setTime(new Date());
		diaActivacion.add(Calendar.HOUR_OF_DAY, -23);
		
		System.out.println("Dia anterior:   " + diaAnterior.getTime());
		System.out.println("Dia activacion: " + diaActivacion.getTime());
		
		System.out.println(diaAnterior.after(diaActivacion));
	}
}
