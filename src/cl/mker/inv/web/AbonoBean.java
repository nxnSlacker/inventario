package cl.mker.inv.web;

import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.inject.Inject;

import org.primefaces.context.RequestContext;

import cl.mker.inv.modelo.Abono;
import cl.mker.inv.modelo.documento.ResumenFactura;
import cl.mker.inv.servicio.AbonoService;
import cl.mker.inv.servicio.ResumenFacturaService;
import cl.mker.inv.servicio.ServiceException;

@ManagedBean
@ViewScoped
public class AbonoBean {

	@Inject
	private ResumenFacturaService resumenFacturaService;
	
	@Inject
	private AbonoService abonoService;
	
	private ResumenFactura resumenFacturaSeleccionada;
	
	private List<ResumenFactura> resumenFacturas;
	
	private List<Abono> abonos;
	
	private Abono abono;
	
	@PostConstruct
	private void init() {
		System.out.println("AbonoBean init");
		try {
			resumenFacturas = resumenFacturaService.listaResumenFacturas();
		} catch (ServiceException e) {
			e.printStackTrace();
		}
	}
	
	public List<ResumenFactura> getResumenFacturas() {
		return resumenFacturas;
	}
	
	public void setResumenFacturas(List<ResumenFactura> resumenFacturas) {
		this.resumenFacturas = resumenFacturas;
	}
	
	public ResumenFactura getResumenFacturaSeleccionada() {
		return resumenFacturaSeleccionada;
	}
	
	public List<Abono> getAbonos() {
		return abonos;
	}
	
	public void setAbonos(List<Abono> abonos) {
		this.abonos = abonos;
	}
	
	public Abono getAbono() {
		return abono;
	}
	
	public void setAbono(Abono abono) {
		this.abono = abono;
	}
	
	public void setResumenFacturaSeleccionada(ResumenFactura resumenFacturaSeleccionada) {
		this.resumenFacturaSeleccionada = resumenFacturaSeleccionada;
		try {
			abonos = abonoService.buscaAbonosPorFactura(resumenFacturaSeleccionada);
		} catch (ServiceException e) {
			e.printStackTrace();
		}
	}
	
	public void eliminaAbono(Abono abono){
		try {
			abonoService.eliminaAbono(abono);
			abonos.remove(abono);
		} catch (ServiceException e){
			e.printStackTrace();
		}
	}
	
	public void asignaNuevoAbono(){
		abono = new Abono();
	}
	
	public long montoMaximo(){
		long total = resumenFacturaSeleccionada.getTotalNeto();
		
		for (Abono abono : abonos) {
			total -= abono.getMonto();
		}
		
		return total;
	}
	
	public void registraAbono(){
		
		try {
			abonoService.registraAbono(abono, resumenFacturaSeleccionada);
			abonos = abonoService.buscaAbonosPorFactura(resumenFacturaSeleccionada);
		} catch (ServiceException e) {
			e.printStackTrace();
		}
		
		RequestContext.getCurrentInstance().execute("PF('dialog1').hide()");
		RequestContext.getCurrentInstance().update("form0");
		RequestContext.getCurrentInstance().update("form2");
	}
}
