package cl.mker.inv.web;

import java.io.Serializable;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.inject.Inject;

import org.primefaces.context.RequestContext;

import cl.mker.inv.modelo.Material;
import cl.mker.inv.servicio.LineaMaterialService;
import cl.mker.inv.servicio.LineaSurtidoService;
import cl.mker.inv.servicio.MaterialService;
import cl.mker.inv.servicio.ServiceException;
import cl.mker.inv.servicio.documento.StockMaterialService;

@ManagedBean
@ViewScoped
public class MaterialBean implements Serializable {


	/**
	 * 
	 */
	private static final long serialVersionUID = 1796929732293981059L;

	@Inject
	private MaterialService materialService;
	@Inject
	private LineaSurtidoService lineaSurtidoService;
	@Inject
	private LineaMaterialService lineaMaterialService;
	
	@Inject
	private StockMaterialService stockMaterialService;
	
	private List<Material> materiales;
	private Material materialSeleccionado;
	
	private List<Material> materialesFiltrados;
	
	@PostConstruct
	private void init(){
		System.out.println("@PostConstruct " + getClass().getSimpleName());
		try {
			materiales = materialService.listaMateriales();
			for (Material material : materiales) {
				material.setStockCliente(stockMaterialService.buscaStockPorMaterial(material));
			}
		} catch (ServiceException e) {
			e.printStackTrace();
		}
	}
	
	public List<Material> getMaterialesFiltrados() {
		return materialesFiltrados;
	}
	
	public void setMaterialesFiltrados(List<Material> materialesFiltrados) {
		this.materialesFiltrados = materialesFiltrados;
	}
	
	public List<Material> getMateriales() {
		return materiales;
	}
	
	public void setMaterialSeleccionado(Material materialSeleccionado) {
		this.materialSeleccionado = materialSeleccionado;
	}
	
	public Material getMaterialSeleccionado() {
		return materialSeleccionado;
	}
	
	// Elimina
	public void eliminaMaterial(Material material){
		try {
			boolean tieneLinea = false;
			if( lineaSurtidoService.tieneLineaSurtido(material) ){
				FacesContext context = FacesContext.getCurrentInstance();
			    context.addMessage(null, new FacesMessage("Failed",  "Elimine el material desde el surtido en primer lugar."));
			    tieneLinea = true;
			}
			if( lineaMaterialService.tieneLineaMaterial(material) ){
				FacesContext context = FacesContext.getCurrentInstance();
			    context.addMessage(null, new FacesMessage("Failed",  "Elimine el material desde la orden en primer lugar."));
			    tieneLinea = true;
			}
			
			if( tieneLinea ){
				return;
			}
			
			
			materialService.eliminaMaterial(material);
			materiales.remove(material);
			
			FacesContext context = FacesContext.getCurrentInstance();
		    context.addMessage(null, new FacesMessage("Successful",  "Material eliminado."));
		} catch (ServiceException e) {
			FacesContext context = FacesContext.getCurrentInstance();
		    context.addMessage(null, new FacesMessage("Failed",  "No se pudo eliminar el material."));
		}
	}
	
	public void asignaNuevoMaterial(){
		materialSeleccionado = new Material();
	}
	
	// Registra o Actualiza
	public void registraMaterial(){
		try {
			Material material = materialService.buscaMaterialPorNombre(materialSeleccionado.getNombre());
			
			if( material != null && material.getIdMaterial() != materialSeleccionado.getIdMaterial() ){
				FacesMessage msg = new FacesMessage("Ya existe un material con ese nombre: ", material.getNombre());
				FacesContext.getCurrentInstance().addMessage(null, msg);
				return;
			}
			
			if( materialService.registraMaterial(materialSeleccionado) ){
				asignaNuevoMaterial();
				materiales = materialService.listaMateriales();
				FacesContext context = FacesContext.getCurrentInstance();
			    context.addMessage(null, new FacesMessage("Succsessful",  "Material registrado."));
				RequestContext.getCurrentInstance().execute("PF('dialog').hide()");
				RequestContext.getCurrentInstance().update("form0");
			}
		} catch (ServiceException e) {
			e.printStackTrace();
		}
	}
}
