package cl.mker.inv.modelo;

import java.sql.Date;
import java.util.List;

public class Orden {

	private Long idOrden = 0L;
	private Cliente cliente;

	private Factura factura;
	private Date fecha;

	private List<LineaMaterial> lineasMaterial;
	
	public Orden() {
	}

	public Orden(Cliente cliente, Factura factura) {
		this.cliente = cliente;
		this.factura = factura;
	}
	
	public Orden(Cliente cliente, Factura factura, Date fecha) {
		this(cliente, factura);
		this.fecha = fecha;
	}

	public Orden(Long idOrden, Cliente cliente, Factura factura, Date fecha) {
		this(cliente, factura, fecha);
		this.idOrden = idOrden;
	}

	public Long getIdOrden() {
		return idOrden;
	}

	public void setIdOrden(Long idOrden) {
		this.idOrden = idOrden;
	}

	public Cliente getCliente() {
		return cliente;
	}

	public void setCliente(Cliente cliente) {
		this.cliente = cliente;
	}

	public Factura getFactura() {
		return factura;
	}

	public void setFactura(Factura factura) {
		this.factura = factura;
	}

	public Date getFecha() {
		return fecha;
	}
	
	public void setFecha(Date fecha) {
		this.fecha = fecha;
	}
	
	public List<LineaMaterial> getLineasMaterial() {
		return lineasMaterial;
	}
	
	public void setLineasMaterial(List<LineaMaterial> lineasMaterial) {
		this.lineasMaterial = lineasMaterial;
	}

	public Long getTotalNeto(){
		if ( lineasMaterial == null ){
			return 0L;
		}
		
		long total = 0;
		
		for (LineaMaterial lineaMaterial : lineasMaterial) {
			total += lineaMaterial.getValor() * lineaMaterial.getCantidad();
		}
		
		return total;
	}
	
	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("Orden [idOrden=");
		builder.append(idOrden);
		builder.append(", cliente=");
		builder.append(cliente);
		builder.append(", factura=");
		builder.append(factura);
		builder.append(", fecha=");
		builder.append(fecha);
		builder.append("]");
		return builder.toString();
	}

}
