package cl.mker.inv.modelo;

import java.io.Serializable;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

public class Admin implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1202807770818143668L;

	@Min(value = 1, message = "El identificador debe ser mayor o igual a {value}")
	@NotNull(message = "El identificador no puede estar vacio")
	private Long id;
	
	@Size(min = 5, max = 50, message = "El nombre debe estar entre los {min} y {max} caracteres")
	@NotNull(message = "El nombre no puede ser vacio")
	private String nombre;
	
	@Min(value = 1, message = "La edad debe ser mayor o igual a {value}")
	@NotNull(message = "La edad no puede estar vacia")
	private Integer edad;

	public Long getId() {
		return id;
	}
	
	public void setId(Long id) {
		this.id = id;
	}
	
	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public Integer getEdad() {
		return edad;
	}

	public void setEdad(Integer edad) {
		this.edad = edad;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((edad == null) ? 0 : edad.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Admin other = (Admin) obj;
		if (edad == null) {
			if (other.edad != null)
				return false;
		} else if (!edad.equals(other.edad))
			return false;
		return true;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("Admin [idt=");
		builder.append(id);
		builder.append(", nombre=");
		builder.append(nombre);
		builder.append(", edad=");
		builder.append(edad);
		builder.append("]");
		return builder.toString();
	}

}
