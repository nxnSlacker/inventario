package cl.mker.inv.modelo;

import java.util.Date;

import javax.validation.constraints.Min;

public class Abono {

	private Long idAbono;
	private Date fecha;
	@Min(value = 1, message = "El valor debe ser mayor o igual a {value} ")
	private Long monto;

	public Long getIdAbono() {
		return idAbono;
	}

	public void setIdAbono(Long idAbono) {
		this.idAbono = idAbono;
	}

	public Date getFecha() {
		return fecha;
	}

	public void setFecha(Date fecha) {
		this.fecha = fecha;
	}

	public Long getMonto() {
		return monto;
	}

	public void setMonto(Long monto) {
		this.monto = monto;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("Abono [idAbono=");
		builder.append(idAbono);
		builder.append(", fecha=");
		builder.append(fecha);
		builder.append(", monto=");
		builder.append(monto);
		builder.append("]");
		return builder.toString();
	}

}
