package cl.mker.inv.modelo;

import java.sql.Date;
import java.util.List;

public class LineaMaterial {

	private Long idLineaMaterial;
	private Orden orden;
	private Material material;

	private Integer cantidad = 0;
	private long valor;

	private Date fechaDevolucion;
	private String decripcionDevolucion;

	private List<Devolucion> devoluciones;
	private int cantidadAnterior;
	
	public LineaMaterial() {
	}

	public LineaMaterial(Orden orden, Material material, int cantidad, long valor) {
		this.orden = orden;
		this.material = material;
		this.cantidad = cantidad;
		this.valor = valor;
	}

	public LineaMaterial(Long idLineaMaterial, Orden orden, Material material, int cantidad, long valor) {
		this(orden, material, cantidad, valor);
		this.idLineaMaterial = idLineaMaterial;
	}

	public Long getIdLineaMaterial() {
		return idLineaMaterial;
	}

	public void setIdLineaMaterial(Long idLineaMaterial) {
		this.idLineaMaterial = idLineaMaterial;
	}

	public Orden getOrden() {
		return orden;
	}

	public void setOrden(Orden orden) {
		this.orden = orden;
	}

	public Material getMaterial() {
		return material;
	}

	public void setMaterial(Material material) {
		this.material = material;
	}

	public Integer getCantidad() {
		return cantidad;
	}

	public void setCantidad(Integer cantidad) {
		this.cantidad = cantidad;
	}

	public long getValor() {
		return valor;
	}

	public void setValor(long valor) {
		this.valor = valor;
	}

	public Date getFechaDevolucion() {
		return fechaDevolucion;
	}

	public void setFechaDevolucion(Date fechaDevolucion) {
		this.fechaDevolucion = fechaDevolucion;
	}

	public String getDecripcionDevolucion() {
		return decripcionDevolucion;
	}

	public void setDecripcionDevolucion(String decripcionDevolucion) {
		this.decripcionDevolucion = decripcionDevolucion;
	}
	
	public void setDevoluciones(List<Devolucion> devoluciones) {
		this.devoluciones = devoluciones;
	}
	
	public List<Devolucion> getDevoluciones() {
		return devoluciones;
	}
	
	public void setCantidadAnterior(int cantidadAnterior) {
		this.cantidadAnterior = cantidadAnterior;
	}
	
	public int getCantidadAnterior() {
		return cantidadAnterior;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("LineaMaterial [idLineaMaterial=");
		builder.append(idLineaMaterial);
		builder.append(", orden=");
		builder.append(orden);
		builder.append(", cantidad=");
		builder.append(cantidad);
		builder.append(", valor=");
		builder.append(valor);
		builder.append(", fechaDevolucion=");
		builder.append(fechaDevolucion);
		builder.append(", decripcionDevolucion=");
		builder.append(decripcionDevolucion);
		builder.append(", material=");
		builder.append(material);
		builder.append("]");
		return builder.toString();
	}

}
