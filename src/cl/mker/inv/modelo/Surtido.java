package cl.mker.inv.modelo;

import java.sql.Date;

public class Surtido {

	private Long idSurtido = 0L;
	private Long numeroFactura;
	private Proveedor proveedor;
	private Date fecha;

	public Surtido() {
	}
	
	public Surtido(Long idSurtido, Long numeroFactura, Material material, Proveedor proveedor, int cantidad, Date fecha, long costo) {
		this(numeroFactura, material, proveedor, cantidad, fecha, costo);
		this.idSurtido = idSurtido;
	}

	public Surtido(Long numeroFactura, Material material, Proveedor proveedor, int cantidad, Date fecha, long costo) {
		this.numeroFactura = numeroFactura;
		this.proveedor = proveedor;
		this.fecha = fecha;
	}

	public Long getIdSurtido() {
		return idSurtido;
	}

	public void setIdSurtido(Long idSurtido) {
		this.idSurtido = idSurtido;
	}

	public Long getNumeroFactura() {
		return numeroFactura;
	}

	public void setNumeroFactura(Long numeroFactura) {
		this.numeroFactura = numeroFactura;
	}

	public Proveedor getProveedor() {
		return proveedor;
	}

	public void setProveedor(Proveedor proveedor) {
		this.proveedor = proveedor;
	}

	public Date getFecha() {
		return fecha;
	}

	public void setFecha(Date fecha) {
		this.fecha = fecha;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((idSurtido == null) ? 0 : idSurtido.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Surtido other = (Surtido) obj;
		if (idSurtido == null) {
			if (other.idSurtido != null)
				return false;
		} else if (!idSurtido.equals(other.idSurtido))
			return false;
		return true;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("Surtido [idSurtido=");
		builder.append(idSurtido);
		builder.append(", numeroFactura=");
		builder.append(numeroFactura);
		builder.append(", proveedor=");
		builder.append(proveedor);
		builder.append(", fecha=");
		builder.append(fecha);
		builder.append("]");
		return builder.toString();
	}

}
