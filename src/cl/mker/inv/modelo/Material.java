package cl.mker.inv.modelo;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

public class Material {

	private Long idMaterial;
	@Size(min = 5, max = 50, message = "El nombre debe tener entre {min} y {max} caracteres.")
	@NotNull(message = "El nombre es obligatorio")
	private String nombre;
	@Min(value = 5, message = "El valor debe ser mayor a {value}")
	private long valor;
	@Size(min = 1, max = 10, message = "El sector debe tener entre {min} y {max} caracteres.")
	@NotNull(message = "El sector es obligatorio")
	private String sector;
	@Size(min = 1, max = 45, message = "El lugar debe tener entre {min} y {max} caracteres.")
	@NotNull(message = "El lugar es obligatorio")
	private String lugar;
	
	private int stockCliente;
	private int stockProveedor;

	public Material() {
	}
	
	public Material(Long idMaterial, String nombre, long valor) {
		this(nombre, valor);
		this.idMaterial = idMaterial;
	}

	public Material(String nombre, long valor) {
		this.nombre = nombre;
		this.valor = valor;
	}

	public Long getIdMaterial() {
		return idMaterial;
	}
	
	public void setIdMaterial(Long idMaterial) {
		this.idMaterial = idMaterial;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public long getValor() {
		return valor;
	}

	public void setValor(long valor) {
		this.valor = valor;
	}
	
	public void setStockCliente(int stockCliente) {
		this.stockCliente = stockCliente;
	}
	
	public int getStockCliente() {
		return stockCliente;
	}
	
	public void setStockProveedor(int stockProveedor) {
		this.stockProveedor = stockProveedor;
	}
	
	public int getStockProveedor() {
		return stockProveedor;
	}
	
	public String getSector() {
		return sector;
	}
	
	public void setSector(String sector) {
		this.sector = sector;
	}
	
	public String getLugar() {
		return lugar;
	}
	
	public void setLugar(String lugar) {
		this.lugar = lugar;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((nombre == null) ? 0 : nombre.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Material other = (Material) obj;
		if (nombre == null) {
			if (other.nombre != null)
				return false;
		} else if (!nombre.equals(other.nombre))
			return false;
		return true;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("Material [idMaterial=");
		builder.append(idMaterial);
		builder.append(", nombre=");
		builder.append(nombre);
		builder.append(", valor=");
		builder.append(valor);
		builder.append(", stockCliente=");
		builder.append(stockCliente);
		builder.append(", stockProveedor=");
		builder.append(stockProveedor);
		builder.append("]");
		return builder.toString();
	}

}
