package cl.mker.inv.modelo;

import javax.validation.constraints.Min;

public class LineaSurtido {

	private Long idLineaSurtido;
	private Surtido Surtido;
	private Material material;
	
	@Min(value = 1, message = "La cantidad debe ser mayor o igual a {value}")
	private int cantidad;
	@Min(value = 1, message = "La nueva cantidad debe ser mayor o igual a {value}")
	private int nuevaCantidad;
	@Min(value = 1, message = "El valor debe ser mayor o igual a {value}")
	private long valor;
	
	public Long getIdLineaSurtido() {
		return idLineaSurtido;
	}
	public void setIdLineaSurtido(Long idLineaSurtido) {
		this.idLineaSurtido = idLineaSurtido;
	}
	public Surtido getSurtido() {
		return Surtido;
	}
	public void setSurtido(Surtido surtido) {
		Surtido = surtido;
	}
	public Material getMaterial() {
		return material;
	}
	public void setMaterial(Material material) {
		this.material = material;
	}
	public int getCantidad() {
		return cantidad;
	}
	public void setCantidad(int cantidad) {
		this.cantidad = cantidad;
	}
	public int getNuevaCantidad() {
		return nuevaCantidad;
	}
	public void setNuevaCantidad(int nuevaCantidad) {
		this.nuevaCantidad = nuevaCantidad;
	}
	public long getValor() {
		return valor;
	}
	public void setValor(long valor) {
		this.valor = valor;
	}
	
	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("LineaSurtido [idLineaSurtido=");
		builder.append(idLineaSurtido);
		builder.append(", Surtido=");
		builder.append(Surtido);
		builder.append(", material=");
		builder.append(material);
		builder.append(", cantidad=");
		builder.append(cantidad);
		builder.append(", valor=");
		builder.append(valor);
		builder.append("]");
		return builder.toString();
	}
	
}
