package cl.mker.inv.modelo;

import java.util.Date;

public class Factura {

	private Long idFactura;
	private String numeroFactura;
	private Impuesto impuesto;
	private Date fecha;
	
	private Long totalBruto;
	private Long totalNeto;
	private Long cantidadAbonos;
	private Long totalAbonado;
	private Long deudaActual;

	public Long getIdFactura() {
		return idFactura;
	}

	public void setIdFactura(Long idFactura) {
		this.idFactura = idFactura;
	}

	public String getNumeroFactura() {
		return numeroFactura;
	}

	public void setNumeroFactura(String numeroFactura) {
		this.numeroFactura = numeroFactura;
	}

	public Date getFecha() {
		return fecha;
	}
	
	public void setFecha(Date fecha) {
		this.fecha = fecha;
	}
	
	public Impuesto getImpuesto() {
		return impuesto;
	}
	
	public void setImpuesto(Impuesto impuesto) {
		this.impuesto = impuesto;
	}
	
	public Long getTotalBruto() {
		return totalBruto;
	}
	
	public void setTotalBruto(Long totalBruto) {
		this.totalBruto = totalBruto;
	}
	
	public Long getTotalNeto() {
		return totalNeto;
	}
	
	public void setTotalNeto(Long totalNeto) {
		this.totalNeto = totalNeto;
	}
	
	public Long getCantidadAbonos() {
		return cantidadAbonos;
	}

	public void setCantidadAbonos(Long cantidadAbonos) {
		this.cantidadAbonos = cantidadAbonos;
	}

	public Long getTotalAbonado() {
		return totalAbonado;
	}

	public void setTotalAbonado(Long totalAbonado) {
		this.totalAbonado = totalAbonado;
	}

	public Long getDeudaActual() {
		return deudaActual;
	}

	public void setDeudaActual(Long deudaActual) {
		this.deudaActual = deudaActual;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("Factura [idFactura=");
		builder.append(idFactura);
		builder.append(", numeroFactura=");
		builder.append(numeroFactura);
		builder.append(", impuesto=");
		builder.append(impuesto);
		builder.append(", fecha=");
		builder.append(fecha);
		builder.append(", totalBruto=");
		builder.append(totalBruto);
		builder.append(", totalNeto=");
		builder.append(totalNeto);
		builder.append(", cantidadAbonos=");
		builder.append(cantidadAbonos);
		builder.append(", totalAbonado=");
		builder.append(totalAbonado);
		builder.append(", deudaActual=");
		builder.append(deudaActual);
		builder.append("]");
		return builder.toString();
	}

}
