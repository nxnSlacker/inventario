package cl.mker.inv.modelo;

import java.io.Serializable;
import java.util.Date;

public class Devolucion implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 183713163407323800L;
	private Long idDevolucion;
	private Integer cantidad;
	private Date fecha;
	private String motivo;
	private LineaMaterial lineaMaterial;
	
	public Long getIdDevolucion() {
		return idDevolucion;
	}
	public void setIdDevolucion(Long idDevolucion) {
		this.idDevolucion = idDevolucion;
	}
	public Integer getCantidad() {
		return cantidad;
	}
	public void setCantidad(Integer cantidad) {
		this.cantidad = cantidad;
	}
	public Date getFecha() {
		return fecha;
	}
	public void setFecha(Date fecha) {
		this.fecha = fecha;
	}
	public String getMotivo() {
		return motivo;
	}
	public void setMotivo(String motivo) {
		this.motivo = motivo;
	}
	public LineaMaterial getLineaMaterial() {
		return lineaMaterial;
	}
	public void setLineaMaterial(LineaMaterial lineaMaterial) {
		this.lineaMaterial = lineaMaterial;
	}
	
	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("Devolucion [idDevolucion=");
		builder.append(idDevolucion);
		builder.append(", cantidad=");
		builder.append(cantidad);
		builder.append(", fecha=");
		builder.append(fecha);
		builder.append(", motivo=");
		builder.append(motivo);
		builder.append(", lineaMaterial=");
		builder.append(lineaMaterial);
		builder.append("]");
		return builder.toString();
	}

}
