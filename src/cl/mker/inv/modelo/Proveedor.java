package cl.mker.inv.modelo;

import java.io.Serializable;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

public class Proveedor implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1953460858686183906L;
	private Long idProveedor;
	@NotNull(message = "El rut es obligatorio")
	@Size(min = 3, max = 20, message = "El rut debe tener mas de {min} caracteres.")
	private String rut;
	@Size(min = 5, max = 50, message = "La razon social debe tener entre {min} y {max} caracteres.")
	@NotNull(message = "La raz�n social es obligatoria")
	private String razonSocial;
	@Size(min = 5, max = 100, message = "La direccion debe tener entre {min} y {max} caracteres.")
	@NotNull(message = "La direccion es obligatoria")
	private String direccion;
	@Size(min = 6, max = 20, message = "El telefono debe tener entre {min} y {max} caracteres.")
	@NotNull(message = "El telefono es obligatoria")
	private String telefono;

	public Proveedor() {
	}
	
	public Proveedor(String rut,  String razonSocial) {
		this.rut = rut;
		this.razonSocial = razonSocial;
	}
	
	public Proveedor(Long idProveedor, String rut, String razonSocial) {
		this(rut, razonSocial);
		this.idProveedor = idProveedor;
	}

	public Long getIdProveedor() {
		return idProveedor;
	}

	public void setIdProveedor(Long idProveedor) {
		this.idProveedor = idProveedor;
	}

	public String getRut() {
		return rut;
	}

	public void setRut(String rut) {
		this.rut = rut;
	}

	public String getRazonSocial() {
		return razonSocial;
	}

	public void setRazonSocial(String razonSocial) {
		this.razonSocial = razonSocial;
	}
	
	public String getDireccion() {
		return direccion;
	}
	
	public void setDireccion(String direccion) {
		this.direccion = direccion;
	}
	
	public String getTelefono() {
		return telefono;
	}
	
	public void setTelefono(String telefono) {
		this.telefono = telefono;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((idProveedor == null) ? 0 : idProveedor.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Proveedor other = (Proveedor) obj;
		if (idProveedor == null) {
			if (other.idProveedor != null)
				return false;
		} else if (!idProveedor.equals(other.idProveedor))
			return false;
		return true;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("Proveedor [idProveedor=");
		builder.append(idProveedor);
		builder.append(", rut=");
		builder.append(rut);
		builder.append(", razonSocial=");
		builder.append(razonSocial);
		builder.append("]");
		return builder.toString();
	}

}
