package cl.mker.inv.modelo;

import java.io.Serializable;
import java.util.Date;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

public class Impuesto implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 638661383390978870L;
	private Long idImpuesto;
	@Min(value = 0, message = "El porcentaje debe ser mayor o igual a {value}")
	@NotNull(message = "El porcentaje no puede quedar vac�o")
	private Double porcentaje;
	@NotNull(message = "la fecha no puede quedar vac�a")
	private Date fecha;

	public Long getIdImpuesto() {
		return idImpuesto;
	}

	public void setIdImpuesto(Long idImpuesto) {
		this.idImpuesto = idImpuesto;
	}

	public Double getPorcentaje() {
		return porcentaje;
	}

	public void setPorcentaje(Double porcentaje) {
		this.porcentaje = porcentaje;
	}

	public Date getFecha() {
		return fecha;
	}

	public void setFecha(Date fecha) {
		this.fecha = fecha;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("Impuesto [idImpuesto=");
		builder.append(idImpuesto);
		builder.append(", porcentaje=");
		builder.append(porcentaje);
		builder.append(", fecha=");
		builder.append(fecha);
		builder.append("]");
		return builder.toString();
	}

}
