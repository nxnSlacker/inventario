package cl.mker.inv.modelo.configuracion;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class CuentaCorreo {

	@XmlElement
	private String usuario;
	@XmlElement(name = "password")
	private String contraseña;
	@XmlElement
	private String host;
	@XmlElement
	private int puerto;

	public String getUsuario() {
		return usuario;
	}

	public void setUsuario(String usuario) {
		this.usuario = usuario;
	}

	public String getContraseña() {
		return contraseña;
	}

	public void setContraseña(String contraseña) {
		this.contraseña = contraseña;
	}

	public String getHost() {
		return host;
	}

	public void setHost(String host) {
		this.host = host;
	}

	public int getPuerto() {
		return puerto;
	}

	public void setPuerto(int puerto) {
		this.puerto = puerto;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("CuentaCorreo [usuario=");
		builder.append(usuario);
		builder.append(", contraseña=");
		builder.append(contraseña);
		builder.append(", host=");
		builder.append(host);
		builder.append(", puerto=");
		builder.append(puerto);
		builder.append("]");
		return builder.toString();
	}

}
