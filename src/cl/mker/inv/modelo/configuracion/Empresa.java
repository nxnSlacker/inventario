package cl.mker.inv.modelo.configuracion;

import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class Empresa {

	@XmlElement
	private String nombre;
	@XmlElement
	private String direccion;
	@XmlElement
	private String ciudad;
	@XmlElement
	private List<String> telefonosFijos;
	@XmlElement
	private List<String> celulares;
	@XmlElement
	private String email;
	@XmlElement
	private String descripcion;
	
	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getDireccion() {
		return direccion;
	}

	public void setDireccion(String direccion) {
		this.direccion = direccion;
	}

	public String getCiudad() {
		return ciudad;
	}

	public void setCiudad(String ciudad) {
		this.ciudad = ciudad;
	}

	public List<String> getTelefonosFijos() {
		return telefonosFijos;
	}

	public void setTelefonosFijos(List<String> telefonosFijos) {
		this.telefonosFijos = telefonosFijos;
	}

	public List<String> getCelulares() {
		return celulares;
	}

	public void setCelulares(List<String> celulares) {
		this.celulares = celulares;
	}

	public String getEmail() {
		return email;
	}
	
	public void setEmail(String email) {
		this.email = email;
	}
	
	public String getDescripcion() {
		return descripcion;
	}
	
	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("Empresa [nombre=");
		builder.append(nombre);
		builder.append(", direccion=");
		builder.append(direccion);
		builder.append(", ciudad=");
		builder.append(ciudad);
		builder.append(", telefonosFijos=");
		builder.append(telefonosFijos);
		builder.append(", celulares=");
		builder.append(celulares);
		builder.append(", email=");
		builder.append(email);
		builder.append("]");
		return builder.toString();
	}

}
