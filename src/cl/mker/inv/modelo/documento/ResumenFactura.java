package cl.mker.inv.modelo.documento;

import java.util.Date;

public class ResumenFactura {

	private Long idFactura;
	private Long numeroFactura;
	private Date fecha;
	private Double porcentaje;
	private Long totalBruto;
	private Long totalNeto;
	private Long cantidadAbonos;
	private Long totalAbonado;
	private Long deudaActual;

	public Long getIdFactura() {
		return idFactura;
	}

	public void setIdFactura(Long idFactura) {
		this.idFactura = idFactura;
	}

	public Long getNumeroFactura() {
		return numeroFactura;
	}

	public void setNumeroFactura(Long numeroFactura) {
		this.numeroFactura = numeroFactura;
	}

	public Date getFecha() {
		return fecha;
	}

	public void setFecha(Date fecha) {
		this.fecha = fecha;
	}

	public Double getPorcentaje() {
		return porcentaje;
	}

	public void setPorcentaje(Double porcentaje) {
		this.porcentaje = porcentaje;
	}

	public Long getTotalBruto() {
		return totalBruto;
	}

	public void setTotalBruto(Long totalBruto) {
		this.totalBruto = totalBruto;
	}

	public Long getTotalNeto() {
		return totalNeto;
	}

	public void setTotalNeto(Long totalNeto) {
		this.totalNeto = totalNeto;
	}

	public Long getCantidadAbonos() {
		return cantidadAbonos;
	}

	public void setCantidadAbonos(Long cantidadAbonos) {
		this.cantidadAbonos = cantidadAbonos;
	}

	public Long getTotalAbonado() {
		return totalAbonado;
	}

	public void setTotalAbonado(Long totalAbonado) {
		this.totalAbonado = totalAbonado;
	}

	public Long getDeudaActual() {
		return deudaActual;
	}

	public void setDeudaActual(Long deudaActual) {
		this.deudaActual = deudaActual;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((numeroFactura == null) ? 0 : numeroFactura.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ResumenFactura other = (ResumenFactura) obj;
		if (numeroFactura == null) {
			if (other.numeroFactura != null)
				return false;
		} else if (!numeroFactura.equals(other.numeroFactura))
			return false;
		return true;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("ResumenFactura [idFactura=");
		builder.append(idFactura);
		builder.append(", numeroFactura=");
		builder.append(numeroFactura);
		builder.append(", fecha=");
		builder.append(fecha);
		builder.append(", porcentaje=");
		builder.append(porcentaje);
		builder.append(", totalBruto=");
		builder.append(totalBruto);
		builder.append(", totalNeto=");
		builder.append(totalNeto);
		builder.append(", cantidadAbonos=");
		builder.append(cantidadAbonos);
		builder.append(", totalAbonado=");
		builder.append(totalAbonado);
		builder.append(", deudaActual=");
		builder.append(deudaActual);
		builder.append("]");
		return builder.toString();
	}

}
