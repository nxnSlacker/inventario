package cl.mker.inv.modelo.documento;

import java.util.List;

import cl.mker.inv.modelo.Factura;
import cl.mker.inv.modelo.Orden;

public class DetalleFactura {

	private Factura factura;
	private List<Orden> ordenes;

	public Factura getFactura() {
		return factura;
	}

	public void setFactura(Factura factura) {
		this.factura = factura;
	}

	public List<Orden> getOrdenes() {
		return ordenes;
	}

	public void setOrdenes(List<Orden> ordenes) {
		this.ordenes = ordenes;
	}

}
