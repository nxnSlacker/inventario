package cl.mker.inv.modelo.documento;

import java.util.Date;

public class ResumenVenta {

	private Long idOrden;
	private Date fecha;
	private String numeroFactura;
	private String nombreCliente;
	private String rut;
	private Double porcentaje;
	private Long idMaterial;
	private String nombreMaterial;
	private Long cantidad;
	private Long valor;
	private Long totalBruto;
	private Long totalNeto;

	public Long getIdOrden() {
		return idOrden;
	}

	public void setIdOrden(Long idOrden) {
		this.idOrden = idOrden;
	}

	public Date getFecha() {
		return fecha;
	}

	public void setFecha(Date fecha) {
		this.fecha = fecha;
	}

	public String getNumeroFactura() {
		return numeroFactura;
	}

	public void setNumeroFactura(String numeroFactura) {
		this.numeroFactura = numeroFactura;
	}

	public String getNombreCliente() {
		return nombreCliente;
	}

	public void setNombreCliente(String nombreCliente) {
		this.nombreCliente = nombreCliente;
	}

	public String getRut() {
		return rut;
	}

	public void setRut(String rut) {
		this.rut = rut;
	}

	public Double getPorcentaje() {
		return porcentaje;
	}

	public void setPorcentaje(Double porcentaje) {
		this.porcentaje = porcentaje;
	}
	
	public Long getIdMaterial() {
		return idMaterial;
	}
	
	public void setIdMaterial(Long idMaterial) {
		this.idMaterial = idMaterial;
	}

	public String getNombreMaterial() {
		return nombreMaterial;
	}

	public void setNombreMaterial(String nombreMaterial) {
		this.nombreMaterial = nombreMaterial;
	}

	public Long getCantidad() {
		return cantidad;
	}

	public void setCantidad(Long cantidad) {
		this.cantidad = cantidad;
	}

	public Long getValor() {
		return valor;
	}

	public void setValor(Long valor) {
		this.valor = valor;
	}

	public Long getTotalBruto() {
		return totalBruto;
	}

	public void setTotalBruto(Long totalBruto) {
		this.totalBruto = totalBruto;
	}

	public Long getTotalNeto() {
		return totalNeto;
	}

	public void setTotalNeto(Long totalNeto) {
		this.totalNeto = totalNeto;
	}

}
