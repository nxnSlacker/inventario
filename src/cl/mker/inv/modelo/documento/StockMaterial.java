package cl.mker.inv.modelo.documento;

public class StockMaterial {

	private Long idMaterial;
	private String nombreMaterial;
	private int cantidadComprados;
	private int cantidadVendidos;
	private int cantidadDevueltos;
	private int cantidadDisponible;

	public Long getIdMaterial() {
		return idMaterial;
	}
	
	public void setIdMaterial(Long idMaterial) {
		this.idMaterial = idMaterial;
	}
	
	public String getNombreMaterial() {
		return nombreMaterial;
	}

	public void setNombreMaterial(String nombreMaterial) {
		this.nombreMaterial = nombreMaterial;
	}

	public int getCantidadComprados() {
		return cantidadComprados;
	}

	public void setCantidadComprados(int cantidadComprados) {
		this.cantidadComprados = cantidadComprados;
	}

	public int getCantidadVendidos() {
		return cantidadVendidos;
	}

	public void setCantidadVendidos(int cantidadVendidos) {
		this.cantidadVendidos = cantidadVendidos;
	}

	public int getCantidadDevueltos() {
		return cantidadDevueltos;
	}
	
	public void setCantidadDevueltos(int cantidadDevueltos) {
		this.cantidadDevueltos = cantidadDevueltos;
	}

	public int getCantidadDisponible() {
		return cantidadDisponible;
	}
	
	public void setCantidadDisponible(int cantidadDisponible) {
		this.cantidadDisponible = cantidadDisponible;
	}
	
	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("StockMaterial [nombreMaterial=");
		builder.append(nombreMaterial);
		builder.append(", cantidadComprados=");
		builder.append(cantidadComprados);
		builder.append(", cantidadVendidos=");
		builder.append(cantidadVendidos);
		builder.append(", cantidadDevueltos=");
		builder.append(cantidadDevueltos);
		builder.append(", cantidadDisponible=");
		builder.append(cantidadDisponible);
		builder.append("]");
		return builder.toString();
	}

}
