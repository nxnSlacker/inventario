package cl.mker.inv.modelo;

import java.io.Serializable;
import java.util.Date;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

public class Gasto implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -5590332711101990431L;
	private Long idGasto;
	private Date fecha;
	@Min(value = 1, message = "El monto debe ser mayor a {value}")
	private Long monto;
	@NotNull(message = "La descripci�n no puede estar vac�a")
	private String descripcion;

	public Long getIdGasto() {
		return idGasto;
	}

	public void setIdGasto(Long idGasto) {
		this.idGasto = idGasto;
	}

	public Date getFecha() {
		return fecha;
	}

	public void setFecha(Date fecha) {
		this.fecha = fecha;
	}

	public Long getMonto() {
		return monto;
	}

	public void setMonto(Long monto) {
		this.monto = monto;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("Gasto [idGasto=");
		builder.append(idGasto);
		builder.append(", fecha=");
		builder.append(fecha);
		builder.append(", monto=");
		builder.append(monto);
		builder.append(", descripcion=");
		builder.append(descripcion);
		builder.append("]");
		return builder.toString();
	}

}
