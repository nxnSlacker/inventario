package cl.mker.inv.modelo;

import java.io.Serializable;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 * Conforma todos los datos de un cliente
 * 
 * Hace uso de validaciones adicionales para asegurar el formato
 */
public class Cliente implements Serializable {

	// Creada por Eclipse para correcto funcionamiento
	private static final long serialVersionUID = 1853776033746860312L;
	
	private Long idCliente;
	@NotNull(message = "El rut es obligatorio")
	@Size(min = 3, max = 20, message = "El rut debe tener mas de {min} caracteres.")
	private String rut;
	@Size(min = 5, max = 50, message = "El nombre debe tener entre {min} y {max} caracteres.")
	@NotNull(message = "El nombre es obligatorio")
	private String nombre;
	@Size(min = 5, max = 100, message = "La direccion debe tener entre {min} y {max} caracteres.")
	@NotNull(message = "La direccion es obligatoria")
	private String direccion;
	@Size(min = 6, max = 20, message = "El telefono debe tener entre {min} y {max} caracteres.")
	@NotNull(message = "El telefono es obligatoria")
	private String telefono;
	
	public Long getIdCliente() {
		return idCliente;
	}
	public void setIdCliente(Long idCliente) {
		this.idCliente = idCliente;
	}
	public String getRut() {
		return rut;
	}
	public void setRut(String rut) {
		this.rut = rut;
	}
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public String getDireccion() {
		return direccion;
	}
	public void setDireccion(String direccion) {
		this.direccion = direccion;
	}
	public String getTelefono() {
		return telefono;
	}
	public void setTelefono(String telefono) {
		this.telefono = telefono;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result	+ ((idCliente == null) ? 0 : idCliente.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Cliente other = (Cliente) obj;
		if (idCliente == null) {
			if (other.idCliente != null)
				return false;
		} else if (!idCliente.equals(other.idCliente))
			return false;
		return true;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("Cliente [idCliente=");
		builder.append(idCliente);
		builder.append(", rut=");
		builder.append(rut);
		builder.append(", nombre=");
		builder.append(nombre);
		builder.append("]");
		return builder.toString();
	}

}
