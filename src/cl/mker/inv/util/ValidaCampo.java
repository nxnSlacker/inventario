package cl.mker.inv.util;

import java.text.NumberFormat;
import java.util.Locale;

public class ValidaCampo {

	/**
	 * Metodo para validar rut
	 * 
	 * Este metodo elimina todo lo que no se un digito ni la letrs 'k' y 'K'
	 * 
	 * Luego asume que el �ltimo digito corresponde al verificador del rut y todo lo que se encuentra antes del el �ltimo digito se asume como la cifra correlativa
	 * 
	 * Retorna un rut formateado con puntos cada 3 numeros si le precede un numero y un gui�n antes de la �ltima cifra
	 * 
	 * Ejemplo: Este conjunto de caracteres representa un rut con caracteres impropios y una vez que se realiza la validaci�n retorna el rut correctamente formateado
	 * 
	 * 0000001.66///.75..9.22----/��\\?6--=
	 * 
	 * 16.675.922-6
	 * 
	 * @param rutCompleto
	 * @return
	 * @throws Exception
	 */
	public static String validaRut(String rutCompleto) throws ValidadorException {
		if(rutCompleto == null){
			throw new ValidadorException("rut no puede ser nulo");
		}
		
		rutCompleto = rutCompleto.toLowerCase();
		// Remplaza todos los caracteres que sean diferentes(^) de digitos (\\d), y diferentes de las letras k
		rutCompleto = rutCompleto.replaceAll("[^\\dk]", "");
		
		if(rutCompleto.length() < 3){ // Como minimo deben existir 2 caracteres, si el rut tuviese un solo caracter, el rutCorrelativo no alcanzar�a a tener caracteres
			throw new ValidadorException("Caracteres insuficientes.");
		}
		
		// Se asume que el ultimo caracter valido debe ser el digito verificador
		String rutVerificador = String.valueOf(rutCompleto.charAt(rutCompleto.length() - 1)); // En este caso busca DESDE el largo - 1
		
		// Se asume que los digitos anteriores al ultimo digito son la cifra correlativa del rut
		String rutCorrelativo = rutCompleto.substring(0, rutCompleto.length() - 1); // en este caso busca HASTA el largo - 1
		
		if(rutCorrelativo.length() < 1){ // Como minimo deben existir 2 caracteres, si el rut tuviese un solo caracter, el rutCorrelativo no alcanzar�a a tener caracteres
			throw new ValidadorException("Caracteres insuficientes.");
		}
		
		// Eliminamos todos los ceros al comienzo del rut
		rutCorrelativo = rutCorrelativo.replaceFirst("^0+", "");

		// Remplazamos todos lo que no sea digito de la cifra. \\D quiere decir DIFERENTE de cualquier diitoes una abreviaci�n de ^\\d
		rutCorrelativo = rutCorrelativo.replace("[\\D]", "");

		// Algoritmo para el modulo 11
		int contador = 2;
		int calculo = 0;
		
        for (int i = rutCorrelativo.length() - 1; i >= 0; i--) {
            contador = contador > 7 ? 2 : contador;
            calculo += Integer.parseInt(rutCorrelativo.substring(i, i + 1)) * contador++;
            System.out.print(rutCorrelativo.substring(i, i + 1));
        }
		
		calculo = 11 - (calculo % 11);
		String verificadorEncontrado;
		if( calculo == 10 ){
			verificadorEncontrado = "K";
		} else {
			if( calculo == 11 ){
				verificadorEncontrado = "0";
			} else { 
				verificadorEncontrado = String.valueOf(calculo);
			}
		}
		
		if( verificadorEncontrado.equalsIgnoreCase(rutVerificador) ){
			// con esto se le especifica el sistema de puntuaci�n num�rico chileno, en otros paises se utilizan comas en lugar de puntos
			Locale localidadChilena = new Locale("es", "CL"); 
			// Producir� cifras separadas por puntos y comas cuando sean decimales
			NumberFormat format = NumberFormat.getInstance(localidadChilena);
				
            return format.format(Long.valueOf(rutCorrelativo)) + "-" + rutVerificador;
		}
		else {
			throw new ValidadorException("El digito verificador del rut no es correcto: " + rutCompleto);
		}
	}
	
}
