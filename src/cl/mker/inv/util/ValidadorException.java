package cl.mker.inv.util;

public class ValidadorException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = -2622753697168398938L;

	public ValidadorException(String message) {
		super(message);
	}
	
	public ValidadorException(Throwable cause) {
		super(cause);
	}
	
	public ValidadorException(String message, Throwable cause) {
		super(message, cause);
		
	}
}
