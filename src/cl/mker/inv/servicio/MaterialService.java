package cl.mker.inv.servicio;

import java.util.List;

import javax.annotation.PostConstruct;
import javax.ejb.Singleton;
import javax.ejb.Startup;
import javax.inject.Inject;

import cl.mker.inv.modelo.Material;
import cl.mker.inv.persistencia.DAOException;
import cl.mker.inv.persistencia.MaterialDAO;

@Startup
@Singleton
public class MaterialService {

	@Inject
	private MaterialDAO materialDAO;
	
	@PostConstruct
	private void init(){
		System.out.println("MaterialDAO init");
	}
	
	public void setMaterialDAO(MaterialDAO materialDAO) {
		this.materialDAO = materialDAO;
	}
	
	public MaterialDAO getMaterialDAO() {
		return materialDAO;
	}
	
	public List<Material> listaMateriales() throws ServiceException {
		try {
			return materialDAO.listaMateriales();
		} catch (DAOException e) {
			throw new ServiceException(e);
		}
	}
	
	public boolean registraMaterial(Material material) throws ServiceException {
		try {
			if( material.getIdMaterial() != null ){
				return actualizaMaterial(material);
			} else {
				return materialDAO.registraMaterial(material);
			}
		} catch (DAOException e) {
			throw new ServiceException(e);
		}
	}
	
	public boolean actualizaMaterial(Material material) throws ServiceException {
		try {
			return materialDAO.actualizaMaterial(material);
		} catch (DAOException e) {
			throw new ServiceException(e);
		}
	}
	
	public Material buscaMaterialPorNombre(String nombre) throws ServiceException {
		try {
			return materialDAO.buscaMaterialPorNombre(nombre);
		} catch (DAOException e) {
			throw new ServiceException(e);
		}
	}
	
	public boolean eliminaMaterial(Material material) throws ServiceException {
		try {
			return materialDAO.eliminaMaterial(material);
		} catch (DAOException e) {
			throw new ServiceException(e);
		}
	}
}
