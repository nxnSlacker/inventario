package cl.mker.inv.servicio;

import java.util.List;

import javax.ejb.Singleton;
import javax.ejb.Startup;
import javax.inject.Inject;

import cl.mker.inv.modelo.Gasto;
import cl.mker.inv.persistencia.DAOException;
import cl.mker.inv.persistencia.GastoDAO;

@Startup
@Singleton
public class GastoService {

	@Inject
	private GastoDAO gastoDAO;

	public List<Gasto> listaGastos() throws ServiceException {
		try {
			return gastoDAO.listaGastos();
		} catch (DAOException e) {
			throw new ServiceException(e);
		}
	}

	public boolean registraGasto(Gasto gasto) throws ServiceException {
		try {
			if( gasto.getIdGasto() == null ){
				return gastoDAO.registraGasto(gasto);
			} else{
				return actualizaGasto(gasto);
			}
		} catch (DAOException e) {
			throw new ServiceException(e);
		}
	}

	public boolean actualizaGasto(Gasto gasto) throws ServiceException {
		try {
			return gastoDAO.actualizaGasto(gasto);
		} catch (DAOException e) {
			throw new ServiceException(e);
		}
	}

	public boolean eliminaGasto(Gasto gasto) throws ServiceException {
		try {
			return gastoDAO.eliminaGasto(gasto);
		} catch (DAOException e) {
			throw new ServiceException(e);
		}
	}
}
