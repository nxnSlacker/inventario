package cl.mker.inv.servicio;

import java.util.List;

import javax.annotation.PostConstruct;
import javax.ejb.Singleton;
import javax.ejb.Startup;
import javax.inject.Inject;

import cl.mker.inv.modelo.Proveedor;
import cl.mker.inv.persistencia.DAOException;
import cl.mker.inv.persistencia.ProveedorDAO;

@Startup
@Singleton
public class ProveedorService {

	@Inject
	private ProveedorDAO proveedorDAO;
	
	@PostConstruct
	private void init(){
		System.out.println("ProveedorDAO init");
	}
	
	public void setProveedorDAO(ProveedorDAO proveedorDAO) {
		this.proveedorDAO = proveedorDAO;
	}
	
	public ProveedorDAO getProveedorDAO() {
		return proveedorDAO;
	}
	
	public List<Proveedor> listaProveedores() throws ServiceException {
		try {
			return proveedorDAO.listaProveedores();
		} catch (DAOException e) {
			throw new ServiceException(e);
		}
	}
	
	public boolean registraProveedor(Proveedor proveedor) throws ServiceException {
		try {
			if( proveedor.getIdProveedor() != null ){
				return actualizaProveedor(proveedor);
			} else {
				return proveedorDAO.registraProveedor(proveedor);
			}
		} catch (DAOException e) {
			throw new ServiceException(e);
		}
	}
	
	public boolean actualizaProveedor(Proveedor proveedor) throws ServiceException {
		try {
			return proveedorDAO.actualizaProveedor(proveedor);
		} catch (DAOException e) {
			throw new ServiceException(e);
		}
	}
	
	public Proveedor buscaProveedorPorRut(String rut) throws ServiceException {
		try {
			return proveedorDAO.buscaProveedorPorRut(rut);
		} catch (DAOException e) {
			throw new ServiceException(e);
		}
	}
	
	public Proveedor buscaProveedorPorSurtido(long idSurtido) throws ServiceException {
		try {
			return proveedorDAO.buscaProveedorPorSurtido(idSurtido);
		} catch (DAOException e) {
			throw new ServiceException(e);
		}
	}
	
	public boolean eliminaProveedor(Proveedor proveedor) throws ServiceException {
		try {
			return proveedorDAO.eliminaProveedor(proveedor);
		} catch (DAOException e) {
			throw new ServiceException(e);
		}
	}
}
