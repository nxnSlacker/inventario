package cl.mker.inv.servicio;

import javax.annotation.PostConstruct;
import javax.ejb.Singleton;
import javax.ejb.Startup;
import javax.inject.Inject;

import cl.mker.inv.modelo.configuracion.CuentaCorreo;
import cl.mker.inv.modelo.configuracion.Empresa;
import cl.mker.inv.persistencia.OperacionalDAO;

@Startup
@Singleton
public class OperacionalService {

	@Inject
	private OperacionalDAO operacionalDAO;
	
	private Empresa empresa;
	
	private CuentaCorreo cuentaCorreo;
	
	@PostConstruct
	private void init(){
		System.out.println("OperacionalService init");
		// Solo se carga la primera vez que se inicia al sistema y luego se retorna el mismo objeto todas las veces
		empresa = operacionalDAO.listaDatosEmpresa();
		cuentaCorreo = operacionalDAO.listaDatosCuentaCorreo();
	}

	public Empresa listaDatosEmpresa(){
		return empresa;
	}

	public CuentaCorreo listaDatosCuentaCorreo() {
		return cuentaCorreo;
	}
}
