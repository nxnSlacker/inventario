package cl.mker.inv.servicio;

import java.util.ArrayList;
import java.util.List;

import javax.ejb.Singleton;
import javax.ejb.Startup;
import javax.inject.Inject;

import cl.mker.inv.modelo.Devolucion;
import cl.mker.inv.modelo.LineaMaterial;
import cl.mker.inv.modelo.Material;
import cl.mker.inv.modelo.Orden;
import cl.mker.inv.persistencia.DAOException;
import cl.mker.inv.persistencia.LineaMaterialDAO;

@Startup
@Singleton
public class LineaMaterialService {

	@Inject
	private LineaMaterialDAO lineaMaterialDAO;
	
	@Inject
	private DevolucionService devolucionService;
	
	
	@Inject
	private LineaSurtidoService lineaSurtidoService;
	
	public List<LineaMaterial> listaLineaMaterialFalsa(Orden orden) throws ServiceException {
		List<LineaMaterial> lineasMaterial = new ArrayList<LineaMaterial>();
		
		try {
			List<Material> materiales = lineaMaterialDAO.listaMaterialesFueraDeOrden(orden.getIdOrden());
			
			LineaMaterial lineaMaterial;
			for (Material material : materiales) {
				lineaMaterial = new LineaMaterial();
				lineaMaterial.setMaterial(material);
				lineaMaterial.getMaterial().setStockCliente(sumaCantidadLineaMaterialPorMaterial(lineaMaterial.getMaterial()));
				lineaMaterial.getMaterial().setStockProveedor(lineaSurtidoService.sumaCantidadLineaSurtidoPorMaterial(lineaMaterial.getMaterial()));
				lineaMaterial.setOrden(orden);
				
				int cantidadActual = lineaMaterial.getMaterial().getStockProveedor() + cantidadMinima(lineaMaterial) - lineaMaterial.getMaterial().getStockCliente();
				if ( cantidadActual <= 0 ){
					continue;
				}
				
				lineasMaterial.add(lineaMaterial);
			}
			
			return lineasMaterial;
		} catch (DAOException e){
			throw new ServiceException(e);
		}
	}
	
	private int sumaCantidadLineaMaterialPorMaterial(Material material) throws ServiceException{
		try {
			return lineaMaterialDAO.sumaCantidadLineaMaterialPorMaterial(material);
		} catch (DAOException e){
			throw new ServiceException(e);
		}
	}

	/**
	 * Asigna la actual cantidad a nuevaCantidad
	 * 
	 * @param surtido
	 * @return
	 * @throws ServiceException
	 */
	public List<LineaMaterial> listaLineaMaterial(Orden orden) throws ServiceException {
		List<LineaMaterial> lineasMaterial = null;
		try {
			lineasMaterial = lineaMaterialDAO.listaLineasMaterialPorIdOrden(orden.getIdOrden());
			
			for (LineaMaterial lineaMaterial : lineasMaterial) {
				lineaMaterial.setOrden(orden);
				lineaMaterial.setMaterial(lineaMaterialDAO.buscaMaterialPorIdLineaMaterial(lineaMaterial.getIdLineaMaterial()));
				lineaMaterial.getMaterial().setStockCliente(sumaCantidadLineaMaterialPorMaterial(lineaMaterial.getMaterial()));
				lineaMaterial.getMaterial().setStockProveedor(lineaSurtidoService.sumaCantidadLineaSurtidoPorMaterial(lineaMaterial.getMaterial()));
				lineaMaterial.setDevoluciones(devolucionService.buscaDevolucionesPorLineaMaterial(lineaMaterial));
				
				lineaMaterial.setCantidadAnterior(lineaMaterial.getCantidad());
			}
			
			return lineasMaterial;
		} catch (DAOException e){
			throw new ServiceException(e);
		}
	}

	public void agregaLineasMaterial(List<LineaMaterial> listadoMaterialesEnOrden) throws ServiceException {
		try {
			for (LineaMaterial lineaMaterial : listadoMaterialesEnOrden) {
				lineaMaterial.setValor(lineaMaterial.getMaterial().getValor());
				if( lineaMaterial.getIdLineaMaterial() == null || lineaMaterial.getIdLineaMaterial() <= 0){
					lineaMaterialDAO.agregaLineasMaterial(lineaMaterial);
				} else {
					lineaMaterialDAO.actualizaLineasMaterial(lineaMaterial);
				}
			}
		} catch (DAOException e){
			throw new ServiceException(e);
		}
	}

	public void eliminaLineasMaterial(List<LineaMaterial> listadoEliminar) throws ServiceException {
		try {
			for (LineaMaterial lineaMaterial : listadoEliminar) {
				lineaMaterialDAO.eliminaLineasMaterial(lineaMaterial);
			}
		} catch (DAOException e){
			throw new ServiceException(e);
		}
	}

	public boolean eliminaLineaMaterialPorOrden(Orden orden) throws ServiceException {
		try {
			return lineaMaterialDAO.eliminaLineasMaterialPorOrden(orden);
		} catch (DAOException e){
			throw new ServiceException(e);
		}
	}
	
	private int cantidadMinima(LineaMaterial lineaMaterial){
		int stock = 0;
		if( lineaMaterial.getDevoluciones() == null){
			return 0;
		}
		
		for (Devolucion devolucion : lineaMaterial.getDevoluciones()) {
			stock += devolucion.getCantidad();
		}
		
		return stock;
	}

	 public boolean tieneLineaMaterial(Material material) throws ServiceException {
		try {
			return listaLineasMaterialPorMaterial(material).size() > 0;
		} catch (ServiceException e){
			throw new ServiceException(e);
		}
	}

	private List<LineaMaterial> listaLineasMaterialPorMaterial(Material material) throws ServiceException {
		try {
			return lineaMaterialDAO.listaLineasMaterialPorMaterial(material);
		} catch (DAOException e){
			throw new ServiceException(e);
		}
	}
}
