package cl.mker.inv.servicio;

import java.util.List;

import javax.ejb.Singleton;
import javax.ejb.Startup;
import javax.inject.Inject;

import cl.mker.inv.modelo.Cliente;
import cl.mker.inv.modelo.Factura;
import cl.mker.inv.modelo.Orden;
import cl.mker.inv.persistencia.DAOException;
import cl.mker.inv.persistencia.OrdenDAO;

@Startup
@Singleton
public class OrdenService {

	@Inject
	private OrdenDAO ordenDAO;
	
	@Inject
	private ClienteService clienteService;
	
	@Inject
	private LineaMaterialService lineaMaterialService;
	
	public OrdenDAO getOrdenDAO() {
		return ordenDAO;
	}
	
	public void setOrdenDAO(OrdenDAO ordenDAO) {
		this.ordenDAO = ordenDAO;
	}
	
	/**
	 * Recupera el proveedor de cada surtido
	 * 
	 * @return
	 * @throws ServiceException
	 */
	public List<Orden> listaOrdenes() throws ServiceException{
		try {
			List<Orden> ordenes = ordenDAO.listaOrdenes();
			
			Cliente cliente;
			for (Orden orden : ordenes) {
				cliente = clienteService.buscaClientePorOrden(orden.getIdOrden());
				orden.setCliente(cliente);
			}
			
			return ordenes;
		} catch (DAOException e) {
			throw new ServiceException(e);
		}
	}
	
	public boolean agregaOrden(Orden orden) throws ServiceException {
		try {
			if(orden.getIdOrden() == null || orden.getIdOrden() <= 0){
				return ordenDAO.agregaOrden(orden);
			}else {
				return actualizaOrden(orden);
			}
		} catch (DAOException e) {
			throw new ServiceException(e);
		}
	}

	public boolean actualizaOrden(Orden orden) throws ServiceException{
		try {
			return ordenDAO.actualizaOrden(orden);
		} catch (DAOException e) {
			throw new ServiceException(e);
		}
	}
	
	public boolean eliminaOrden(Orden orden) throws ServiceException{
		try {
			return ( lineaMaterialService.eliminaLineaMaterialPorOrden(orden) && ordenDAO.eliminaOrden(orden));
		} catch (DAOException e) {
			throw new ServiceException(e);
		}
	}
	
	public List<Orden> listaOrdenesPorFactura(Factura factura) throws ServiceException{
		try {
			List<Orden> ordenes = ordenDAO.listaOrdenesPorFactura(factura); 
			
			for (Orden orden : ordenes) {
				orden.setFactura(factura);
			}
			
			asignaLineasMaterial(ordenes);
			
			return ordenes;
		} catch (DAOException e) {
			throw new ServiceException(e);
		}
	}

	public List<Orden> listaOrdenesSinFacturaPorCliente(Cliente cliente) throws ServiceException {
		try {
			List<Orden> ordenes = ordenDAO.listaOrdenesSinFacturaPorCliente(cliente); 
			
			asignaLineasMaterial(ordenes);
			
			return ordenes;
		} catch (DAOException e) {
			throw new ServiceException(e);
		}
	}
	
	public List<Orden> listaOrdenesPorCliente(Cliente cliente) throws ServiceException {
		try {
			return ordenDAO.listaOrdenesPorCliente(cliente);
		} catch (DAOException e) {
			throw new ServiceException(e);
		}
	}
	
	public boolean tieneOrdenes(Cliente cliente) throws ServiceException {
		try {
			return listaOrdenesPorCliente(cliente).size() > 0;
		} catch (ServiceException e) {
			throw new ServiceException(e);
		}
	}
	
	private void asignaLineasMaterial(List<Orden> ordenes) throws ServiceException {
		try {
			for (Orden orden : ordenes) {
				orden.setLineasMaterial(lineaMaterialService.listaLineaMaterial(orden));
			}
		} catch (ServiceException e) {
			throw new ServiceException(e);
		}
	}
	
	public void actualizaFacturaParaOrdenes(List<Orden> ordenes) throws ServiceException {
		try {
			for (Orden orden : ordenes) {
				System.out.println("+" + orden);
				ordenDAO.actualizaFacturaParaOrden(orden);
			}
		}catch(DAOException e){
			throw new ServiceException(e);
		}
	}
	
	public void descartaFacturaParaOrdenes(List<Orden> ordenes) throws ServiceException {
		try {
			for (Orden orden : ordenes) {
				System.out.println("-" + orden);
				ordenDAO.descartaFacturaParaOrden(orden);
			}
		}catch(DAOException e){
			throw new ServiceException(e);
		}
	}
}
