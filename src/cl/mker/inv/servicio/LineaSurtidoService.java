package cl.mker.inv.servicio;

import java.util.ArrayList;
import java.util.List;

import javax.ejb.Singleton;
import javax.ejb.Startup;
import javax.inject.Inject;

import cl.mker.inv.modelo.LineaSurtido;
import cl.mker.inv.modelo.Material;
import cl.mker.inv.modelo.Surtido;
import cl.mker.inv.persistencia.DAOException;
import cl.mker.inv.persistencia.LineaSurtidoDAO;

@Startup
@Singleton
public class LineaSurtidoService {

	@Inject
	private LineaSurtidoDAO lineaSurtidoDAO;
	
	public List<LineaSurtido> listaLineaSurtidoFalsa(Surtido surtido) throws ServiceException {
		List<LineaSurtido> lineasSurtido = new ArrayList<LineaSurtido>();
		try {
			List<Material> materiales = lineaSurtidoDAO.listaMaterialesFueraDeSurtido(surtido.getIdSurtido());
			
			LineaSurtido lineaSurtido;
			for (Material material : materiales) {
				lineaSurtido = new LineaSurtido();
				lineaSurtido.setMaterial(material);
				lineaSurtido.setSurtido(surtido);
				
				lineasSurtido.add(lineaSurtido);
			}
			
			return lineasSurtido;
		} catch (DAOException e){
			throw new ServiceException(e);
		}
	}
	
	/**
	 * Asigna la actual cantidad a nuevaCantidad
	 * 
	 * @param surtido
	 * @return
	 * @throws ServiceException
	 */
	public List<LineaSurtido> listaLineaSurtido(Surtido surtido) throws ServiceException {
		List<LineaSurtido> lineasSurtido = new ArrayList<LineaSurtido>();
		try {
			lineasSurtido = lineaSurtidoDAO.listaLineasSurtidoPorIdSurtido(surtido.getIdSurtido());
			
			for (LineaSurtido lineaSurtido : lineasSurtido) {
				lineaSurtido.setSurtido(surtido);
				lineaSurtido.setMaterial(lineaSurtidoDAO.buscaMaterialPorIdLineaSurtido(lineaSurtido.getIdLineaSurtido()));
				lineaSurtido.setNuevaCantidad(lineaSurtido.getCantidad());
			}
			
			return lineasSurtido;
		} catch (DAOException e){
			throw new ServiceException(e);
		}
	}

	public void agregaLineasSurtido(List<LineaSurtido> listadoMaterialesEnSurtido) throws ServiceException {
		try {
			for (LineaSurtido lineaSurtido : listadoMaterialesEnSurtido) {
				if( lineaSurtido.getIdLineaSurtido() == null || lineaSurtido.getIdLineaSurtido() <= 0){
					lineaSurtidoDAO.agregaLineasSurtido(lineaSurtido);
				} else {
					lineaSurtidoDAO.actualizaLineasSurtido(lineaSurtido);
				}
			}
		} catch (DAOException e){
			throw new ServiceException(e);
		}
	}

	public void eliminaLineasSurtido(List<LineaSurtido> listadoEliminar) throws ServiceException {
		try {
			for (LineaSurtido lineaSurtido : listadoEliminar) {
				lineaSurtidoDAO.eliminaLineasSurtido(lineaSurtido);
			}
		} catch (DAOException e){
			throw new ServiceException(e);
		}
	}

	public boolean eliminaLineaSurtidoPorSurtido(Surtido surtido) throws ServiceException {
		try {
			return lineaSurtidoDAO.eliminaLineasSurtidoPorSurtido(surtido);
		} catch (DAOException e){
			throw new ServiceException(e);
		}
	}

	public int sumaCantidadLineaSurtidoPorMaterial(Material material) throws ServiceException {
		try {
			return lineaSurtidoDAO.sumaCantidadLineaSurtidoPorMaterial(material);
		} catch (DAOException e){
			throw new ServiceException(e);
		}
	}

	public boolean tieneLineaSurtido(Material material) throws ServiceException {
		try {
			return listaLineasSurtidoPorMaterial(material).size() > 0;
		} catch (ServiceException e){
			throw new ServiceException(e);
		}
	}

	private List<LineaSurtido> listaLineasSurtidoPorMaterial(Material material) throws ServiceException {
		try {
			return lineaSurtidoDAO.listaLineasSurtidoPorMaterial(material);
		} catch (DAOException e){
			throw new ServiceException(e);
		}
	}

}