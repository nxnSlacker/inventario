package cl.mker.inv.servicio;

import java.util.List;

import javax.annotation.PostConstruct;
import javax.ejb.Singleton;
import javax.ejb.Startup;
import javax.inject.Inject;

import cl.mker.inv.modelo.Factura;
import cl.mker.inv.modelo.documento.ResumenFactura;
import cl.mker.inv.persistencia.DAOException;
import cl.mker.inv.persistencia.documento.ResumenFacturaDAO;

@Startup
@Singleton
public class ResumenFacturaService {

	@Inject
	private ResumenFacturaDAO facturaDAO;
	
	@PostConstruct
	private void init(){
		System.out.println("ResumenFacturaService init");
	}
	
	public List<ResumenFactura> listaResumenFacturas() throws ServiceException {
		try {
			return facturaDAO.listaResumenFacturas();
		} catch (DAOException e) {
			throw new ServiceException(e);
		}
	}
	
	public ResumenFactura listaResumenPorFactura(Factura factura) throws ServiceException {
		try {
			return facturaDAO.listaResumenPorFactura(factura);
		} catch (DAOException e) {
			throw new ServiceException(e);
		}
	}
}
