package cl.mker.inv.servicio;

import java.util.List;

import javax.annotation.PostConstruct;
import javax.ejb.Singleton;
import javax.ejb.Startup;
import javax.inject.Inject;

import cl.mker.inv.modelo.Devolucion;
import cl.mker.inv.modelo.LineaMaterial;
import cl.mker.inv.persistencia.DAOException;
import cl.mker.inv.persistencia.DevolucionDAO;

@Startup
@Singleton
public class DevolucionService {

	@Inject
	private DevolucionDAO devolucionDAO;
	
	@PostConstruct
	private void init(){
		System.out.println("DevolucionService init");
	}
	
	public List<Devolucion> buscaDevolucionesPorLineaMaterial(LineaMaterial lineaMaterial) throws ServiceException {
		try {
			return devolucionDAO.buscaDevolucionesPorLineaMaterial(lineaMaterial);
		} catch (DAOException e) {
			throw new ServiceException(e);
		}
	}
	
	public boolean registraDevolucion(Devolucion devolucion) throws ServiceException {
		try {
			if( devolucion.getIdDevolucion() != null ){
				return devolucionDAO.actualizaDevolucion(devolucion);
			} else {
				return devolucionDAO.registraDevolucion(devolucion);
			}
		} catch (DAOException e) {
			throw new ServiceException(e);
		}
	}
	
	public boolean eliminaDevolucion(Devolucion devolucion) throws ServiceException {
		try {
			return devolucionDAO.eliminaDevolucion(devolucion);
		} catch (DAOException e) {
			throw new ServiceException(e);
		}
	}
}
