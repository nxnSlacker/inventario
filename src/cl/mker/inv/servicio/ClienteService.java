package cl.mker.inv.servicio;

import java.util.List;

import javax.annotation.PostConstruct;
import javax.ejb.Singleton;
import javax.ejb.Startup;
import javax.inject.Inject;

import cl.mker.inv.modelo.Cliente;
import cl.mker.inv.persistencia.ClienteDAO;
import cl.mker.inv.persistencia.DAOException;

// Indica al servidor que debe instanciar esta clase apenas se carga la aplicacion
@Startup
// Indica al servidor que se instanciará una sola vez, por lo que todas las clases que hagan uso de ella
// estará ocupando la misma instancia
@Singleton
public class ClienteService {

	// ########################################
	//  Variables instanciadas automaticamente
	// ########################################
	@Inject
	private ClienteDAO clienteDAO;
	
	@PostConstruct
	private void init(){
		System.out.println("ClienteService init");
	}
	
	// ########################################
	//  Metodos de acceso
	// ########################################
	public void setClienteDAO(ClienteDAO clienteDAO) {
		this.clienteDAO = clienteDAO;
	}
	public ClienteDAO getClienteDAO() {
		return clienteDAO;
	}
	
	/**
	 * Delega la busqueda de los clientes al objeto clienteDAO que es el que se encarga de ir a la base de datos
	 * 
	 * @return
	 * @throws ServiceException
	 */
	public List<Cliente> listaClientes() throws ServiceException {
		try {
			return clienteDAO.listaClientes();
		} catch (DAOException e) {
			throw new ServiceException(e);
		}
	}
	
	/**
	 * Decide que metodo DAO se ejecutará dependiendo si el cliente ya existe o es nuevo
	 * 
	 * @param cliente
	 * @return
	 * @throws ServiceException
	 */
	public boolean registraCliente(Cliente cliente) throws ServiceException {
		try {
			if( cliente.getIdCliente() != null ){
				return actualizaCliente(cliente);
			} else {
				return clienteDAO.registraCliente(cliente);
			}
		} catch (DAOException e) {
			throw new ServiceException(e);
		}
	}
	
	/**
	 * Delega la actualización de un cliente al abjeto clienteDAO
	 * 
	 * @param cliente
	 * @return
	 * @throws ServiceException
	 */
	public boolean actualizaCliente(Cliente cliente) throws ServiceException {
		try {
			return clienteDAO.actualizaCliente(cliente);
		} catch (DAOException e) {
			throw new ServiceException(e);
		}
	}
	
	/**
	 * Delega la busqueda de un cliente por su rut al objeto clienteDAO
	 * 
	 * @param rut
	 * @return
	 * @throws ServiceException
	 */
	public Cliente buscaClientePorRut(String rut) throws ServiceException {
		try {
			return clienteDAO.buscaClientePorRut(rut);
		} catch (DAOException e) {
			throw new ServiceException(e);
		}
	}
	
	/**
	 * Delega la eliminacion de un cliente al objeto clienteDAO
	 * 
	 * @param cliente
	 * @return
	 * @throws ServiceException
	 */
	public boolean eliminaCliente(Cliente cliente) throws ServiceException {
		try {
			return clienteDAO.eliminaCliente(cliente);
		} catch (DAOException e) {
			throw new ServiceException(e);
		}
	}

	/**
	 * Delga la busqueda de un cliente por un codigo de orden al objeto clienteDAO
	 * 
	 * @param idOrden
	 * @return
	 * @throws ServiceException
	 */
	public Cliente buscaClientePorOrden(Long idOrden) throws ServiceException {
		try {
			return clienteDAO.buscaClientePorOrden(idOrden);
		} catch (DAOException e) {
			throw new ServiceException(e);
		}
	}
}
