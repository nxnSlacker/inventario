package cl.mker.inv.servicio;

import java.util.List;

import javax.annotation.PostConstruct;
import javax.ejb.Singleton;
import javax.ejb.Startup;
import javax.inject.Inject;

import cl.mker.inv.modelo.Abono;
import cl.mker.inv.modelo.documento.ResumenFactura;
import cl.mker.inv.persistencia.AbonoDAO;
import cl.mker.inv.persistencia.DAOException;

@Startup
@Singleton
public class AbonoService {

	@Inject
	private AbonoDAO abonoDAO;
	
	@PostConstruct
	private void init(){
		System.out.println("AbonoService init");
	}
	
	public List<Abono> buscaAbonosPorFactura(ResumenFactura factura) throws ServiceException {
		try {
			return abonoDAO.buscaAbonosPorFactura(factura);
		} catch (DAOException e) {
			throw new ServiceException(e);
		}
	}
	
	public boolean registraAbono(Abono abono, ResumenFactura resumenFactura) throws ServiceException {
		try {
			return abonoDAO.insertaAbono(abono, resumenFactura);
		} catch (DAOException e) {
			throw new ServiceException(e);
		}
	}
	
	public boolean eliminaAbono(Abono abono) throws ServiceException {
		try {
			return abonoDAO.eliminaAbono(abono);
		} catch (DAOException e) {
			throw new ServiceException(e);
		}
	}
}
