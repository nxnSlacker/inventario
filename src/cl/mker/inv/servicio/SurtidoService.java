package cl.mker.inv.servicio;

import java.util.List;

import javax.ejb.Singleton;
import javax.ejb.Startup;
import javax.inject.Inject;

import cl.mker.inv.modelo.Proveedor;
import cl.mker.inv.modelo.Surtido;
import cl.mker.inv.persistencia.DAOException;
import cl.mker.inv.persistencia.SurtidoDAO;

@Startup
@Singleton
public class SurtidoService {

	@Inject
	private SurtidoDAO surtidoDAO;
	
	@Inject
	private ProveedorService proveedorService;
	
	@Inject
	private LineaSurtidoService lineaSurtidoService;
	
	public SurtidoDAO getSurtidoDAO() {
		return surtidoDAO;
	}
	
	public void setSurtidoDAO(SurtidoDAO surtidoDAO) {
		this.surtidoDAO = surtidoDAO;
	}
	
	/**
	 * Recupera el proveedor de cada surtido
	 * 
	 * @return
	 * @throws ServiceException
	 */
	public List<Surtido> listaSurtidos() throws ServiceException{
		try {
			List<Surtido> surtidos = surtidoDAO.listaSurtidos();
			
			Proveedor proveedor;
			for (Surtido surtido : surtidos) {
				proveedor = proveedorService.buscaProveedorPorSurtido(surtido.getIdSurtido());
				surtido.setProveedor(proveedor);
			}
			
			return surtidos;
		} catch (DAOException e) {
			throw new ServiceException(e);
		}
	}
	
	public boolean agregaSurtido(Surtido surtido) throws ServiceException {
		try {
			if(surtido.getIdSurtido() == null || surtido.getIdSurtido() <= 0){
				return surtidoDAO.agregaSurtido(surtido);
			}else {
				return actualizaSurtido(surtido);
			}
		} catch (DAOException e) {
			throw new ServiceException(e);
		}
	}

	public boolean actualizaSurtido(Surtido surtido) throws ServiceException{
		try {
			return surtidoDAO.actualizaSurtido(surtido);
		} catch (DAOException e) {
			throw new ServiceException(e);
		}
	}
	
	public boolean eliminaSurtido(Surtido surtido) throws ServiceException{
		try {
			return ( lineaSurtidoService.eliminaLineaSurtidoPorSurtido(surtido) && surtidoDAO.eliminaSurtido(surtido));
		} catch (DAOException e) {
			throw new ServiceException(e);
		}
	}
	
	public List<Surtido> listaSurtidosPorProveedor(Proveedor proveedor) throws ServiceException{
		try {
			return surtidoDAO.listaSurtidosPorProveedor(proveedor);
		} catch (DAOException e) {
			throw new ServiceException(e);
		}
	}
	
	public boolean tieneSurtido(Proveedor proveedor) throws ServiceException {
		try {
			return listaSurtidosPorProveedor(proveedor).size() > 0;
		} catch (ServiceException e) {
			throw new ServiceException(e);
		}
	}

	public Surtido buscaSurtidoPorNumero(Long numeroFactura) throws ServiceException {
		try {
			return surtidoDAO.buscaSurtidoPorNumero(numeroFactura);
		} catch (DAOException e) {
			throw new ServiceException(e);
		}
	}
}
