package cl.mker.inv.servicio;

import java.util.List;
import java.util.Set;

import javax.annotation.PostConstruct;
import javax.inject.Inject;

import cl.mker.inv.persistencia.DAOException;
import cl.mker.inv.persistencia.PerfilDAO;
import cl.mker.inv.persistencia.PermisoDAO;
import cl.mker.inv.seguridad.Perfil;
import cl.mker.inv.seguridad.Usuario;

public class PerfilService {

	@Inject
	private PerfilDAO perfilDAO;
	
	@Inject
	private PermisoDAO permisoDAO;
	
	@PostConstruct
	private void init(){
		System.out.println("PerfilService init");
	}
	
	public Set<Perfil> listaPerfiles() throws ServiceException {
		try {
			return perfilDAO.listaPerfiles();
		} catch (DAOException e) {
			throw new ServiceException(e);
		}
	}
	
	private void buscaPermisosPorPerfil(Perfil perfil) throws ServiceException {
		try {
			perfil.setPermisos(permisoDAO.buscaPermisosPorPerfil(perfil));
		} catch (DAOException e) {
			throw new ServiceException(e);
		}
	}
	
	public void buscaPermisosPorPerfil(Set<Perfil> perfiles) throws ServiceException {
		try {
			for (Perfil perfil : perfiles) {
				buscaPermisosPorPerfil(perfil);
			}
		} catch (ServiceException e) {
			throw new ServiceException(e);
		}
	}
	
	public Set<Perfil> buscaPerfilesPorUsuario(Usuario usuario) throws ServiceException {
		try {
			Set<Perfil> perfiles = perfilDAO.buscaPerfilesPorUsuario(usuario);
			
			buscaPermisosPorPerfil(perfiles);
			
			return perfiles;
		} catch (DAOException e) {
			throw new ServiceException(e);
		}
	}

	public void agregaPerfiles(Usuario usuario,	List<Perfil> perfiles) throws ServiceException {
		try {
			for (Perfil perfil : perfiles) {
				perfilDAO.agregaPerfilUsuario(usuario, perfil);
			}
		} catch (DAOException e) {
			throw new ServiceException(e);
		}
	}

	public void eliminaPerfiles(Usuario usuario, List<Perfil> perfiles) throws ServiceException {
		try {
			for (Perfil perfil : perfiles) {
				perfilDAO.eliminaPerfilUsuario(usuario, perfil);
			}
		} catch (DAOException e) {
			throw new ServiceException(e);
		}
	}
	
	
}
