package cl.mker.inv.servicio.seguridad;

import java.util.Set;

import javax.inject.Inject;

import org.apache.shiro.authc.AuthenticationException;
import org.apache.shiro.authc.AuthenticationInfo;
import org.apache.shiro.authc.AuthenticationToken;
import org.apache.shiro.authc.LockedAccountException;
import org.apache.shiro.authc.SimpleAuthenticationInfo;
import org.apache.shiro.authc.UsernamePasswordToken;
import org.apache.shiro.authz.AuthorizationInfo;
import org.apache.shiro.authz.SimpleAuthorizationInfo;
import org.apache.shiro.realm.AuthorizingRealm;
import org.apache.shiro.subject.PrincipalCollection;

import cl.mker.inv.seguridad.Perfil;
import cl.mker.inv.seguridad.Permiso;
import cl.mker.inv.seguridad.Usuario;
import cl.mker.inv.servicio.PerfilService;
import cl.mker.inv.servicio.ServiceException;
import cl.mker.inv.servicio.UsuarioService;

public class UserRealm extends AuthorizingRealm {

	private static String REALM_NAME = "userRealm";

	@Inject
	private UsuarioService usuarioService;

	@Inject
	PerfilService perfilService;

	public UserRealm() {
		setName(REALM_NAME);
	}

	protected AuthenticationInfo doGetAuthenticationInfo(AuthenticationToken authToken) throws AuthenticationException {
		UsernamePasswordToken token = (UsernamePasswordToken) authToken;
		
		try {
			Usuario usuario = usuarioService.buscaUsuarioPorUsername(token.getUsername());
			if (usuario != null) {
				if( ! usuario.isHabilitado() ){
					throw new LockedAccountException("Cuenta bloqueada");
				}
				return new SimpleAuthenticationInfo(usuario.getUsername(),	usuario.getPassword(), getName());
			}
		} catch (ServiceException e) {
			e.printStackTrace();
		}
		
		return null;
	}

	protected AuthorizationInfo doGetAuthorizationInfo(PrincipalCollection principals) {
		String userId = (String) principals.fromRealm(getName()).iterator().next();
		Usuario usuario;
		try {
			usuario = usuarioService.buscaUsuarioPorUsername(userId);
		} catch (ServiceException e1) {
			e1.printStackTrace();
			return null;
		}
		if (usuario != null) {
			SimpleAuthorizationInfo info = new SimpleAuthorizationInfo();
			Set<Perfil> perfiles;
			try {
				perfiles = perfilService.buscaPerfilesPorUsuario(usuario);
			} catch (ServiceException e) {
				e.printStackTrace();
				return null;
			}
			for (Perfil perfil : perfiles) {
				info.addRole(perfil.getGrupo());
				for (Permiso permiso : perfil.getPermisos()) {
					info.addStringPermission(permiso.getAccion());
				}
			}
			return info;
		} else {
			return null;
		}
	}

}
