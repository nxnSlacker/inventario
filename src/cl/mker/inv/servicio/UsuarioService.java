package cl.mker.inv.servicio;

import java.util.List;
import java.util.UUID;

import javax.ejb.Singleton;
import javax.ejb.Startup;
import javax.inject.Inject;

import org.apache.shiro.crypto.hash.Sha256Hash;

import cl.mker.inv.persistencia.DAOException;
import cl.mker.inv.persistencia.UsuarioDAO;
import cl.mker.inv.seguridad.Usuario;
import cl.mker.inv.servicio.mail.EmailService;

@Startup
@Singleton
public class UsuarioService {

	@Inject 
	private UsuarioDAO usuarioDAO;
	
	@Inject
	private PerfilService perfilService;
	
	@Inject
	private EmailService emailService;
	
	public List<Usuario> listaUsuarios() throws ServiceException {
		try {
			List<Usuario> usuarios = usuarioDAO.listaUsuarios();
			
			for (Usuario usuario : usuarios) {
				usuario.setPerfiles(perfilService.buscaPerfilesPorUsuario(usuario));
			}
			
			return usuarios;
		} catch (DAOException e) {
			throw new ServiceException(e);
		}
	}
	
	public Usuario buscaUsuarioPorUsername(String username) throws ServiceException {
		try {
			return usuarioDAO.buscaUsuarioPorUsername(username);
		} catch (DAOException e) {
			throw new ServiceException(e);
		}
	}
	
	public boolean registraUsuario(Usuario usuario) throws ServiceException {
		try {
			usuario.setUsername(usuario.getUsername().trim());
			
			if( usuario.getIdUsuario() == null ){
				if( ! insertaUsuario(usuario) ){
					return false;
				}
			} else {
				if( ! actualizaUsuario(usuario) ){
					return false;
				}
			}
			
			if( ! usuario.isEnviaMail() ){
				return true;
			}
			
			usuario.setCodigoActivacion(UUID.randomUUID().toString().replace("-", ""));

			if( actualizaActivacion(usuario) ){ // idUsuario, codigoActivacion
				emailService.enviaCodigoActivacion(usuario);
			}
			
			return true;
		} catch (ServiceException e) {
			throw new ServiceException(e);
		}
	}

	public boolean insertaUsuario(Usuario usuario) throws ServiceException {
		try {
			return usuarioDAO.insertUsuario(usuario);			
		} catch (DAOException e) {
			throw new ServiceException(e);
		}
	}
	
	private boolean actualizaUsuario(Usuario usuario) throws ServiceException {
		try {
			return usuarioDAO.actualizaUsuario(usuario);
		} catch (DAOException e) {
			throw new ServiceException(e);
		}
	}
	
	private boolean actualizaActivacion(Usuario usuario) throws ServiceException {
		try {
			return usuarioDAO.actualizaCodigoActivacion(usuario);
		} catch (DAOException e) {
			throw new ServiceException(e);
		}
	}
	
	public boolean actualizaPassword(Usuario usuario) throws ServiceException {
		try {
			usuario.setPassword(new Sha256Hash(usuario.getPassword()).toHex());
			return usuarioDAO.actualizaPassword(usuario);
		} catch (DAOException e) {
			throw new ServiceException(e);
		}
	}
	
	public boolean eliminaUsuario(Usuario usuario) throws ServiceException {
		try {
			return usuarioDAO.eliminaUsuario(usuario);
		} catch (DAOException e) {
			throw new ServiceException(e);
		}
	}
	
	public static void main(String[] args) {
		System.out.println(new Sha256Hash("55ec319").toHex());
		System.out.println(UUID.randomUUID().toString().replace("-", ""));
	}

}
