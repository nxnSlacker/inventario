package cl.mker.inv.servicio;

public class ServiceException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 2662136043959480936L;

	public ServiceException(String message) {
		super(message);
	}
	
	public ServiceException(Throwable cause) {
		super(cause);
	}
	
	public ServiceException(String message, Throwable cause) {
		super(message, cause);
	}
}
