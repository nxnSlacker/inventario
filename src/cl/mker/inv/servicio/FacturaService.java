package cl.mker.inv.servicio;

import java.util.List;

import javax.annotation.PostConstruct;
import javax.ejb.Singleton;
import javax.ejb.Startup;
import javax.inject.Inject;

import cl.mker.inv.modelo.Cliente;
import cl.mker.inv.modelo.Factura;
import cl.mker.inv.modelo.Orden;
import cl.mker.inv.modelo.documento.ResumenFactura;
import cl.mker.inv.persistencia.DAOException;
import cl.mker.inv.persistencia.FacturaDAO;

@Startup
@Singleton
public class FacturaService {

	@Inject
	private FacturaDAO facturaDAO;
	
	@Inject
	private ResumenFacturaService resumenFacturaService;
	
	@Inject
	private ImpuestoService impuestoService;
	
	@PostConstruct
	private void init(){
		System.out.println("FacturaService init");
	}
	
	public List<Factura> listaFacturas() throws ServiceException {
		try {
			List<Factura> facturas = facturaDAO.listaFacturas();
			
			for (Factura factura : facturas) {
				factura.setImpuesto(impuestoService.buscaImpuestoPorFactura(factura));
				calculaTotalPorFactura(factura);
			}
			
			return facturas;
		} catch (DAOException e) {
			throw new ServiceException(e);
		}
	}
	
	private void calculaTotalPorFactura(Factura factura) throws ServiceException {
		try {
			ResumenFactura resumenFactura = resumenFacturaService.listaResumenPorFactura(factura);
			factura.setTotalBruto(resumenFactura.getTotalBruto());
			factura.setTotalNeto(resumenFactura.getTotalNeto());
			
			factura.setCantidadAbonos(resumenFactura.getCantidadAbonos());
			factura.setTotalAbonado(resumenFactura.getTotalAbonado());
			factura.setDeudaActual(resumenFactura.getDeudaActual());
			
		} catch (ServiceException e) {
			throw new ServiceException(e); 
		}
	}

	public Cliente buscaClientePorFactura(Factura factura) throws ServiceException {
		try {
			return facturaDAO.buscaClientePorFactura(factura);
		} catch (DAOException e) {
			throw new ServiceException(e);
		}
	}
	
	public Factura buscaFacturaPorOrden(Orden orden) throws ServiceException {
		try {
			return facturaDAO.buscaFacturaPorOrden(orden);
		} catch (DAOException e) {
			throw new ServiceException(e);
		}
	}
	
	public boolean regsitraFactura(Factura factura) throws ServiceException {
		try {
			return facturaDAO.registraFactura(factura);
		} catch (DAOException e) {
			throw new ServiceException(e);
		}
	}

	public boolean eliminaFactura(Factura factura) throws ServiceException {
		try {
			return facturaDAO.eliminaFactura(factura);
		} catch (DAOException e) {
			throw new ServiceException(e);
		}
	}

	public boolean actualizaFactura(Factura factura) throws ServiceException{
		try {
			return facturaDAO.actualizaFactura(factura);
		} catch (DAOException e) {
			throw new ServiceException(e);
		}
	}

	public boolean tieneFactura(Orden orden) throws ServiceException {
		try {
			return buscaFacturaPorOrden(orden) != null;
		} catch (ServiceException e) {
			throw new ServiceException(e);
		}
	}
	
	public boolean descartaOrdenesParaFactura(Factura factura) throws ServiceException {
		try {
			return facturaDAO.descartaOrdenesParaFactura(factura);
		} catch (DAOException e) {
			throw new ServiceException(e);
		}
	}

	public Factura buscaFacturaPorNumero(Long numeroFactura) throws ServiceException {
		try {
			return facturaDAO.buscaFacturaPorNumero(numeroFactura);
		} catch (DAOException e) {
			throw new ServiceException(e);
		}
	}
	
}
