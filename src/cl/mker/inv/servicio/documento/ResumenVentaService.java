package cl.mker.inv.servicio.documento;

import java.util.Date;
import java.util.List;

import javax.ejb.Singleton;
import javax.ejb.Startup;
import javax.inject.Inject;

import cl.mker.inv.modelo.documento.ResumenVenta;
import cl.mker.inv.persistencia.DAOException;
import cl.mker.inv.persistencia.documento.ResumenVentaDAO;
import cl.mker.inv.servicio.ServiceException;

@Startup
@Singleton
public class ResumenVentaService {

	@Inject
	private ResumenVentaDAO resumenVentaDAO;
	
	public List<ResumenVenta> listaResumenVentas(Date fechaInicio, Date fechaFin) throws ServiceException {
		try {
			return resumenVentaDAO.listaResumenVentas(new java.sql.Date(fechaInicio.getTime()), new java.sql.Date(fechaFin.getTime()));
		} catch (DAOException e) {
			throw new ServiceException(e);
		}
	}

	
}
