package cl.mker.inv.servicio.documento;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.Date;
import java.util.List;

import cl.mker.inv.modelo.documento.ResumenVenta;

import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.PageSize;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;

public class ReporteResumenVentaService {

	public ByteArrayOutputStream listaResumenFacturas(List<ResumenVenta> resumenVentas) throws DocumentException, IOException {
    	ByteArrayOutputStream baos = new ByteArrayOutputStream();
        // step 0
    	if( resumenVentas == null || resumenVentas.isEmpty() ){
    		return null;
    	}
    	// step 1
        Document document = new Document(PageSize.A4.rotate());
        // step 2
        PdfWriter.getInstance(document, baos);
        // step 3
        document.open();
        // step 4 Contenido
        document.add(getTable(resumenVentas));
        document.newPage();
        // step 5
        document.close();
        
        return baos;
    }
 
    private PdfPTable getTable(List<ResumenVenta> resumenVentas) throws DocumentException, IOException {
    	// Porporcion del ancho de las columna respecto a las demas
        PdfPTable table = new PdfPTable(new float[] { .7f, 1, 1, 1, 1, 1, 1, .5f, 0.8f, .8f, .9f, .9f });
        // Asigna el total del ancho 100%
        table.setWidthPercentage(100f);
        
        table.getDefaultCell().setPadding(3);
        table.getDefaultCell().setUseAscender(true);
        table.getDefaultCell().setUseDescender(true);
        table.getDefaultCell().setColspan(12);
        
        table.getDefaultCell().setBackgroundColor(BaseColor.RED);
        table.getDefaultCell().setHorizontalAlignment(Element.ALIGN_CENTER);
        table.addCell(new Date().toString());
        
        table.getDefaultCell().setHorizontalAlignment(Element.ALIGN_LEFT);
        table.getDefaultCell().setColspan(1);
        table.getDefaultCell().setBackgroundColor(BaseColor.YELLOW);
        
        for (int i = 0; i < 1; i++) {
            table.addCell("Id orden");
            table.addCell("Fecha");
            table.addCell("Numero factura");
            table.addCell("Cliente");
            table.addCell("Rut");
            table.addCell("Id Material");
            table.addCell("Material");
            table.addCell("IVA");
            table.addCell("Cantidad");
            table.addCell("Valor");
            table.addCell("Total Bruto");
            table.addCell("Total Neto");
        }
        
        table.getDefaultCell().setBackgroundColor(null);
        table.setHeaderRows(2);

        for (ResumenVenta resumenVenta : resumenVentas) {
        	table.addCell(String.valueOf(resumenVenta.getIdOrden()));
        	table.addCell(String.valueOf(resumenVenta.getFecha()));
        	table.addCell(String.valueOf(resumenVenta.getNumeroFactura()));
        	table.addCell(String.valueOf(resumenVenta.getNombreCliente()));
        	table.addCell(String.valueOf(resumenVenta.getRut()));
        	table.addCell(String.valueOf(resumenVenta.getIdMaterial()));
        	table.addCell(String.valueOf(resumenVenta.getNombreMaterial()));
        	table.addCell(String.valueOf(resumenVenta.getPorcentaje()));
        	table.addCell(String.valueOf(resumenVenta.getCantidad()));
        	table.addCell(String.valueOf(resumenVenta.getValor()));
        	table.addCell(String.valueOf(resumenVenta.getTotalBruto()));
        	table.addCell(String.valueOf(resumenVenta.getTotalNeto()));
        }
        
        return table;
    }
}
