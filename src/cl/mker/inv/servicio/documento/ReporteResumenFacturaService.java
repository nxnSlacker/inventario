package cl.mker.inv.servicio.documento;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.ejb.Singleton;
import javax.ejb.Startup;

import cl.mker.inv.modelo.documento.ResumenFactura;

import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.PageSize;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;

@Startup
@Singleton
public class ReporteResumenFacturaService {

    public ByteArrayOutputStream listaResumenFacturas(List<ResumenFactura> resumenesFactura, int diasMoroso) throws DocumentException, IOException {
    	ByteArrayOutputStream baos = new ByteArrayOutputStream();
        // step 0
    	if( resumenesFactura == null || resumenesFactura.isEmpty() ){
    		return null;
    	}
    	// step 1
        Document document = new Document(PageSize.A4.rotate());
        // step 2
        PdfWriter.getInstance(document, baos);
        // step 3
        document.open();
        // step 4 Contenido
        document.add(getTable(resumenesFactura, diasMoroso));
        document.newPage();
        // step 5
        document.close();
        
        return baos;
    }
 
    private PdfPTable getTable(List<ResumenFactura> resumenesFactura, int diasMoroso) throws DocumentException, IOException {
    	// Porporcion del ancho de las columna respecto a las demas
        PdfPTable table = new PdfPTable(new float[] { 1, 1, 1, 1, 1, 1, 1, 1, 1 });
        // Asigna el total del ancho 100%
        table.setWidthPercentage(100f);
        
        table.getDefaultCell().setPadding(3);
        table.getDefaultCell().setUseAscender(true);
        table.getDefaultCell().setUseDescender(true);
        // Cantidad de columnas
        table.getDefaultCell().setColspan(9);
        
        table.getDefaultCell().setBackgroundColor(BaseColor.RED);
        table.getDefaultCell().setHorizontalAlignment(Element.ALIGN_CENTER);
        table.addCell(new Date().toString());
        
        table.getDefaultCell().setHorizontalAlignment(Element.ALIGN_LEFT);
        table.getDefaultCell().setColspan(1);
        table.getDefaultCell().setBackgroundColor(BaseColor.YELLOW);
        
        // Identifica a cada columna
        for (int i = 0; i < 1; i++) {
            table.addCell("Id");
            table.addCell("Numero factura");
            table.addCell("Fecha");
            table.addCell("IVA");
            table.addCell("Total Bruto");
            table.addCell("Total Neto");
            table.addCell("N� Abonos");
            table.addCell("Total Abonados");
            table.addCell("Deuda Actual");
        }
        
        table.getDefaultCell().setBackgroundColor(null);
        table.setHeaderRows(2);

        Date fechaActual = new Date();
        Calendar calendarioprueba = Calendar.getInstance();
        
        for (ResumenFactura resumenFactura : resumenesFactura) {
        	if( resumenFactura.getCantidadAbonos() <= 0 ){ // Si ha pagado nada
        		calendarioprueba.setTime(resumenFactura.getFecha());
        		calendarioprueba.add(Calendar.DAY_OF_MONTH, diasMoroso); // Agregamos un mes desde la fecha de creaci�n de la factura
        		
        		if( fechaActual.after(calendarioprueba.getTime()) ){ // Si la fecha actual sigue siendo inferior a la fecha de la factura con el mes extra entonces no han hab�do pagos
        			table.getDefaultCell().setBackgroundColor(BaseColor.ORANGE); // El color de los morosos
        		}
        	}
        	if( resumenFactura.getDeudaActual() == 0 ){
        		table.getDefaultCell().setBackgroundColor(BaseColor.GREEN);
        	}
        	table.addCell(String.valueOf(resumenFactura.getIdFactura()));
        	table.addCell(String.valueOf(resumenFactura.getNumeroFactura()));
        	table.addCell(String.valueOf(resumenFactura.getFecha()));
        	table.addCell(String.valueOf(resumenFactura.getPorcentaje()));
        	table.addCell(String.valueOf(resumenFactura.getTotalBruto()));
        	table.addCell(String.valueOf(resumenFactura.getTotalNeto()));
        	table.addCell(String.valueOf(resumenFactura.getCantidadAbonos()));
        	table.addCell(String.valueOf(resumenFactura.getTotalAbonado()));
        	table.addCell(String.valueOf(resumenFactura.getDeudaActual()));
        	table.getDefaultCell().setBackgroundColor(null);
        }
        return table;
    }
    
    public static void main(String[] args) {
		Date fechaPrueba = new Date();
		Date fechaActual = new Date();
		
		Calendar calendarioprueba = Calendar.getInstance();
		calendarioprueba.setTime(fechaPrueba);
		calendarioprueba.add(Calendar.MONTH, 1);
		
		System.out.println("Fecha actual " + fechaActual);
		System.out.println("Fecha prueba " + calendarioprueba.getTime());
		
		System.out.println("Fecha actual mayor: " + fechaActual.after(calendarioprueba.getTime()));
	}
}
