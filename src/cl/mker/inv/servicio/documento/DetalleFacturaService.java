package cl.mker.inv.servicio.documento;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.Date;
import java.util.List;

import javax.ejb.Singleton;
import javax.ejb.Startup;
import javax.inject.Inject;

import cl.mker.inv.modelo.Factura;
import cl.mker.inv.modelo.Impuesto;
import cl.mker.inv.modelo.LineaMaterial;
import cl.mker.inv.modelo.Orden;
import cl.mker.inv.modelo.documento.DetalleFactura;
import cl.mker.inv.servicio.ImpuestoService;
import cl.mker.inv.servicio.OrdenService;
import cl.mker.inv.servicio.ServiceException;

import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.PageSize;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;

@Startup
@Singleton
public class DetalleFacturaService {

	@Inject
	private OrdenService ordenService;
	
	@Inject
	private ImpuestoService impuestoService;
	
	public DetalleFactura buscaDetallePorFactura(Factura factura) throws ServiceException {
		try {
			DetalleFactura detalleFactura = new DetalleFactura();
			factura.setImpuesto(impuestoService.buscaImpuestoPorFactura(factura));
			detalleFactura.setFactura(factura);
			detalleFactura.setOrdenes(ordenService.listaOrdenesPorFactura(factura));
			return detalleFactura;
		} catch (ServiceException e) {
			throw new ServiceException(e);
		}
	}

	public ByteArrayOutputStream buscaContenidoDetalleFactura(Factura factura) throws ServiceException{
		try {
			DetalleFactura detalleFactura = buscaDetallePorFactura(factura);
			return buscaContenidoDetalleFactura(detalleFactura);
		} catch (Exception e) {
			throw new ServiceException(e);
		}
	}
	
	public ByteArrayOutputStream buscaContenidoDetalleFactura(DetalleFactura detalleFactura) throws DocumentException, IOException {
    	ByteArrayOutputStream baos = new ByteArrayOutputStream();
        // step 0
    	if( detalleFactura == null ){
    		return null;
    	}
    	// step 1
        Document document = new Document(PageSize.A4.rotate());
        // step 2
        PdfWriter.getInstance(document, baos);
        // step 3
        document.open();
        // step 4 Contenido
        document.add(getTablaFactura(detalleFactura.getFactura()));
        getTablasOrdenes(document, detalleFactura.getOrdenes(), detalleFactura.getFactura().getImpuesto());
        document.newPage();
        // step 5
        document.close();
        
        return baos;
    }
 
	private PdfPTable getTablaFactura(Factura factura){
		// Porporcion del ancho de las columna respecto a las demas
        PdfPTable table = new PdfPTable(new float[] { 1, 1, 1, 1, 1, 1, 1, 1, 1 });
        // Asigna el total del ancho 100%
        table.setWidthPercentage(100f);
        
        table.getDefaultCell().setPadding(3);
        table.getDefaultCell().setUseAscender(true);
        table.getDefaultCell().setUseDescender(true);
        // Cantidad de columnas
        table.getDefaultCell().setColspan(9);
        
        table.getDefaultCell().setBackgroundColor(BaseColor.RED);
        table.getDefaultCell().setHorizontalAlignment(Element.ALIGN_CENTER);
        table.addCell(new Date().toString());
        
        table.getDefaultCell().setHorizontalAlignment(Element.ALIGN_LEFT);
        table.getDefaultCell().setColspan(1);
        table.getDefaultCell().setBackgroundColor(BaseColor.YELLOW);
        
        // Identifica a cada columna
        for (int i = 0; i < 1; i++) {
            table.addCell("Id");
            table.addCell("Numero factura");
            table.addCell("Fecha");
            table.addCell("IVA");
            table.addCell("Total Bruto");
            table.addCell("Total Neto");
            table.addCell("N� Abonos");
            table.addCell("Total Abonados");
            table.addCell("Deuda Actual");
        }
        
        table.getDefaultCell().setBackgroundColor(null);
        table.setHeaderRows(2);

        //Date fechaActual = new Date();
        //Calendar calendarioprueba = Calendar.getInstance();
        
        table.addCell(String.valueOf(factura.getIdFactura()));
        table.addCell(String.valueOf(factura.getNumeroFactura()));
        table.addCell(String.valueOf(factura.getFecha()));
        table.addCell(String.valueOf(factura.getImpuesto().getPorcentaje()));
        table.addCell(String.valueOf(factura.getTotalBruto()));
        table.addCell(String.valueOf(factura.getTotalNeto()));
        table.addCell(String.valueOf(factura.getCantidadAbonos()));
        table.addCell(String.valueOf(factura.getTotalAbonado()));
        table.addCell(String.valueOf(factura.getDeudaActual()));
        table.getDefaultCell().setBackgroundColor(null);
        	
        return table;
	}
	
	private void getTablasOrdenes(Document document, List<Orden> ordenes, Impuesto impuesto){
		for (Orden orden : ordenes) {
			try {
				document.add(getTablaOrden(orden, impuesto));
			} catch (DocumentException e) {
				e.printStackTrace();
			}
		}
	}
	
	private PdfPTable getTablaOrden(Orden orden, Impuesto impuesto){
		// Porporcion del ancho de las columna respecto a las demas
        PdfPTable table = new PdfPTable(new float[] { 1, 1, 1, 1, 1, 1 });
        // Asigna el total del ancho 100%
        table.setWidthPercentage(100f);
        
        table.getDefaultCell().setPadding(3);
        table.getDefaultCell().setUseAscender(true);
        table.getDefaultCell().setUseDescender(true);
        // Cantidad de columnas
        table.getDefaultCell().setColspan(6);
        
        table.getDefaultCell().setBackgroundColor(BaseColor.RED);
        table.getDefaultCell().setHorizontalAlignment(Element.ALIGN_CENTER);
        
        table.addCell("Orden " + orden.getIdOrden());
        table.addCell("Fecha " + orden.getFecha().toString());
        
        table.getDefaultCell().setHorizontalAlignment(Element.ALIGN_LEFT);
        table.getDefaultCell().setColspan(1);
        table.getDefaultCell().setBackgroundColor(BaseColor.YELLOW);
        
        // Identifica a cada columna
        for (int i = 0; i < 1; i++) {
            table.addCell("Id");
            table.addCell("Nombre");
            table.addCell("Cantidad");
            table.addCell("Valor");
            table.addCell("Total Bruto");
            table.addCell("Total Neto");
        }
        
        table.getDefaultCell().setBackgroundColor(null);
        table.setHeaderRows(2);
        //Date fechaActual = new Date();
        //Calendar calendarioprueba = Calendar.getInstance();
        
        long totalBruto = 0;
        long totalNeto = 0;
        
        for (LineaMaterial lineaMaterial : orden.getLineasMaterial()) {
        	table.addCell(String.valueOf(lineaMaterial.getMaterial().getIdMaterial()));
        	table.addCell(String.valueOf(lineaMaterial.getMaterial().getNombre()));
        	table.addCell(String.valueOf(lineaMaterial.getCantidad()));
        	table.addCell(String.valueOf(lineaMaterial.getValor()));
        	table.addCell(String.valueOf(lineaMaterial.getValor() * lineaMaterial.getCantidad()));
        	table.addCell(String.valueOf((long) lineaMaterial.getValor() * lineaMaterial.getCantidad() * ( 1 + ( impuesto.getPorcentaje() / 100))));
        	
        	totalBruto += lineaMaterial.getValor() * lineaMaterial.getCantidad();
        	totalNeto += (long) lineaMaterial.getValor() * lineaMaterial.getCantidad() * ( 1 + ( impuesto.getPorcentaje() / 100));
		}
        
        
        	
        return table;
	}
	
}
