package cl.mker.inv.servicio.documento;

import java.util.List;

import javax.annotation.PostConstruct;
import javax.ejb.Singleton;
import javax.ejb.Startup;
import javax.inject.Inject;

import cl.mker.inv.modelo.Material;
import cl.mker.inv.modelo.documento.StockMaterial;
import cl.mker.inv.persistencia.DAOException;
import cl.mker.inv.persistencia.documento.StockMaterialDAO;
import cl.mker.inv.servicio.ServiceException;

@Startup
@Singleton
public class StockMaterialService {

	@Inject
	private StockMaterialDAO stockMaterialDAO;
	
	@PostConstruct
	private void init(){
		System.out.println("StockMaterialService init");
	}
	
	public List<StockMaterial> listaStockMateriales() throws ServiceException {
		try {
			return stockMaterialDAO.listaStockMateriales();
		} catch (DAOException e) {
			throw new ServiceException(e);
		}
	}
	
	public int buscaStockPorMaterial(Material material) throws ServiceException {
		List<StockMaterial> stockMateriales;
		try {
			stockMateriales = listaStockMateriales();
			
			for (StockMaterial stockMaterial : stockMateriales) {
				if( (stockMaterial.getIdMaterial() != null) && (stockMaterial.getIdMaterial().equals(material.getIdMaterial())) ){
					return stockMaterial.getCantidadDisponible();
				}
			}
		} catch (ServiceException e) {
			e.printStackTrace();
		}
		
		return 0;
	}
}
