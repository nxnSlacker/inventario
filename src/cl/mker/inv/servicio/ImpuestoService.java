package cl.mker.inv.servicio;

import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.ejb.Singleton;
import javax.ejb.Startup;
import javax.inject.Inject;

import cl.mker.inv.modelo.Factura;
import cl.mker.inv.modelo.Impuesto;
import cl.mker.inv.persistencia.DAOException;
import cl.mker.inv.persistencia.ImpuestoDAO;

@Startup
@Singleton
public class ImpuestoService {

	@Inject
	private ImpuestoDAO impuestoDAO;
	
	@PostConstruct
	private void init(){
		System.out.println("ImpuestoService init ");
	}
	
	public Impuesto recuperaImpuestoActual() throws ServiceException {
		try {
			List<Impuesto> impuestos = listaImpuestosOrdenadosPorFecha();
			Date fechaActual = new Date();
			Collections.reverse(impuestos);
			for (Impuesto impuesto : impuestos) {
				if( impuesto.getFecha().compareTo(fechaActual) <= 0 ){
					return impuesto;
				}
			}
			
			throw new ServiceException("No se han agregado impuestos al sistema");
		} catch (ServiceException e) {
			throw new ServiceException(e);
		}
	}
	
	public List<Impuesto> listaImpuestosOrdenadosPorFecha() throws ServiceException {
		try {
			List<Impuesto> impuestos = listaImpuestos();
			
			Collections.sort(impuestos, new Comparator<Impuesto>() {
				@Override
				public int compare(Impuesto i1, Impuesto i2) {
					return i1.getFecha().compareTo(i2.getFecha());
				}
			});
			
			return impuestos;
		} catch (ServiceException e) {
			throw new ServiceException(e);
		}
	}
	
	public List<Impuesto> listaImpuestos() throws ServiceException {
		try {
			return impuestoDAO.listaImpuestos();
		} catch (DAOException e) {
			throw new ServiceException(e);
		}
	}
	
	public boolean registraImpuesto(Impuesto impuesto) throws ServiceException {
		try {
			return impuestoDAO.registraImpuesto(impuesto);
		} catch (DAOException e) {
			throw new ServiceException(e);
		}
	}
	
	public boolean actualizaImpuesto(Impuesto impuesto) throws ServiceException {
		try {
			return impuestoDAO.actualizaImpuesto(impuesto);
		} catch (DAOException e) {
			throw new ServiceException(e);
		}
	}
	
	public boolean eliminaImpuesto(Impuesto impuesto) throws ServiceException {
		try {
			return impuestoDAO.eliminaImpuesto(impuesto);
		} catch (DAOException e) {
			throw new ServiceException(e);
		}
	}
	
	public Impuesto buscaImpuestoPorFactura(Factura factura) throws ServiceException {
		try {
			return impuestoDAO.buscaImpuestoPorFactura(factura);
		} catch (DAOException e) {
			throw new ServiceException(e);
		}
	}
	
}
