package cl.mker.inv.servicio.mail;

import java.util.UUID;

import javax.annotation.PostConstruct;
import javax.ejb.Singleton;
import javax.ejb.Startup;
import javax.inject.Inject;

import org.apache.commons.mail.Email;
import org.apache.commons.mail.EmailException;
import org.apache.commons.mail.SimpleEmail;

import cl.mker.inv.modelo.configuracion.CuentaCorreo;
import cl.mker.inv.seguridad.Usuario;
import cl.mker.inv.servicio.OperacionalService;
import cl.mker.inv.servicio.ServiceException;

@Startup
@Singleton
public class EmailService {

	@Inject
	private OperacionalService operacionalService;
	
	private CuentaCorreo cuentaCorreo;
	
	@PostConstruct
	public void init(){
		System.out.println("@PostConstruct init");
		
		cuentaCorreo = operacionalService.listaDatosCuentaCorreo();
	}
	
	// TODO: Refactor
	private void enviaMail(String destino, String titulo, String mensaje) throws ServiceException {
		try {
			Email email = new SimpleEmail();
			email.setSmtpPort(cuentaCorreo.getPuerto());
			email.setHostName(cuentaCorreo.getHost());
			email.setAuthentication(cuentaCorreo.getUsuario(), cuentaCorreo.getContrase�a());
			email.setStartTLSEnabled(true);
			email.setFrom(cuentaCorreo.getUsuario(), "Estimado");
			email.setSubject(titulo);
			email.setMsg(mensaje);
			email.addTo(destino);
			email.send();
		} catch (EmailException e) {
			throw new ServiceException(e);
		}
	}
	
	public void enviaCodigoActivacion(Usuario usuario) throws ServiceException {
		try {
			enviaMail(usuario.getUsername(), "Olvid� contrase�a", "Ingrese este c�digo: " + usuario.getCodigoActivacion());
		} catch (ServiceException e) {
			throw new ServiceException(e);
		}
	}
	
	public static void main(String[] args) throws ServiceException{
		Usuario usuario = new Usuario();
		usuario.setUsername("faundez.jp@gmail.com");
		usuario.setCodigoActivacion(UUID.randomUUID().toString());
		new EmailService().enviaCodigoActivacion(usuario);
	}
	
}
