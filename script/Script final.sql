insert into permiso (idPermiso, accion, descripcion) values ( 1, 'administraUsuario', 'Administra la mantencion de usuarios');
insert into permiso (idPermiso, accion, descripcion) values ( 2, 'administraCliente', 'Administra la mantencion de clientes');
insert into permiso (idPermiso, accion, descripcion) values ( 3, 'administraProveedor', 'Administra la mantencion de proveedores');
insert into permiso (idPermiso, accion, descripcion) values ( 4, 'administraMaterial', 'Administra la mantencion de materiales');
insert into permiso (idPermiso, accion, descripcion) values ( 5, 'administraImpuesto', 'Administra la mantencion de impuestos');

insert into permiso (idPermiso, accion, descripcion) values ( 6, 'administraSurtido', 'Administra la mantencion de surtido');
insert into permiso (idPermiso, accion, descripcion) values ( 7, 'administraDevolucion', 'Administra la mantencion de devoluciones');
insert into permiso (idPermiso, accion, descripcion) values ( 8, 'administraGasto', 'Administra la mantencion de gastos');
insert into permiso (idPermiso, accion, descripcion) values ( 9, 'listaReportes', 'Lista la los reportes');

insert into permiso (idPermiso, accion, descripcion) values (10, 'administraOrden', 'Administra la mantencion de ordenes');
insert into permiso (idPermiso, accion, descripcion) values (11, 'administraFactura', 'Administra la mantencion de facturas');

insert into perfil (idPerfil, grupo) values (1, 'Administrador');
insert into perfil (idPerfil, grupo) values (2, 'Operador');
insert into perfil (idPerfil, grupo) values (3, 'Vendedor');

insert into perfil_permiso (idPerfil, idPermiso) values (1,  1);
insert into perfil_permiso (idPerfil, idPermiso) values (1,  2);
insert into perfil_permiso (idPerfil, idPermiso) values (1,  3);
insert into perfil_permiso (idPerfil, idPermiso) values (1,  4);
insert into perfil_permiso (idPerfil, idPermiso) values (1,  5);

insert into perfil_permiso (idPerfil, idPermiso) values (2,  6);
insert into perfil_permiso (idPerfil, idPermiso) values (2,  7);
insert into perfil_permiso (idPerfil, idPermiso) values (2,  8);
insert into perfil_permiso (idPerfil, idPermiso) values (2,  9);

insert into perfil_permiso (idPerfil, idPermiso) values (3, 10);
insert into perfil_permiso (idPerfil, idPermiso) values (3, 11);

insert into usuario (username, password, habilitado) values ('slacker@darkstar.cl', 'f2f0eb8f1e718489d8c9eefd282a313b1ecb03fd5c51bebb0afd2037b5b70076', true);
insert into usuario (username, password, habilitado) values ('jpf@darkstar.cl', 'f2f0eb8f1e718489d8c9eefd282a313b1ecb03fd5c51bebb0afd2037b5b70076', true);

insert into usuario_perfil (idUsuario, idPerfil) values (1, 1);
insert into usuario_perfil (idUsuario, idPerfil) values (1, 2);
insert into usuario_perfil (idUsuario, idPerfil) values (1, 3);

insert into cliente (rut, nombre, direccion, telefono) values ("16675922-1", "Juan Pablo", "Baquedano 131", "315268");
insert into cliente (rut, nombre, direccion, telefono) values ("123000456-2", "Cristian Andres", "Baquedano 131", "315268");
insert into cliente (rut, nombre, direccion, telefono) values ("98712365-3", "Eduardo Javier", "Baquedano 131", "315268");

insert into proveedor (rut, razonSocial, direccion, telefono) values ("123-4", "Previred SPA", "Baquedano 131", "315268");
insert into proveedor (rut, razonSocial, direccion, telefono) values ("456-7", "Synapsis", "Baquedano 131", "315268");
insert into proveedor (rut, razonSocial, direccion, telefono) values ("890-k", "IBM", "Baquedano 131", "315268");

insert into material (nombre, valor, sector, lugar) values ("Jabon Popeye", 100, 'Sector1', 'A1');
insert into material (nombre, valor, sector, lugar) values ("Coca Cola", 200, 'Sector1', 'A1');
insert into material (nombre, valor, sector, lugar) values ("Manzanas", 300, 'Sector1', 'A1');

CALL `inventario`.`insertaImpuesto`(17, '2013-03-03');
CALL `inventario`.`insertaImpuesto`(18, '2014-04-04');
CALL `inventario`.`insertaImpuesto`(19, '2015-05-05');

insert into surtido (idProveedor, numeroFactura, fecha) values (1, 1, NOW());
insert into surtido (idProveedor, numeroFactura, fecha) values (1, 2, NOW());
insert into surtido (idProveedor, numeroFactura, fecha) values (1, 3, NOW());

insert into lineaSurtido (idSurtido, idMaterial, cantidad, valor) values (1, 1, 4, 50);
insert into lineaSurtido (idSurtido, idMaterial, cantidad, valor) values (1, 2, 6, 100);
insert into lineaSurtido (idSurtido, idMaterial, cantidad, valor) values (2, 3, 10, 150);

insert into orden (idCliente, fecha) values (1, NOW());
insert into orden (idCliente, fecha) values (2, NOW());

insert into lineaMaterial (idOrden, idMaterial, cantidad, valor) values (1, 1, 2, 100);
insert into lineaMaterial (idOrden, idMaterial, cantidad, valor) values (1, 2, 3, 200);
insert into lineaMaterial (idOrden, idMaterial, cantidad, valor) values (2, 3, 5, 300);
