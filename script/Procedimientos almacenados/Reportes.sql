DELIMITER //
CREATE PROCEDURE listaStockMateriales ()
BEGIN
	SELECT *, (comprados - vendidos) disponibles
	FROM (
		SELECT 
			m.idMaterial, 
			m.nombre, 
			COALESCE((SELECT sum(cantidad) FROM lineaMaterial WHERE idMaterial = m.idMaterial), 0) AS vendidos,
			COALESCE((SELECT sum(cantidad) FROM lineaSurtido WHERE idMaterial = m.idMaterial), 0) AS comprados,
			COALESCE((SELECT sum(d.cantidad) FROM devolucion d JOIN lineaMaterial lm ON lm.idLineaMaterial = d.idLineaMaterial WHERE lm.idMaterial = m.idMaterial), 0) AS devueltos
		FROM material m
	) cantidades;
END //
DELIMITER ;

DELIMITER //
CREATE PROCEDURE listaResumenFacturas ()
BEGIN
	SELECT r.*, count(a.monto) AS 'cantidadAbonos', COALESCE(sum(a.monto), 0) AS 'totalAbonado', totalNeto - COALESCE(sum(a.monto), 0) AS 'deudaActual'
    FROM (
		SELECT t.idFactura, t.numeroFactura, t.fecha, i.porcentaje , sum(t.total) AS totalBruto, sum(t.total) * (1 + i.porcentaje / 100) AS totalNeto
		FROM (
			SELECT f.idFactura, f.fecha, f.numeroFactura, f.idImpuesto, m.nombre, o.idOrden, lm.idLineaMaterial, lm.cantidad, lm.valor, (lm.cantidad * lm.valor) AS total
			FROM lineaMaterial lm
			JOIN material m ON m.idMaterial = lm.idMaterial
			JOIN orden o ON o.idOrden = lm.idOrden
			JOIN factura f ON f.idFactura = o.idFactura
		) t
		JOIN impuesto i ON i.idImpuesto = t.idImpuesto
		GROUP BY idFactura, numeroFactura, fecha
    ) r
    LEFT JOIN abono a ON a.idFactura = r.idFactura
    GROUP BY numeroFactura
    ORdER BY fecha ASC;
END //
DELIMITER ;

DELIMITER //
CREATE PROCEDURE listaResumenPorFactura 
(idFactura LONG)
BEGIN
	SELECT r.*, count(a.monto) AS 'cantidadAbonos', COALESCE(sum(a.monto), 0) AS 'totalAbonado', totalNeto - COALESCE(sum(a.monto), 0) AS 'deudaActual'
    FROM (
		SELECT t.idFactura, t.numeroFactura, t.fecha, i.porcentaje , sum(t.total) AS totalBruto, sum(t.total) * (1 + i.porcentaje / 100) AS totalNeto
		FROM (
			SELECT f.idFactura, f.fecha, f.numeroFactura, f.idImpuesto, m.nombre, o.idOrden, lm.idLineaMaterial, lm.cantidad, lm.valor, (lm.cantidad * lm.valor) AS total
			FROM lineaMaterial lm
			JOIN material m ON m.idMaterial = lm.idMaterial
			JOIN orden o ON o.idOrden = lm.idOrden
			JOIN factura f ON f.idFactura = o.idFactura
		) t
		JOIN impuesto i ON i.idImpuesto = t.idImpuesto
		GROUP BY idFactura, numeroFactura, fecha
    ) r
    LEFT JOIN abono a ON a.idFactura = r.idFactura
    WHERE r.idFactura = idFactura
    GROUP BY numeroFactura
    ORdER BY fecha ASC;
END //
DELIMITER ;

DELIMITER //
CREATE PROCEDURE listaResumenVentasPorFecha
(fechaInicio DATE, fechaFin DATE)
BEGIN
	SELECT 
		o.idOrden, 
		o.fecha, 
		COALESCE(f.numeroFactura, '') numeroFactura, 
		c.nombre nombreCliente, 
        c.rut,
		COALESCE(i.porcentaje, '') porcentaje, 
		m.idMaterial idMaterial,
        m.nombre nombreMaterial,
		lm.cantidad, 
		lm.valor, 
		(lm.cantidad * lm.valor) 'totalBruto', 
		COALESCE((lm.cantidad * lm.valor * (1 + i.porcentaje / 100)), '') 'totalNeto'
	FROM lineamaterial lm
	JOIN material m ON m.idMaterial = lm.idMaterial
	JOIN orden o ON o.idOrden = lm.idOrden
	JOIN cliente c ON c.idCliente = o.idCliente
	LEFT JOIN factura f ON f.idFactura = o.idFactura
	LEFT JOIN impuesto i ON i.idImpuesto = f.idImpuesto
	WHERE f.idFactura IS NOT NULL
	AND CAST(o.fecha AS DATE) BETWEEN fechaInicio AND fechaFin
	ORDER BY o.fecha ASC;
END //
DELIMITER ;
