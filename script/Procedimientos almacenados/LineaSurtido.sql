
DELIMITER //
CREATE PROCEDURE listaLineasSurtidoPorIdSurtido (idSurtido LONG)
BEGIN
	SELECT *
	FROM lineasurtido
	WHERE lineasurtido.idSurtido = idSurtido;
END //
DELIMITER ;

DELIMITER //
CREATE PROCEDURE listaMaterialesFueraDeSurtido (idSurtido LONG)
BEGIN
	SELECT *
    FROM material
    WHERE idMaterial NOT IN (
		SELECT m.idMaterial
		FROM material m 
		JOIN lineasurtido ls ON m.idMaterial = ls.idMaterial
		WHERE ls.idSurtido = idSurtido
    );
END //
DELIMITER ;

DELIMITER //
CREATE PROCEDURE buscaMaterialPorIdLineaSurtido (idLineaSurtido LONG)
BEGIN
	SELECT m.*
	FROM material m 
	JOIN lineasurtido ls ON m.idMaterial = ls.idMaterial
	WHERE ls.idLineaSurtido = idLineaSurtido;
END //
DELIMITER ;

DELIMITER //
CREATE PROCEDURE insertaLineaSurtido
(idSurtido LONG, idMaterial LONG, cantidad INT, valor LONG)
BEGIN
	INSERT INTO `inventario`.`lineasurtido`(`idSurtido`,`idMaterial`,`cantidad`,`valor`)
	VALUES(idSurtido,idMaterial,cantidad,valor);
END //
DELIMITER ;

DELIMITER //
CREATE PROCEDURE actualizaLineaSurtido
(idSurtido LONG, idMaterial LONG, cantidad INT, valor LONG)
BEGIN
	UPDATE `inventario`.`lineasurtido`
	SET lineasurtido.cantidad = cantidad,
	lineasurtido.valor = valor
	WHERE lineasurtido.idMaterial = idMaterial 
    AND lineasurtido.idSurtido = idSurtido;
END //
DELIMITER ;

DELIMITER //
CREATE PROCEDURE eliminaLineaSurtido
(idLineaSurtido LONG)
BEGIN 
	DELETE FROM lineaSurtido 
    WHERE lineaSurtido.idLineaSurtido = idLineaSurtido;
END //
DELIMITER ;

DELIMITER //
CREATE PROCEDURE eliminaLineaSurtidoPorSurtido
(idSurtido LONG)
BEGIN
	DELETE FROM lineasurtido WHERE lineasurtido.idSurtido = idSurtido;
END //
DELIMITER ;

DELIMITER //
CREATE PROCEDURE sumaCantidadLineaSurtidoPorMaterial (idMaterial LONG)
BEGIN
	SELECT COALESCE(SUM(cantidad),0) cantidad
	from lineasurtido
	WHERE lineasurtido.idMaterial = idMaterial;
END //
DELIMITER ;


DELIMITER //
CREATE PROCEDURE listaLineasSurtidoPorMaterial (idMaterial LONG)
BEGIN
	SELECT *
	FROM lineasurtido
	WHERE lineasurtido.idMaterial = idMaterial;
END //
DELIMITER ;
