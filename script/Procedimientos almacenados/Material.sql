DELIMITER //
CREATE PROCEDURE insertaMaterial
(nombre VARCHAR(45), valor LONG, sector VARCHAR(10), lugar VARCHAR(45))
BEGIN
    INSERT INTO inventario.material	(valor, nombre, sector, lugar) VALUES(valor, nombre, sector, lugar);
END //
DELIMITER ;

DELIMITER //
CREATE PROCEDURE eliminaMaterial
(idMaterial LONG)
BEGIN
	DELETE FROM material WHERE material.idMaterial = idMaterial;
END //
DELIMITER ;

DELIMITER //
CREATE PROCEDURE actualizaMaterial
(idMaterial LONG, nombre VARCHAR(45), valor LONG, sector VARCHAR(10), lugar VARCHAR(45))
BEGIN
	UPDATE inventario.material
	SET	material.valor = valor,
	material.nombre = nombre,
    material.sector = sector,
    material.lugar = lugar
	WHERE material.idMaterial = idMaterial;
END //
DELIMITER ;


DELIMITER //
CREATE PROCEDURE buscaMaterialPorNombre (nombre VARCHAR(50))
BEGIN
	SELECT * FROM material WHERE material.nombre = nombre;
END //
DELIMITER ;

DELIMITER //
CREATE PROCEDURE listaMateriales ()
BEGIN
	SELECT * FROM material;
END //
DELIMITER ;
