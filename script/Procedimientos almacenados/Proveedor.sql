DELIMITER //
CREATE PROCEDURE insertaProveedor
(rut VARCHAR(45), razonSocial VARCHAR(50), direccion VARCHAR(100), telefono VARCHAR(20))
BEGIN
	INSERT INTO inventario.proveedor (rut, razonSocial, direccion, telefono) VALUES (rut, razonSocial, direccion, telefono);
END //
DELIMITER ;

DELIMITER //
CREATE PROCEDURE eliminaProveedor
(idProveedor LONG)
BEGIN
	DELETE FROM proveedor WHERE proveedor.idProveedor = idProveedor;
END //
DELIMITER ;

DELIMITER //
CREATE PROCEDURE actualizaProveedor
(idProveedor LONG, rut VARCHAR(45), razonSocial VARCHAR(50), direccion VARCHAR(100), telefono VARCHAR(20))
BEGIN
	UPDATE `inventario`.`proveedor`
	SET	proveedor.rut = rut,
	proveedor.razonSocial = razonSocial,
    proveedor.direccion = direccion,
    proveedor.telefono = telefono
	WHERE proveedor.idProveedor = idProveedor;
END //
DELIMITER ;


DELIMITER //
CREATE PROCEDURE buscaProveedorPorRut (rut VARCHAR(50))
BEGIN
	SELECT * FROM proveedor WHERE proveedor.rut = rut;
END //
DELIMITER ;

DELIMITER //
CREATE PROCEDURE listaProveedores ()
BEGIN
	SELECT * FROM proveedor;
END //
DELIMITER ;

