DELIMITER //
CREATE PROCEDURE listaUsuarios ()
BEGIN
	SELECT *
	FROM usuario;
END //
DELIMITER ;

DELIMITER //
CREATE PROCEDURE buscaUsuarioPorUsername 
(username VARCHAR(45))
BEGIN
	SELECT *
	FROM usuario
    WHERE usuario.username = username;
END //
DELIMITER ;

DELIMITER //
CREATE PROCEDURE insertaUsuario
(username VARCHAR(45), habilitado BOOL)
BEGIN
	INSERT INTO inventario.usuario (username,habilitado) VALUES (username,habilitado);
    SELECT LAST_INSERT_ID();
END //
DELIMITER ;

DELIMITER //
CREATE PROCEDURE actualizaUsuario
(idUsuario LONG, username VARCHAR(45), habilitado BOOL)
BEGIN
	UPDATE inventario.usuario
	SET	usuario.username = username,
		usuario.habilitado = habilitado
	WHERE usuario.idUsuario = idUsuario;
END //
DELIMITER ;

DELIMITER //
CREATE PROCEDURE actualizaCodigoActivacion
(idUsuario LONG, codigoActivacion VARCHAR(32))
BEGIN
	UPDATE inventario.usuario
	SET	usuario.codigoActivacion = codigoActivacion,
    fechaActivacion = NOW()
	WHERE usuario.idUsuario = idUsuario;
END //
DELIMITER ;

DELIMITER //
CREATE PROCEDURE actualizaPassword
(idUsuario LONG, password VARCHAR(64))
BEGIN
	UPDATE inventario.usuario
	SET	usuario.password = password
	WHERE usuario.idUsuario = idUsuario;
END //
DELIMITER ;

DELIMITER //
CREATE PROCEDURE eliminaUsuario 
(idUsuario LONG)
BEGIN
	DELETE FROM usuario
    WHERE usuario.idUsuario = idUsuario;
END //
DELIMITER ;