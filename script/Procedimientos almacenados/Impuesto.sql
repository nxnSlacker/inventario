
DELIMITER //
CREATE PROCEDURE listaImpuestos ()
BEGIN
	SELECT `impuesto`.`idImpuesto`, `impuesto`.`porcentaje`, `impuesto`.`fecha`
	FROM `inventario`.`impuesto`;
END //
DELIMITER ;


DELIMITER //
CREATE PROCEDURE insertaImpuesto
(porcentaje DOUBLE, fecha DATETIME)
BEGIN
	INSERT INTO `inventario`.`impuesto`(`porcentaje`,`fecha`)
	VALUES(porcentaje,fecha);
END //
DELIMITER ;

DELIMITER //
CREATE PROCEDURE actualizaImpuesto
(idImpuesto INT, porcentaje DOUBLE, fecha DATETIME)
BEGIN
	UPDATE `inventario`.`impuesto`
	SET	impuesto.porcentaje = porcentaje,
	impuesto.fecha = fecha
	WHERE impuesto.idImpuesto = idImpuesto;
END //
DELIMITER ;

DELIMITER //
CREATE PROCEDURE eliminaImpuesto
(idImpuesto INT)
BEGIN
	DELETE FROM `inventario`.`impuesto`
	WHERE impuesto.idImpuesto = idImpuesto;
END //
DELIMITER ;

DELIMITER //
CREATE PROCEDURE buscaImpuestoPorFactura 
(idFactura LONG)
BEGIN
	SELECT i.idImpuesto, i.porcentaje, i.fecha
	FROM impuesto i
    JOIN factura f ON f.idImpuesto = i.idImpuesto
	WHERE f.idFactura = idFactura;
END //
DELIMITER ;