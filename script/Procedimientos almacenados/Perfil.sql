DELIMITER //
CREATE PROCEDURE buscaPerfilesPorUsuario 
(idUsuario INT)
BEGIN
    SELECT p.* 
	FROM perfil p
	JOIN usuario_perfil up on up.idPerfil = p.idPerfil
	WHERE up.idUsuario = idUsuario;
END //
DELIMITER ;

DELIMITER //
CREATE PROCEDURE listaPerfiles ()
BEGIN
    SELECT *
	FROM perfil;
END //
DELIMITER ;

DELIMITER //
CREATE PROCEDURE agregaPerfilUsuario 
(idUsuario LONG, idPerfil LONG)
BEGIN
    INSERT INTO usuario_perfil (idUsuario, idPerfil) VALUES (idUsuario, idPerfil);
END //
DELIMITER ;

DELIMITER //
CREATE PROCEDURE eliminaPerfilUsuario 
(idUsuario LONG, idPerfil LONG)
BEGIN
    DELETE FROM usuario_perfil WHERE usuario_perfil.idUsuario = idUsuario AND usuario_perfil.idPerfil = idPerfil;
END //
DELIMITER ;