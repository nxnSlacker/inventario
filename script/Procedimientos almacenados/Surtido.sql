DELIMITER //
CREATE PROCEDURE insertaSurtido
(idProveedor LONG, numeroFactura LONG)
BEGIN
	INSERT INTO `inventario`.`surtido`
	(`idProveedor`,`numeroFactura`,`fecha`)
	VALUES (idProveedor,numeroFactura, NOW());
    SELECT LAST_INSERT_ID();
END //
DELIMITER ;

DELIMITER //
CREATE PROCEDURE eliminaSurtido
(idSurtido LONG)
BEGIN

	DELETE FROM surtido WHERE surtido.idSurtido = idSurtido;
END //
DELIMITER ;

DELIMITER //
CREATE PROCEDURE actualizaSurtido
(idSurtido LONG, idProveedor INT, numeroFactura LONG)
BEGIN
	UPDATE `inventario`.`surtido`
	SET
	surtido.idProveedor = idProveedor,
	surtido.numeroFactura = numeroFactura
	WHERE surtido.idSurtido = idSurtido;
END //
DELIMITER ;


DELIMITER //
CREATE PROCEDURE buscaSurtidoPorNumeroFactura (numeroFactura INT)
BEGIN
	SELECT * FROM surtido WHERE surtido.numeroFactura = numeroFactura;
END //
DELIMITER ;

DELIMITER //
CREATE PROCEDURE listaSurtidos ()
BEGIN
	SELECT * FROM surtido;
END //
DELIMITER ;

-- Enviar a sscript de surtido
DELIMITER //
CREATE PROCEDURE buscaProveedorPorSurtido (idSurtido LONG)
BEGIN
	SELECT p.*
	FROM proveedor p 
	JOIN surtido s ON p.idProveedor = s.idProveedor
	WHERE s.idSurtido = idSurtido;
END //
DELIMITER ;

DELIMITER //
CREATE PROCEDURE listaSurtidosPorProveedor (idProveedor LONG)
BEGIN
	SELECT * FROM surtido WHERE surtido.idProveedor = idProveedor;
END //
DELIMITER ;

DELIMITER //
CREATE PROCEDURE buscaSurtidoPorNumero
(numeroFactura LONG)
BEGIN
	SELECT *
	FROM surtido
    WHERE surtido.numeroFactura = numeroFactura;
END //
DELIMITER ;
