
DELIMITER //
CREATE PROCEDURE listaLineasMaterialPorIdOrden (idOrden LONG)
BEGIN
	SELECT *
	FROM lineamaterial
	WHERE lineamaterial.idOrden = idOrden;
END //
DELIMITER ;

DELIMITER //
CREATE PROCEDURE listaMaterialesFueraDeOrden (idOrden LONG)
BEGIN
	SELECT *
    FROM material
    WHERE idMaterial NOT IN (
		SELECT m.idMaterial
		FROM material m 
		JOIN lineamaterial lm ON m.idMaterial = lm.idMaterial
		WHERE lm.idOrden = idOrden
    );
END //
DELIMITER ;

DELIMITER //
CREATE PROCEDURE buscaMaterialPorIdLineaMaterial (idLineaMaterial LONG)
BEGIN
	SELECT m.*
	FROM material m 
	JOIN lineamaterial lm ON m.idMaterial = lm.idMaterial
	WHERE lm.idLineaMaterial = idLineaMaterial;
END //
DELIMITER ;

DELIMITER //
CREATE PROCEDURE insertaLineaMaterial
(idOrden LONG, idMaterial LONG, cantidad INT, valor LONG)
BEGIN
	INSERT INTO `inventario`.`lineamaterial`(`idOrden`,`idMaterial`,`cantidad`,`valor`)
	VALUES(idOrden,idMaterial,cantidad,valor);
END //
DELIMITER ;

DELIMITER //
CREATE PROCEDURE actualizaLineaMaterial
(idOrden LONG, idMaterial LONG, cantidad INT, valor LONG)
BEGIN
	UPDATE `inventario`.`lineamaterial`
	SET lineamaterial.cantidad = cantidad,
	lineamaterial.valor = valor
	WHERE lineamaterial.idMaterial = idMaterial 
    AND lineamaterial.idOrden = idOrden;
END //
DELIMITER ;

DELIMITER //
CREATE PROCEDURE eliminaLineaMaterial
(idLineaMaterial LONG)
BEGIN 
	DELETE FROM lineaMaterial
    WHERE lineamaterial.idLineaMaterial = idLineaMaterial;
END //
DELIMITER ;

DELIMITER //
CREATE PROCEDURE eliminaLineaMaterialPorOrden
(idOrden LONG)
BEGIN
	DELETE FROM lineamaterial WHERE lineamaterial.idOrden = idOrden;
END //
DELIMITER ;

DELIMITER //
CREATE PROCEDURE sumaCantidadLineaMaterialPorMaterial (idMaterial LONG)
BEGIN
	SELECT COALESCE(SUM(cantidad),0) cantidad
	from lineamaterial
	WHERE lineamaterial.idMaterial = idMaterial;
END //
DELIMITER ;

DELIMITER //
CREATE PROCEDURE listaLineasMaterialPorMaterial (idMaterial LONG)
BEGIN
	SELECT *
	FROM lineamaterial
	WHERE lineamaterial.idMaterial = idMaterial;
END //
DELIMITER ;
