
DELIMITER //
CREATE PROCEDURE listaGastos ()
BEGIN
    SELECT *
	FROM gasto;
END //
DELIMITER ;

DELIMITER //
CREATE PROCEDURE eliminaGasto (idGasto LONG)
BEGIN
    DELETE FROM gasto
    WHERE gasto.idGasto = idGasto;
END //
DELIMITER ;

DELIMITER //
CREATE PROCEDURE insertaGasto
(monto LONG, descripcion VARCHAR(45))
BEGIN
	INSERT INTO inventario.gasto (monto, descripcion, fecha) VALUES (monto, descripcion, NOW());
END //
DELIMITER ;

DELIMITER //
CREATE PROCEDURE actualizaGasto
(idGasto LONG, monto LONG, descripcion VARCHAR(45))
BEGIN
	UPDATE inventario.gasto 
    SET inventario.monto = monto,
    inventario.descripcion = descripcion
    WHERE gasto.idGasto = idGasto;
END //
DELIMITER ;
