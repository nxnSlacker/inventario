DELIMITER //
CREATE PROCEDURE listaFacturas ()
BEGIN
	SELECT *
	FROM factura;
END //
DELIMITER ;

DELIMITER //
CREATE PROCEDURE buscaClientePorFactura(idFactura LONG)
BEGIN
	SELECT c.*
	FROM cliente c
	JOIN orden o ON 
		o.idCliente = c.idCliente
	JOIN factura f ON 
		f.idFactura = o.idFactura
	WHERE f.idFactura = idFactura
	LIMIT 1;
END //
DELIMITER ;


DELIMITER //
CREATE PROCEDURE listaOrdenesSinFacturaPorCliente(idCliente LONG)
BEGIN
	SELECT *
	FROM orden
	WHERE orden.idCliente = idCliente AND orden.idFactura IS NULL;
END //
DELIMITER ;

DELIMITER //
CREATE PROCEDURE listaOrdenesPorCliente(idCliente LONG)
BEGIN
	SELECT *
	FROM orden
	WHERE orden.idCliente = idCliente;
END //
DELIMITER ;

DELIMITER //
CREATE PROCEDURE insertaFactura
(idImpuesto LONG, numeroFactura VARCHAR(45))
BEGIN
	INSERT INTO `inventario`.`factura`
	(idImpuesto,numeroFactura,fecha)
	VALUES (idImpuesto, numeroFactura, NOW());
    SELECT LAST_INSERT_ID();
END //
DELIMITER ;

DELIMITER //
CREATE PROCEDURE eliminaFactura
(idFactura LONG)
BEGIN
	DELETE FROM factura WHERE factura.idFactura = idFactura;
END //
DELIMITER ;

DELIMITER //
CREATE PROCEDURE actualizaFactura
(idFactura LONG, idImpuesto LONG, numeroFactura VARCHAR(45))
BEGIN
	UPDATE `inventario`.`factura`
	SET	factura.idImpuesto = idImpuesto,
		factura.numeroFactura = numeroFactura
	WHERE factura.idFactura = idFactura;
END //
DELIMITER ;

DELIMITER //
CREATE PROCEDURE buscaFacturaPorOrden
(idOrden LONG)
BEGIN
	SELECT f.*
	FROM Factura f
    JOIN Orden o ON f.idFactura = o.idFactura
    WHERE o.idOrden = idOrden;
END //
DELIMITER ;

DELIMITER //
CREATE PROCEDURE descartaOrdenesParaFactura
(idFactura LONG)
BEGIN
	update orden 
    set idFactura = null
    where orden.idFactura = idFactura;
END //
DELIMITER ;

DELIMITER //
CREATE PROCEDURE buscaFacturaPorNumero
(numeroFactura LONG)
BEGIN
	SELECT *
	FROM factura
    WHERE factura.numeroFactura = numeroFactura;
END //
DELIMITER ;