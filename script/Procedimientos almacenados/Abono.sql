DELIMITER //
CREATE PROCEDURE buscaAbonosPorFactura (idFactura LONG)
BEGIN
    SELECT *
	FROM abono
    WHERE abono.idFactura = idFactura;
END //
DELIMITER ;

DELIMITER //
CREATE PROCEDURE eliminaAbono (idAbono LONG)
BEGIN
    DELETE FROM abono
    WHERE abono.idAbono = idAbono;
END //
DELIMITER ;

DELIMITER //
CREATE PROCEDURE insertaAbono
(idFactura LONG, monto LONG)
BEGIN
	INSERT INTO inventario.abono (idFactura, monto, fecha) VALUES (idFactura, monto, NOW());
END //
DELIMITER ;
