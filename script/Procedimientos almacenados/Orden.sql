DELIMITER //
CREATE PROCEDURE insertaOrden
(idCliente LONG)
BEGIN
	INSERT INTO `inventario`.`orden`
	(idCliente,fecha)
	VALUES (idCliente, NOW());
    SELECT LAST_INSERT_ID();
END //
DELIMITER ;

DELIMITER //
CREATE PROCEDURE eliminaOrden
(idOrden LONG)
BEGIN
	DELETE FROM orden WHERE orden.idOrden = idOrden;
END //
DELIMITER ;

DELIMITER //
CREATE PROCEDURE actualizaOrden
(idOrden LONG, idCliente INT)
BEGIN
	UPDATE `inventario`.`orden`
	SET
	orden.idCliente = idCliente
	WHERE orden.idOrden = idOrden;
END //
DELIMITER ;

DELIMITER //
CREATE PROCEDURE actualizaFacturaParaOrden
(idOrden LONG, idFactura LONG)
BEGIN
	UPDATE `inventario`.`orden`
	SET orden.idFactura = idFactura
	WHERE orden.idOrden = idOrden;
END //
DELIMITER ;

DELIMITER //
CREATE PROCEDURE descartaFacturaParaOrden
(idOrden LONG)
BEGIN
	UPDATE `inventario`.`orden`
		SET orden.idFactura = null
	WHERE orden.idOrden = idOrden;
END //
DELIMITER ;
-- Busca orde por numero factura no esta

DELIMITER //
CREATE PROCEDURE listaOrdenes ()
BEGIN
	SELECT * FROM orden;
END //
DELIMITER ;

-- Enviar a script de orden
DELIMITER //
CREATE PROCEDURE buscaClientePorOrden (idOrden LONG)
BEGIN
	SELECT c.*
	FROM cliente c 
	JOIN orden o ON c.idCliente = o.idCliente
	WHERE o.idOrden = idOrden;
END //
DELIMITER ;


DELIMITER //
CREATE PROCEDURE listaOrdenesPorFactura (idFactura LONG)
BEGIN
	SELECT *
	FROM orden
    WHERE orden.idFactura = idFactura;
END //
DELIMITER ;