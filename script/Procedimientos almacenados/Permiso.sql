DELIMITER //
CREATE PROCEDURE buscaPermisosPorPerfil 
(idPerfil LONG)
BEGIN
    SELECT p.* 
	FROM permiso p
	JOIN perfil_permiso pp on pp.idPermiso = p.idPermiso
	WHERE pp.idPerfil = idPerfil;
END //
DELIMITER ;