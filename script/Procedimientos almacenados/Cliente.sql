DELIMITER //
CREATE PROCEDURE insertaCliente
(rut VARCHAR(45), nombre VARCHAR(50), direccion VARCHAR(100), telefono VARCHAR(20))
BEGIN
	INSERT INTO inventario.cliente (rut, nombre, direccion, telefono) VALUES (rut, nombre, direccion, telefono);
END //
DELIMITER ;

DELIMITER //
CREATE PROCEDURE eliminaCliente
(idCliente LONG)
BEGIN
	DELETE FROM cliente WHERE cliente.idCliente = idCliente;
END //
DELIMITER ;

DELIMITER //
CREATE PROCEDURE actualizaCliente
(idCliente LONG, rut VARCHAR(45), nombre VARCHAR(50), direccion VARCHAR(100), telefono VARCHAR(20))
BEGIN
	UPDATE inventario.cliente
	SET	cliente.rut = rut,
	cliente.nombre = nombre,
    cliente.direccion = direccion,
    cliente.telefono = telefono
	WHERE cliente.idCliente = idCliente;
END //
DELIMITER ;

DELIMITER //
CREATE PROCEDURE buscaClientePorRut (rut VARCHAR(50))
BEGIN
	SELECT * FROM cliente WHERE cliente.rut = rut;
END //
DELIMITER ;

DELIMITER //
CREATE PROCEDURE listaClientes ()
BEGIN
	SELECT * FROM cliente;
END //
DELIMITER ;



