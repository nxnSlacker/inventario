
DELIMITER //
CREATE PROCEDURE buscaDevolucionesPorLineaMaterial
(idLineaMaterial INT)
BEGIN
	SELECT *
	FROM inventario.devolucion
	WHERE devolucion.idLineaMaterial = idLineaMaterial;
END //
DELIMITER ;

DELIMITER //
CREATE PROCEDURE insertaDevolucion
(idLineaMaterial LONG, cantidad INT, motivo VARCHAR(50))
BEGIN
	INSERT INTO devolucion (idLineaMaterial, cantidad, fecha, motivo) VALUES (idLineaMaterial, cantidad, NOW(), motivo);
END //
DELIMITER ;

DELIMITER //
CREATE PROCEDURE actualizaDevolucion
(idDevolucion LONG, cantidad INT, motivo VARCHAR(50))
BEGIN
	UPDATE devolucion
		SET devolucion.cantidad = cantidad,
        devolucion.fecha = NOW(),
        devolucion.motivo = motivo
	WHERE devolucion.idDevolucion = idDevolucion;
END //
DELIMITER ;

DELIMITER //
CREATE PROCEDURE eliminaDevolucion
(idDevolucion LONG)
BEGIN
    DELETE FROM devolucion WHERE devolucion.idDevolucion = idDevolucion;
END //
DELIMITER ;